import { AppRegistry } from 'react-native';
import App from './App';
import clear from 'react-native-clear-app-cache';

   clear.clearAppCache(() => {
      console.log("app cache cleared")
    })
    
   clear.getAppCacheSize((value, unit) => {
       console.log(value)
       console.log(unit)
    })
/*import { Sentry,SentryLog } from 'react-native-sentry';
Sentry.config('https://952a58b7c77645759b0ea33c431c7cfb:767aa17b98ae469d873b0f396efb285b@sentry.io/416606',{
  deactivateStacktraceMerging: false, // default: true | Deactivates the stacktrace merging feature
  logLevel: SentryLog.Debug, // default SentryLog.None | Possible values:  .None, .Error, .Debug, .Verbose
  disableNativeIntegration: false, // default: false | Deactivates the native integration and only uses raven-js
  handlePromiseRejection: true // default: true | Handle unhandled promise rejections
}).install();*/
AppRegistry.registerComponent('HighFive', () => App);
