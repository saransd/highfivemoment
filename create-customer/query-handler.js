var {ApolloClient} = require('apollo-client');
var fetch = require('node-fetch');
var { createHttpLink } = require('apollo-link-http');
var { InMemoryCache } = require('apollo-cache-inmemory');
var gql = require("graphql-tag");

const link = createHttpLink({ uri: 'https://hfm-api.azurewebsites.net/graphql', fetch: fetch });
const cache = new InMemoryCache();

const client = new ApolloClient({
   link : link,
   cache : cache
});

const callQuery = (queryString,callback) =>
{
 const query = gql`${queryString}`;
  
  client
  .query({
    query: query
  })
  .then(result => { 
    callback(result)
  }).catch((e) => { 
    console.log(e)
  });
}

const callMutation = (queryString,variables,callback) => 
{
  const query = gql`${queryString}`; 
  client.mutate({
    variables:variables,
    mutation: query
  })
  .then(result => {
     callback(result)
  }).catch((e)=>{
     console.log(e);
  });
}

exports.callQuery = callQuery;

exports.callMutation = callMutation;


