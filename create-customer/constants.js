const QUERY_EMP_BY_EMAIL = function (email) {
  return `{
  employee(email:"${email}")
  {
    id
    employeeId
    employeeName{
      givenName
      familyName
      additionalName
    }
    organization{
      organizationCoreData{
        organizationId
        organizationName
      }
    }
    totalHighFive
    joinedDate
  }
}`
}


const QUERY_UPDATE_EMP_ID = function (obj) {
 return  `mutation updateEmployee($id:ID!,$employeeId:ID){
  updateEmployee(employee:{
    id:$id
    employeeId:$employeeId
  })
    {
      employeeId
    }
}`
}


const QUERY_ADD_CONSUMER = function (obj) {
return `mutation addConsumer($consumerId:ID!,$givenName:String!,$familyName:String!,$number:String!,$street:String!,$street2:String,$city:String!,
$country:USACountry!,$USA_state:USAState!,$USA_zipCode:String!,$consumerEmployeeMapping:ConsumerEmployeeMappingInput,$consumerEmails:[Email],$latitude:Float!,$longitude:Float!){
  addConsumer(consumer:{
    consumerCoreData:{
      consumerId : $consumerId
      consumerName:{
         givenName: $givenName
         familyName: $familyName
      }
    }
    consumerCurrentAddress:{
      number:$number,
      street:$street,
      street2:$street2,
      city:$city,
      countryInfo:{
        country:$country
        USA_state:$USA_state
        USA_zipCode:$USA_zipCode
      }
    }
    consumerEmployeeMapping:$consumerEmployeeMapping
    consumerEmails:$consumerEmails
    consumreGeo : {
      latitude:$latitude,
      longitude:$longitude
    }
    
  })
  {
    consumerCoreData{
      consumerId
      consumerName
      {
        givenName
        familyName
      }
    }
    consumerCurrentAddress{
      number
      street
      street2
      city
      countryInfo{
        country
        USA_state
        USA_zipCode
      }
    }
    consumerEmails
    consumreGeo{
      latitude
      longitude
    }
  }
}`
}

exports.QUERY_EMP_BY_EMAIL = QUERY_EMP_BY_EMAIL;
exports.QUERY_UPDATE_EMP_ID = QUERY_UPDATE_EMP_ID;
exports.QUERY_ADD_CONSUMER = QUERY_ADD_CONSUMER;


