const initialState = {
   profileObj:{},
   totalHighFive:0,
   organizations:[],
   users:[]
};

const employee = (state = initialState, action) => {
    switch (action.type) {
      
        case 'GET_PROFILE':
        return Object.assign({}, state, action.payload);
        case "LAST_HIGHFIVE_SCORE":
        return Object.assign({}, state, action.payload);
        case "ORGANIZATIONS":
        return Object.assign({}, state, action.payload);
        case "USERS":
        return Object.assign({},state,action.payload);
        default:
            return state
    }
}

export default employee;