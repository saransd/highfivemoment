import { combineReducers } from 'redux';
// import navigationReducer from "./navigation";

import employee from './employee';

const rootReducer = combineReducers({
    employee,
      // navigationReducer

});

export default rootReducer;