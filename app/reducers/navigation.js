import {getRoutes} from '../../Routes';

const AppNavigator = getRoutes(null,null,null,true,{});
console.log('AppNavigator   :'+JSON.stringify(AppNavigator));

const initialState = AppNavigator.router.getStateForAction(
  AppNavigator.router.getActionForPathAndParams("Home")
);
console.log('AppNavigator   :'+JSON.stringify(AppNavigator));

const navigationReducer = (state = initialState, action) => {
  const newState = AppNavigator.router.getStateForAction(action, state);
  return newState || state;
};

export default navigationReducer;