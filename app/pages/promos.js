import React from 'react';
import {Container,Content,Text,Grid,Row,Col,Button} from 'native-base';
import {Image,Dimensions,StyleSheet,Platform, PixelRatio} from 'react-native';
import {withNavigation} from 'react-navigation';
import {SearchBox} from '../components/tabElements';
import _ from 'lodash';
import {globalstyles} from '../assets/css/styles';
import Analytics from 'appcenter-analytics';

 const deviceHeight = Dimensions.get('window').height;
  const deviceWidth = Dimensions.get('window').width;
  var DeviceInfo = require('react-native-device-info');
  // import styles from '../assets/css/settings';
  var fsizetitle;
  var fsize1;
  var fsize2;
  var fsize3;
  var serachBarWidth;
  var gridColHeight;
  var redeemButtonHeight;
  var testMarginTop;
  var titleTop;

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      fsize1 = 29
      fsize2 = 27
      fsize3 = 23
      gridColHeight = deviceHeight/7
      serachBarWidth = deviceWidth/1.6
      redeemButtonHeight = 60
      testMarginTop = deviceHeight/25
      titleTop = deviceHeight/12
    }
    else{
      fsize1 = 26
      fsize2 = 25
      fsize3 = 20
      gridColHeight = deviceHeight/7
      serachBarWidth = deviceWidth/1.6
      redeemButtonHeight = 50
      testMarginTop = deviceHeight/25
      titleTop = deviceHeight/20
    }
  }
  else{
    titleTop = 0
    testMarginTop = deviceHeight/25
    fsize1 = 24
    fsize2 = 25
    fsize3 = 20
    redeemButtonHeight = 50
    serachBarWidth = deviceWidth/1.1
    gridColHeight = deviceHeight/7

    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      fsize1 = 18
      fsize2 = 18
      fsize3 = 18
      gridColHeight = deviceHeight/9
      redeemButtonHeight = 50
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
    
      gridColHeight = deviceHeight/7
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      
    }
    else{
    
    }
  }


 class Promos extends React.Component {
    static navigationOptions ={
        header:null
    }

    render()
    {  
        return(
               <Content style={{paddingRight:10,paddingLeft:10,paddingTop:20}}>
               <Text style={styles.text1}>HIGH FIVE PROMOS</Text>
               <SearchBox icon="ios-search" placeholder="Search Promotions" styles={styles.searchBox}/>
               <Grid>
               <Row style={styles.gridRow1}>
                   <Image source={require('../assets/images/urbanfuel.png')} style={styles.gridImage}/>
              </Row>
                <Row style={styles.gridRow2}>
                    <Text style={styles.text2}>$1.00 off your order</Text>
                </Row>
                {DeviceInfo.isTablet()?
                  <Col style={styles.gridCol}>
                  <Text style={styles.textSize}>Show your server or barista and</Text>
                  <Text style={styles.textSize}>have them click redeem</Text>
                  </Col> :
                <Col style={styles.gridCol}>
                    <Text style={styles.textSize}>Show your server or</Text>
                    <Text style={styles.textSize}>barista and have them</Text>
                    <Text style={styles.textSize}>click redeem</Text>
                </Col> }
                <Row style={styles.gridRow3}>
                     <Button style={[globalstyles.footerColor,styles.redeemButton]}
                             onPress={()=>{Analytics.trackEvent("Redeem"); this.props.navigation.navigate("Redeem")}}>
                         <Text style={styles.redeemButtonText}>REDEEM</Text>
                     </Button>
                </Row>
                </Grid>
           </Content>
        )
    }
}

export default withNavigation(Promos) 
const styles = StyleSheet.create({
    text1:{
      marginTop: titleTop,
      fontSize:fsize1,
      alignSelf:'center'
    },
    searchBox:{
      width:serachBarWidth,
      backgroundColor:'#E5E5E5',
      alignSelf:'center',
      marginTop: deviceHeight/50,
      marginBottom:testMarginTop,
    },
    gridRow1:{
      height:deviceHeight/5,
      alignItems:'center',
      justifyContent:'center'
    },
    gridImage:{
      marginTop: testMarginTop,
      marginBottom: testMarginTop,
      height:deviceHeight/6,
      width:deviceWidth/3
    },
    gridImage2:{
      margin:deviceHeight/30,
      height:deviceHeight/6,
      width:deviceWidth/3
    },
    gridRow2:{
      margin: testMarginTop,
      justifyContent:'center'
    },
    text2:{
      fontSize:fsize2
    },
    gridCol:{
      height:gridColHeight,
      alignItems:'center',
    },
    gridRow3:{
      paddingTop:20,
      alignItems:'center',
      justifyContent:'center'
    },
    redeemButton:{
      height:redeemButtonHeight,
      width:150,
      borderRadius:15,
      alignItems:'center',
      justifyContent:'center'
    },
    redeemButtonText:{
      fontSize:fsize1,
      height: 50,
      paddingTop:22
    },
    textSize:{
        fontSize:fsize3
    }
})


