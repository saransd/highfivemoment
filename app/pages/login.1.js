import React from 'react';
import {Image} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text,Item,Input } from 'native-base';
import {styles} from '../assets/css/styles';
export default class Login extends React.Component
{
 static navigationOptions = {
     header:null
 }
  constructor(props)
  {
      super(props)
  }
     render()
     {
     return ( <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Header</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Text>
            This is Content Section
          </Text>

        <Image style={styles.logo} source={require('../assets/images/thf_logo.png')}/>
        <Item>
          <Input placeholder="email" />
          </Item>
        </Content>
        <Footer>
          <FooterTab>
            <Button full onPress={()=>{this.props.navigation.navigate("SignUp")}}>
              <Text>Don't have an account? Sign Up here</Text>
            </Button>
          </FooterTab>
        </Footer>
     </Container> );
     }
}