import React from 'react';
import {View,TouchableOpacity,WebView} from 'react-native';
import {PRIVACY_POLICY,TERMS_OF_USE} from '../constants';
import {globalstyles} from '../assets/css/styles';
import {Container,Content,Text,Icon} from 'native-base';
import Loader from '../components/loader';

export default class Privacy extends React.Component{
    static navigationOptions = {
        header:null
    }
    renderLoading()
    {
     return <Loader/>
    }
    render()
    {
      return  (<Container style={[globalstyles.containerBgColor,{paddingTop:20}]}>
                <Content contentContainerStyle={{flex:1}}>
                  <TouchableOpacity style={{flexDirection:'row',paddingLeft:10,alignItems:'center'}} onPress={()=>{this.props.navigation.goBack()}}>
                     <Icon type="FontAwesome" name="caret-left" style={{color:'#B5B5B5'}}/>
                     <Text style={{paddingLeft:5,fontSize:16}}>Back</Text>
                   </TouchableOpacity>
                   {/*this.props.navigation.state.params.title === "Privacy Policy" ? 
                   <View>
                   <Text style={{backgroundColor:'#f2f2f2',padding:10,marginTop:5,fontSize:23}}>
                     {this.props.navigation.state.params.title}
                    </Text>
                   <Text style={{paddingLeft:15,paddingTop:10,fontSize:14}}>Content WIP</Text>
                   </View> : */}
                    <WebView
                     source={{uri: this.props.navigation.state.params.url}}
                    startInLoadingState={true}
                    onLoadStart={this.renderLoading}/>
                   </Content>
      </Container> )
          
    }
}