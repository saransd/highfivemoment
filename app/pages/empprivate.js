import React from 'react';
import {Image,StyleSheet,ImageBackground,View,TextInput,Platform,Dimensions} from 'react-native';
import {Container,Header,Content,Grid,Row,Col,Text,CheckBox,ListItem,Body,Button,Picker,Form,Item} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import {ProfileBox,GiveHighFive} from '../components/empElements';
var window = Dimensions.get('window');

export default class EmpPrivate extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
    }


    render()
    {
        let textArr = ["Nurse Practitioner","Northwestern Pediatrics"];

        return(<Container style={[globalstyles.containerBgColor,{paddingLeft:15,paddingRight:15,paddingTop:40,paddingBottom:10}]}>
                <Content>
                  <Grid>
                     <ProfileBox textArr={textArr}/>
                     <GiveHighFive text="for Northwestern Pediatrics"/>
                    <Row style={[globalstyles.gridPadding,{alignItems:'center',justifyContent:'center'}]}>
                      <Col style={{height:150,alignItems:'center',justifyContent:'center',backgroundColor:'#F2F2F2'}}>
                        <View style={{marginLeft:30,alignItems:'center'}}>
                         <Text style={[globalstyles.text,globalstyles.subtext]}>THIS PROFILE IS</Text>
                         <Text style={[globalstyles.text,{fontSize:28}]}>PRIVATE</Text>
                         </View>
                      </Col>
                      <Col style={{height:150,alignItems:'center',justifyContent:'center',backgroundColor:'#F2F2F2'}}>
                        <Image style={{width:110,height:110}} source={require('../assets/images/caution.png')}/>
                      </Col>
                    </Row>
                   </Grid>
                 </Content>
               </Container>)
    }
}


