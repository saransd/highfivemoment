import React from 'react';
import {Image,StyleSheet,ImageBackground,View,TextInput,Platform,Dimensions} from 'react-native';
import {Container,Header,Content,Grid,Row,Col,Text,CheckBox,ListItem,Body,Button,Picker,Form,Item} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import {ProfileBox,HandBox,TrophyBox,CalendarBox,GiveHighFive} from '../components/empElements';
var window = Dimensions.get('window');

export default class OtherEmpTwo extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
    }


    render()
    {
        let textArr = ["Nurse","Northwestern Pediatrics","Waitress","Chilis"];

        return(<Container style={[globalstyles.containerBgColor,{paddingLeft:15,paddingRight:15,paddingTop:40,paddingBottom:10}]}>
                <Content>
                  <Grid>
                     <ProfileBox textArr={textArr}/>
                     <GiveHighFive text="for Northwestern Pediatrics"/>
                     <GiveHighFive text="for Chilis"/>
                    <Row style={globalstyles.gridPadding}>
                      <HandBox bgColor={styles.boxBgColor}/>
                      <TrophyBox bgColor={styles.boxBgColor}/>
                    </Row>
                    <Row style={[globalstyles.gridPadding,{alignItems:'center',justifyContent:'center'}]}>
                       <CalendarBox bgColor={styles.boxBgColor}/>
                    </Row>
                   </Grid>
                 </Content>
               </Container>)
    }
}

const styles = StyleSheet.create({
     boxBgColor : {
        backgroundColor : "#F0F0F0"
     }
})


