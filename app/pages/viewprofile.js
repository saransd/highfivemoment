import React from 'react';
import {Content,Item} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import ProfilePicker from '../components/picker';
import OtherEmp from './otheremp';
import CustProfile from './custprofile';
import Analytics from 'appcenter-analytics';
import {callQuery} from '../utils/query-handler';
import {GET_USERID_NAME} from '../constants';
import {getUsers} from '../actions';
import {connect} from 'react-redux';

let pageName = "Profile";

let eventName = "Get Users";

 class ViewProfile extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
        this.state = {
            selected : "Personal Profile",
        }
        this.onValueChange = this.onValueChange.bind(this);
    }

    
    onValueChange(value)
    {
        this.setState({selected:value});
        if(value === "Work Profile")
        {
           Analytics.trackEvent("WorkProfile");
        }
    }

    render()
    {
        let consumer = {}; 
        let employee = {}; 
        let events = [];
        let pickerOptions = ["Personal Profile","Work Profile"];
        if(this.props.profileObj && Object.keys(this.props.profileObj).length > 0)
        {
            consumer = this.props.profileObj.consumer;
            employee = this.props.profileObj.employee;
            events = this.props.profileObj.events ? this.props.profileObj.events : [];
        }
        let textArr = [""];
        return(<Content>
                <ProfilePicker value={this.state.selected} options={pickerOptions} type="profile" employee={employee} action={(value)=>{this.onValueChange(value)}}/>
                {this.state.selected === "Work Profile" ? <OtherEmp employee={employee} consumer={consumer} events={events} organizations={this.props.organizations} users={this.props.users}/> : <CustProfile consumer={consumer} organizations={this.props.organizations} users={this.props.users}/>}
               </Content>)
    }
}

const mapStateToProps = (state) => {
    return {
        organizations : state.employee.organizations,
        users : state.employee.users
    }
}

export default connect (mapStateToProps) (ViewProfile)
