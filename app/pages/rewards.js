import React from 'react';
import {Container,Content,Text,Grid,Row,Col,Button} from 'native-base';
import {SearchBox} from '../components/tabElements';
import _ from 'lodash';
import {globalstyles} from '../assets/css/styles';

export default class Rewards extends React.Component {
    static navigationOptions ={
        header:null
    }
    constructor(props)
    {
        super(props);
        this.state = {
          rewards :  [{title:'Pay It Forward',desc:'Every 5 High Fives given at CVS, $1 donated to Charity X',isButton:false},
                     {title:'Free Class',desc:'Every 5 High Fives given to instructors, get 1 free class',isButton:true},
                     {title:'Free Cup of Coffee',desc:'Every 5 High Fives given to Baristas, get 1 free latte',isButton:true}],
          filteredRewards : []
        }
        this.filterRewards = this.filterRewards.bind(this);
    }

    filterRewards(text)
    {
        let pattern = new RegExp(text,'i');
        let filteredRewards = _.filter(this.state.rewards,(item)=>{
            let reward = item.title.toLowerCase();
            if(reward.search(pattern)!==-1)
            {
                return reward;
            }
        })
      this.setState({filteredRewards:filteredRewards});

    }
    render()
    {  
       let rewards = this.state.filteredRewards.length > 0 ? this.state.filteredRewards : this.state.rewards;
        return(<Content style={{paddingRight:10,paddingLeft:10,paddingTop:20}}>
               <Text style={{fontSize:24,alignSelf:'center'}}>HIGH FIVE REWARDS</Text>
               <SearchBox icon="ios-search" placeholder="Search Rewards" styles={{backgroundColor:'#E5E5E5',alignSelf:'center',marginTop:10}} action={(text)=>{this.filterRewards(text)}}/>
               <Grid style={globalstyles.formElementMargin}>
                   {rewards.map((reward,index) => {
                  return <Row key={"rewards-"+index} style={!reward.isButton ? {marginBottom:15} : {marginBottom:10}}>
                       <Col style={{width:75,height:75,backgroundColor:'#A2D9EB',marginRight:5}}/>
                       <Col>
                          <Text style={{fontFamily:'Roboto',fontWeight:'800',fontSize:14}}>{reward.title}</Text>
                          <Text style={{fontFamily:'Roboto',fontSize:12}}>{reward.desc}</Text>
                         {reward.isButton ? <Button rounded style={{backgroundColor:'#334149',alignSelf:'flex-end',height:30}}>
                             <Text style={{fontSize:12,color:'#FFF'}}>REDEEM</Text>
                          </Button> : null}
                       </Col>
                   </Row>
                   })}
               </Grid>
           </Content>
        )
    }
}