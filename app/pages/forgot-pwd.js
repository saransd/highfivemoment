import React from 'react';
import {View,Alert} from 'react-native';
import { Container, Header,Content,Button,Text,Spinner } from 'native-base';
import {TextBox, TextBoxForgotPass, SubmitButton,PwdTextBlock,PwdTextBlockForgot,FormFooter,ErrorText} from '../components/formElements';
import {Loader} from '../components/loader';
import {globalstyles} from '../assets/css/styles';
import {GetLogo} from '../components/get_logo.js';
import firebase from '../utils/firebase-config';
import styles from '../assets/css/login';

export default class ForgotPassword extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props)
    {
        super(props)
        this.state = {
            loading:false,
            email:'',
            errorText:""
        }
        this.gotoResetPwd = this.gotoResetPwd.bind(this);
    }

    gotoResetPwd()
    {
      if(this.state.email)
      {
         /* var user = firebase.auth().currentUser;
         if(user !== null && user.email === this.state.email)
         {
           this.setState({loading:true});
          user.sendEmailVerification().then(()=> {
             this.props.navigation.navigate("ResetPwd",{email:this.state.email});
             this.setState({errorText:"",loading:false});
           }).catch((error)=> {
               console.log("%%%%%%%%%%%%%%%%%%%%")
               console.log(error);
               this.setState({errorText:"Please enter a valid email",loading:false});
          })
         }*/
          firebase.auth().sendPasswordResetEmail(this.state.email).then(()=> {
            Alert.alert(
             '',
            'Please check your email and follow the instructions.',
            [{text: 'Ok', onPress: () => this.props.navigation.navigate("Login")}],
            { cancelable: false }
            )
             this.setState({errorText:"",loading:false});
           })
      }
      else {
         this.setState({errorText:"Please fill the email field"})
      }
    // firebase.auth().sendPasswordResetEmail(this.state.email).then(function() {
        
      //}.bind(this)).catch(function(error) {
       //  console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@2");
       //  console.log(error)
      //});
    }

    render()
    {
        return(
          <Container style={globalstyles.containerBgColor}>
              {this.state.loading ? <Loader/> : null}
          <Content contentContainerStyle={globalstyles.formContent}>
             <View style={[styles.formWidth]}>
              <GetLogo styles={[styles.thfLogoForm,styles.logo]}/>
              <PwdTextBlockForgot text="Forgot Your Password?"/>
               <TextBoxForgotPass 
                  placeholder="email" 
                  value={this.state.email} 
                  changeText={(email)=>this.setState({email})} 
                  page="forgot" 
                  keyboardType="email-address"/>
              <ErrorText error={this.state.errorText}/>
              <SubmitButton text="Submit" styles={styles.loginButton} page="forgot" action={()=>{this.gotoResetPwd()}}/>
             </View>
          </Content>
          <FormFooter navigation={this.props.navigation} route="SignUp" text="Don't have an account? Sign Up here"/>
          </Container>
        )
    }
}