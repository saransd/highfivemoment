import React from 'react';
import {ImageBackground,AsyncStorage,Platform,View} from 'react-native';
import { Container, Header, Content, List, ListItem,Switch,Text,Left,Right,Body,Item,Form,Footer } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from '../utils/firebase-config';
import {SettingsList} from '../components/settings-list';
import VersionNumber from 'react-native-version-number';
import styles from '../assets/css/settings';

export default class Settings extends React.Component{
    static navigationOptions = {
        header : null      
    }
    constructor(props)
    {
        super(props);
    }

   render()
   {
        let consumer = {}; 
        let employee = {}; 
        if(this.props.profileObj && Object.keys(this.props.profileObj).length > 0)
        {
            consumer = this.props.profileObj.consumer;
            employee = this.props.profileObj.employee;
        }

       return(  <Content>
           {/*<ImageBackground style={{flex: 1, width:undefined, height:undefined}} source={require('../assets/images/signup_bg.png')}>*/} 
            <Header style={styles.header}><Text style={styles.fontHead}>SETTINGS</Text></Header> 
               <SettingsList navigation={this.props.navigation} employee={employee}/>
           {/*</ImageBackground>*/}
           <Footer style={styles.footer}><Text style={styles.fontHead}>VERSION {VersionNumber.appVersion}</Text></Footer>
        </Content>)
   }
}

