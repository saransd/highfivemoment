import React from 'react';
import {View} from 'react-native';
import { Container, Header,Content,Button,Text,Spinner } from 'native-base';
import {TextBox,SubmitButton,PwdTextBlock,FormFooter,ErrorText} from '../components/formElements';
import {Loader} from '../components/loader';
import {globalstyles} from '../assets/css/styles';
import {GetLogo} from '../components/get_logo.js';
import firebase from '../utils/firebase-config';
import styles from '../assets/css/login';

export default class ResetPassword extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props)
    {
        super(props)
        this.state = {
            loading:false,
            email:props.navigation.state.params.email,
            newPassword:'',
            confirmPassword:'',
            error:""
        }
        this.resetPwd = this.resetPwd.bind(this);
    }

    resetPwd()
    {
      if(this.state.newPassword && this.state.confirmPassword)
      {
       var user = firebase.auth().currentUser;
      /* var credential = firebase.auth.EmailAuthProvider.credential(
       this.state.email,
       this.state.newPassword
       );
       console.log("****************************")
       console.log(credential);

      // Prompt the user to re-provide their sign-in credential
     user.reauthenticateWithCredential(credential).then(function() {
     // User re-authenticated.
       }).catch(function(error) {
           console.log("^^^^^^^^^^^^^^^^^^^^");
           console.log(error);
         // An error happened.
       });*/

      var newPassword = this.state.newPassword

    if(this.state.newPassword === this.state.confirmPassword)
    {
       this.setState({loading:true});
       user.updatePassword(newPassword).then(()=>{
           this.setState({loading:false});
          console.log("%%%%%%%%%%%%%%%%%%%%%%%% updated");
          this.props.navigation.navigate("Login");
     // Update successful.
        }).catch((error)=> {
            console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%");
            console.log(error);
            let resetError = "Password reset failed. Please try again";
             for(var e in error)
             {
                if(e === "message")
                {
                    resetError = error[e];
                 }
             }
             console.log(resetError)
            this.setState({error:resetError,loading:false});
       // An error happened.
      });
    }
       else
       {
        this.setState({error:"Password does not match"});
       }
    }
    else{
        this.setState({error:"Please fill all the fields"});
    }
    }

    render()
    {
        return(
          <Container style={globalstyles.containerBgColor}>
              {this.state.loading ? <Loader/> : null}
          <Content contentContainerStyle={globalstyles.formContent}>
             <View style={styles.formWidth}>
              <GetLogo styles={[styles.thfLogoForm,styles.logo]}/>
              <PwdTextBlock text="Reset Password"/>
              <TextBox placeholder="Enter new password" value={this.state.newPassword} changeText={(newPassword)=>this.setState({newPassword})} page="reset" keyboardType="default" secure={true}/>
              <TextBox placeholder="Confirm new password" value={this.state.confirmPassword} changeText={(confirmPassword)=>this.setState({confirmPassword})} page="reset" keyboardType="default" secure={true}/>
              <ErrorText error={this.state.error}/>
              <SubmitButton text="Reset" styles={styles.loginButton} page="reset" action={()=>{this.resetPwd()}}/>
             </View>
          </Content>
          <FormFooter navigation={this.props.navigation} route="SignUp" text="Don't have an account? Sign Up here"/>
          </Container>
        )
    }
}