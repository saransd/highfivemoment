import React from 'react';
import {Image,StyleSheet,Dimensions,TouchableOpacity,AsyncStorage,RefreshControl,Platform,PixelRatio} from 'react-native';
import {Container,Content,Text,Grid,Row,Col,List,ListItem,Body,Right} from 'native-base';
import {SearchBox,BackArrow} from '../components/tabElements';
import FooterComponent from '../components/footer';
import {EmpListItem} from '../components/empElements';
import {globalstyles} from '../assets/css/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import {QUERY_EMPLOYEES} from '../constants';
import {Query} from 'react-apollo';
import {client,callQuery,callMutation,callWatchQuery,GenerateConsumerHighFive,callQueryNetwork} from '../utils/query-handler';
import {QUERY_EVENTS,QUERY_ADD_EVENT,QUERY_UPDATE_EVENT,QUERY_ORG_SEARCH,QUERY_USERS_ORGID,QUERY_ORG_AND_EMPLOYEES} from '../constants';
import {createMutationObject,createUpdateMutationObject} from '../utils/mutation-object';
import {Loader} from '../components/loader';
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
import { withNavigation } from 'react-navigation';
import Analytics from 'appcenter-analytics';
import gql from "graphql-tag";
import _ from 'lodash';
import moment from 'moment';
import  momentTimeZone from 'moment-timezone';
var DeviceInfo = require('react-native-device-info');
import Firebase from '../utils/nativefirebase-config';

const pageName = "Organization";
const eventName = "onLoad";

var profilepicHeight;//100
var widthAndroid = (deviceWidth/2.5)-(deviceWidth/30);//161
var heightAndroid;//150
var imageSize1;//30
var fsize;//20
var fsize2;//14
var serachBarWidth;
var testMarginTop;

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      heightAndroid = deviceHeight/6
      imageSize1 = 60
      fsize = 22
      fsize2 = 18
      profilepicHeight = deviceHeight/8
      serachBarWidth = deviceWidth/1.6
      testMarginTop = deviceHeight/40
    }
    else{
      heightAndroid = deviceHeight/6
      imageSize1 = 55
      fsize = 20
      fsize2 = 16
      profilepicHeight = deviceHeight/8
      serachBarWidth = deviceWidth/1.6
      testMarginTop = deviceHeight/35
    }
  }
  else{
    testMarginTop = deviceHeight/35
    serachBarWidth = deviceWidth/1.1
    imageSize1 = 50
    fsize = 19
    fsize2 = 14
    profilepicHeight = deviceHeight/8
    heightAndroid = deviceHeight/6
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      heightAndroid = deviceHeight/6.5
      imageSize1 = 30
      fsize = 14
      testMarginTop = deviceHeight/50
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      heightAndroid = deviceHeight/6.5
      imageSize1 = 35
      fsize = 16
      testMarginTop = deviceHeight/40
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      imageSize1 = 40
      fsize = 17
      fsize2 = 15
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      imageSize1 = 45
      fsize = 18
      fsize2=15
    }
  }

export default class Organization extends React.Component{
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
        this.state = {
            org : props.navigation.state.params.org,
            img : props.navigation.state.params.img,
            filteredEmp : [],
            employees:[], //props.navigation.state.params.employees,
            orgHighFive: "",//props.navigation.state.params.orgHighFive,
            events : [],
            loading:false,
            latitude:0,
            longitude:0,
            refreshing:false
        }
        this.filterEmp = this.filterEmp.bind(this)
    }


    getOrganizationData = () => {
     let orgId = this.state.org.id;   
     let orgName = this.state.org.name;
      callQueryNetwork(QUERY_ORG_AND_EMPLOYEES(orgName,orgId),pageName,eventName,(result)=>{
          let organizationResult = result.data.organizations[0];
          this.setState({orgHighFive: organizationResult ?  organizationResult.organization.highFiveScore: 0 });
       });
      callQueryNetwork(QUERY_USERS_ORGID(orgId),pageName,eventName,(result)=>{ 
          this.setState({employees:result.data.users,loading:false,refreshing:false});
      })
    }

   componentDidMount()
    {
    this.setState({loading:true});
    let orgId = this.state.org.id;
       /*callQuery(QUERY_EMPLOYEES(orgId),(result)=>{
          this.setState({employees:result.data.employees,loading:false });
       });*/
       this.getOrganizationData();

      /*function refreshCount(_SELF){
           client.resetStore().then(()=>{
            callQuery(QUERY_ORG_SEARCH(orgName),(result)=>{
                _SELF.setState({orgHighFive:result.data.organizationSearch[0].totalHighFive,loading:false });
             });
           });
       }

       setInterval(()=>{refreshCount(this)},600000)*/
  
       /*callQuery(QUERY_EVENTS,(result)=>{
          this.setState({events:result.events,loading:false });
       });*/

    
          /* let options = {
            enableHighAccuracy : false,
            //timeout : 5000,
           //maximumAge : 0
           }
       
           error = (e) => {
            console.log(e.message);
           }
   
          success = (pos) => {
            let crd = pos.coords;
            this.setState({latitude:Number(crd.latitude),longitude:Number(crd.longitude)});
         }
   
     navigator.geolocation.getCurrentPosition(success,error,options);*/
    }

    filterEmp(text)
    {
      let pattern = new RegExp(text,'i');
      let filteredEmp = _.filter(this.state.employees,(emp)=>{
           let empName = emp.givenName.toLowerCase() + " " + emp.familyName.toLowerCase();
           if(empName.search(pattern)!==-1)
           {
               return empName;
           }
      });
      this.setState({filteredEmp:filteredEmp});
    }

 giveHighFive = (empInfo,org) => {

  /*AsyncStorage.getItem('current-user').then((value) => {
   let user = JSON.parse(value);
   let totalHighFives = empInfo.totalHighFive ? empInfo.totalHighFive.totalHighFives  : 0;
   let date = moment().format("YYYY-MM-DDTHH:mm:ss");
   let datetime = momentTimeZone.tz(date, "America/Chicago").utc().format();*/
   let hasEvent = false;
   let eventId = "";
    
   this.sendNotification(empInfo);
    
/*let obj = {
      employeeId:empInfo.employeeId,
      patronId: user ? user.uid : "",
      givenName:user ? user.email.split('@')[0] : "",
      familyName:user ? user.email.split('@')[0] : "",
      eventDate : datetime,
      latitude : this.state.latitude,
      longitude : this.state.longitude,
      totalHighFives: totalHighFives,
      attributeBreakdown:[]
    }*/

    let empId = empInfo.directoryId;

    /* this.state.events.map((event) => {
        if(event && event.employeeInfo && event.employeeInfo.employeeId && event.employeeInfo.employeeId === empId)
         {
             hasEvent = true;
             eventId = event.id;
         }
    });*/

 let totalHighFive = empInfo.totalHighFive ? empInfo.totalHighFive  : 0;

   /*if(hasEvent)
    {
     createUpdateMutationObject(eventId,[],"",(obj)=>{
         callMutation(QUERY_UPDATE_EVENT(obj),obj,(result)=>{
          console.log(result)
          let events = this.state.events.slice(); 
          events.push(result.data.addEvent);
          this.setState({events:events});
          let updatedHighfive = totalHighFive;//result.data.updateEvent.totalHighFive.totalHighFives;
          this.props.navigation.navigate("AfterHighfive",{empInfo:empInfo,org:org,events:events,totalHighFives:updatedHighfive,hasEvent:true,eventId:eventId});
           });
     });
    }*/
   // else{
      //createMutationObject(empInfo,totalHighFive,(obj)=>{
        //callMutation(QUERY_ADD_EVENT(obj),obj,(result)=>{
        //console.log(result) 
         //let events = this.state.events.slice(); 
         //events.push(result.data.addEvent);
        //this.setState({events:events});
       //let eventId = result.data.addEvent.id;
      // let updatedHighfive = totalHighFive;//result.data.addEvent.totalHighFive.totalHighFives;
       //GenerateConsumerHighFive();
       this.props.navigation.navigate("AfterHighfive",{empInfo:empInfo,
                                                       org:org,
                                                       events:[],
                                                       eventId:"",
                                                       totalHighFives:totalHighFive,
                                                       hasEvent:false,
                                                       getOrganizationData:this.getOrganizationData
                                                       });
        // });
      //});
   // }
    //})

    Analytics.trackEvent("InstantHF");
    }

    sendNotification(empInfo){
      console.log('sendNotification Organization');
      Firebase.database().ref('devicetoken/'+empInfo.employeeId).on("value", snapshot => {
      var tokenval=snapshot.val();
      console.log("userid"+JSON.stringify(tokenval));
      if(tokenval!=null){
        var notification={
          to: tokenval.token,
          notification: {
            title: "Notification",
            body: "Your scoring is pending",
            sound: "default",
          },
          data: {
            targetScreen: "detail"
          },
          priority: "high"
        };
        fetch("https://fcm.googleapis.com/fcm/send", {
          method: 'post',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            //Key = Server Key  from FirebaseApp==>Settings==>Cloud Messaging==>Server Key
            "Authorization": "key=AAAA-UP5J3U:APA91bGnDf3Ts-JbD9xsZSC7NodsPgJQQDRH8t5sWoD_hePXq5GdQrz4KfBZMjA-ixmod5EnQL6SunUUI3-VW6HZowtxkdSLdrr4czaEzu7Etwz4yyVOU62jBFKwiPF6kI-xW5uOrPPE"
          },
          body: JSON.stringify(notification),
        })
        .then(res=>{
          console.log('list product _response'+JSON.stringify(res));
          return res.json();
        })
        .then(json=>{
          // Alert.alert("json"+JSON.stringify(json));
          Alert.alert("Notification Sent");
        })
        .catch(error => {
          console.log ('Error : '+JSON.stringify(error));
          console.log ( 'Notification sent Failed.');
        });
      }
    });
    }
    render()
    {  
        let org = this.state.org;
        let imgsrc = org.organizationImageUrl ? {uri : org.organizationImageUrl} : require('../assets/images/kroger.png') ;
        let text = org.text ? org.text : "";
        let distance = org.distance ? org.distance : "";
        let orgAddressLine1 = org.address.number+" "+org.address.street;
        let orgAddressLine2 = org.address.street2+" "+org.address.city;
        let employee = this.state.filteredEmp.length > 0 ? this.state.filteredEmp : this.state.employees;
        let empInfo = [];
        //let query = gql`${QUERY_EMPLOYEES(org.organizationId)}`;
        let orgHighFive = this.state.orgHighFive ? this.state.orgHighFive : 0;
        return(
         <Container style={globalstyles.containerBgColor}>
             {this.state.loading ? <Loader/> : null}
        <Content 
         removeClippedSubviews={true}
            refreshControl={
              <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={()=>{this.getOrganizationData()}}
              />}>
         <Grid style={{paddingLeft:5,paddingTop:deviceHeight/60}}>
              <Row>
                 <BackArrow action={()=>{this.props.navigation.goBack()}}/>
                 <Col style={{width:widthAndroid,height:heightAndroid,alignItems:'center',justifyContent:'center'}}>
                   <Image style={{width:profilepicHeight,height:profilepicHeight,resizeMode:'contain'}} source={imgsrc}/>
                 </Col>
                 <Col>
                   <Text style={[styles.text,{fontWeight:'800'}]}>{text}</Text>
                   <Text style={{fontSize:fsize2}}>{orgAddressLine1}</Text>
                    <Text style={{fontSize:fsize2}}>{orgAddressLine2}</Text>
                   <Text style={{fontSize:fsize2}}>{distance}</Text>
                   <Row style={{paddingTop:deviceHeight/80}}>
                    <Image style={{height:imageSize1,width:imageSize1,resizeMode:'contain'}} source={require('../assets/images/hand_nobox.png')}/>
                    <Text style={{fontSize:fsize2}}>{orgHighFive}</Text>
                    </Row>
                 </Col>
             </Row>
         </Grid>    
         <SearchBox icon="ios-search" placeholder="Search Employees" styles={styles.searchBox} action={(text)=>{this.filterEmp(text)}}/>
         <List>
           
    {/*<Query
    query={query}
    pollInterval={500}>
    {({ loading, error, data }) => {
      if (loading) return null;
      if (error) return `Error!: ${error}`;
        
    return data.employees.map((emp,index)=> {
            return <EmpListItem key={"emp-list-"+index} emp={emp}  action1={()=>{this.props.navigation.navigate("Highfive",{empInfo:emp,org:org,events:this.state.events})}} action2={()=>{this.giveHighFive(emp,org)}}/>
             })}
                }
  </Query>*/}
         {employee.map((emp,index) => {
          if(emp.employment && emp.employment.length > 0)
          {
         return emp.employment.map((job,j)=>{
           if(job.organization.id === org.id)
           {
             return <EmpListItem key={"emp-list-"+index} emp={emp} org={org} action1={()=>{ Analytics.trackEvent("EmployeeP"); this.props.navigation.navigate("Highfive",{empInfo:emp,org:org,getOrganizationData:this.getOrganizationData})}} action2={()=>{this.giveHighFive(emp,org)}}/>
           }
          })
          }
         }) }
         </List>
         </Content> 
         <FooterComponent/>
         </Container>
        )
    }
}

let styles = StyleSheet.create({
    text:{
      fontFamily:'Roboto',
      fontSize:18
    },
    subText:{
     fontFamily:'Roboto',
     fontSize:12
    },
    searchBox:{
      width:serachBarWidth,
      backgroundColor:'#E5E5E5',
      alignSelf:'center',
      marginTop: deviceHeight/50,
      marginBottom:testMarginTop,
    },
})


