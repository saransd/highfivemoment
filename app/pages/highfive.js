import React from 'react';
import {Image,StyleSheet,ImageBackground,View,TextInput,Platform,Dimensions,Animated,AsyncStorage,RefreshControl,PixelRatio} from 'react-native';
import {Container,Header,Content,Grid,Row,Col,Text,CheckBox,ListItem,Body,Button,Picker,Form,Item} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import {ProfileBox,HandBox,CalendarBox,GiveHighFive,TrophyBoxCalendar,FeedbackTextView} from '../components/empElements';
import {BackArrow} from '../components/tabElements';
import FooterComponent from '../components/footer';
import {withNavigation} from 'react-navigation';
import { Query,Mutation } from "react-apollo";
import {callQuery,callMutation,client,callWatchQuery,GenerateConsumerHighFive,callQueryNetwork} from '../utils/query-handler';
import gql from "graphql-tag";
import {QUERY_EVENTS,QUERY_ADD_EVENT,QUERY_UPDATE_EVENT,GET_HIGHFIVES,QUERY_EMP_ATTRIBS,QUERY_GET_EMP_INFO} from '../constants';
import {createMutationObject,createUpdateMutationObject} from '../utils/mutation-object';
import {GetMonthAndYear} from '../utils/date-functions';
import {getJobTitle} from '../utils/getName';
import HandBoxEmp from '../components/handbox-emp';
import Analytics from 'appcenter-analytics';
var window = Dimensions.get('window');
import moment from 'moment';
import  momentTimeZone from 'moment-timezone';
import {Loader} from '../components/loader';

const pageName = "Highfive Profile";

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
var DeviceInfo = require('react-native-device-info');


export default class Highfive extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
        this.state = {
            selected : "",
            empInfo : props.navigation.state.params.empInfo,
            org : props.navigation.state.params.org,
            events : props.navigation.state.params.events,
            getOrganizationData : props.navigation.state.params.getOrganizationData ? props.navigation.state.params.getOrganizationData : null,
            hasEvent:false,
            empAttributes:[],
            eventId: "",
            latitude:0,
            longitude:0,
            totalHighFive:0,
            refreshing:false,
            loading:false
        }
        this.springValue = new Animated.Value(1);
        this.onValueChange = this.onValueChange.bind(this);
        this.spring = this.spring.bind(this);
    }

    componentDidMount()
    {
       let empId = this.state.empInfo.id;
       let hasEvent = false;
           /* this.state.events.map((event) => {
                 if(event.employeeInfo && event.employeeInfo.employeeId && event.employeeInfo.employeeId === empId)
                 {
                   this.setState({hasEvent:true,eventId:event.id});
                 }
            })*/
              callQueryNetwork(QUERY_GET_EMP_INFO(empId),pageName,"get emp Info",(result)=>{
                 this.setState({events:result.data.events,loading:false});
              })
      
        /*callWatchQuery(GET_HIGHFIVES(empId),(data)=>{
            this.setState({totalHighFive:data.employee.totalHighFive});
        })*/
    }
     
    onValueChange(value)
    {
        this.setState({selected:value});
    }

 spring () {
 
   /*AsyncStorage.getItem('current-user').then((value) => {
   let user = JSON.parse(value);
   let totalHighFives = this.state.totalHighFive;
   let date = moment().format("YYYY-MM-DDTHH:mm:ss");
   let datetime = momentTimeZone.tz(date, "America/Chicago").utc().format();*/
   /*let obj = {
     employeeId : this.state.empInfo.employeeId,
     patronId : user.uid,
       givenName: user.email.split('@')[0],
       familyName : user.email.split('@')[0],
     eventDate: date,
        latitude : 38.8951,
        longitude : -77.0364,
      totalHighFives:totalHighFives,
      attributeBreakdown:[]
    }*/

    let totalHighFive = this.state.totalHighFive ?  this.state.totalHighFive : this.state.empInfo.totalHighFive ? this.state.empInfo.totalHighFive  : 0;

  // createMutationObject(this.state.empInfo,this.state.totalHighFive,(obj)=>{
    /*if(this.state.hasEvent)
    {
     createUpdateMutationObject(this.state.eventId,[],"",(obj)=>{
     callMutation(QUERY_UPDATE_EVENT(obj),obj,(result)=>{
     console.log(result) 
      let events = this.state.events.slice(); 
      events.push(result.data.addEvent);
     let updatedHighfive = totalHighFive;//result.data.updateEvent.totalHighFive.totalHighFives;
     this.setState({events:events,totalHighFive:updatedHighfive});
     this.props.navigation.navigate("AfterHighfive",{empInfo:this.state.empInfo,org:this.state.org,events:events,totalHighFives:updatedHighfive,hasEvent:true,eventId:this.state.eventId});
    });
     });
    }*/
    //else{
      //createMutationObject(this.state.empInfo,this.state.totalHighFive,(obj)=>{
    // callMutation(QUERY_ADD_EVENT(obj),obj,(result)=>{
     //console.log(result) 
      // let events = this.state.events.slice(); 
      //events.push(result.data.addEvent);
     //let updatedHighfive = totalHighFive; //result.data.addEvent.totalHighFive.totalHighFives;
    // let eventId = result.data.addEvent.id;
     //this.setState({events:events,totalHighFive:updatedHighfive});
     //GenerateConsumerHighFive();
     this.props.navigation.navigate("AfterHighfive",{empInfo:this.state.empInfo,org:this.state.org,events:[],eventId:"",totalHighFives:totalHighFive,
                                                      hasEvent:false,getOrganizationData:this.state.getOrganizationData});
    //});
    //  });
   // }
 //  }); 
/*let obj = {
      employeeId:this.state.empInfo.employeeId,
      patronId: user ? user.uid : "",
      givenName:user ? user.email.split('@')[0] : "",
      familyName:user ? user.email.split('@')[0] : "",
      eventDate : datetime,
      latitude : this.state.latitude,
      longitude : this.state.longitude,
      totalHighFives: totalHighFives,
      attributeBreakdown:[]
    }

  
    var _SELF = this;
    this.springValue.setValue(0.3);
    Animated.spring(
      this.springValue,
      {
        toValue: 1,
        friction: 1,
        tension: 1
      }
    ).start((anim)=>{
        if(anim.finished)
        {
          this.props.getContent(8);
        }
    });
  })*/
  
  Analytics.trackEvent("HighFiveP");
    
}


    render()
    {
      
      let org = this.state.org;
      let employeeId = this.state.empInfo.id;
      let employment = this.state.empInfo.employment;
      let attributes = this.state.empInfo.attributesScore ? this.state.empInfo.attributesScore : [];
      let textArr = [getJobTitle(employment,org),org.name];
      let high_five_text = `for ${org.name}`;
      let query = gql`${GET_HIGHFIVES(employeeId)}`;
      let totalHighFives = this.state.totalHighFive ? this.state.totalHighFive : this.state.empInfo.highFiveScore ? this.state.empInfo.highFiveScore : 0;
      let date = GetMonthAndYear(this.state.empInfo.joined);
      let year = date.year;
      let month = date.month; 
      let events = this.state.events;
        
        return(<Container style={[globalstyles.containerBgColor]}>
               {this.state.loading ?  <Loader/> : null }
                <Content 
                  style={{paddingTop:20,paddingLeft:15,paddingRight:15,paddingBottom:10}} 
                   removeClippedSubviews={false}
                 >
                  <BackArrow action={()=>{this.props.navigation.goBack()}}/>
                  {/*<Form style={{alignItems:'flex-end'}}>
                    <Picker
                    mode="dropdown"
                    placeholder="Select One"
                    selectedValue={this.state.selected}
                    onValueChange={this.onValueChange.bind(this)}
                    style={{width:170,height:40,backgroundColor:'#334149',alignItems:'center',justifyContent:'center'}}
                    itemStyle={{backgroundColor:'#334149'}}
                    textStyle={{color:'#FFF'}}
                    itemTextStyle={{color:'#FFF'}}>
                     <Item label="Work Account" value="key0" />
                     <Item label="Personal Account" value="key1" />
                    </Picker>
                   </Form>*/}
                  <Grid style={{paddingBottom:deviceHeight/20}}>
                      <ProfileBox textArr={textArr} empInfo={this.state.empInfo}/>
                      <GiveHighFive text={high_five_text} 
                                    empInfo={this.state.empInfo} 
                                    action={()=>{this.spring()}} 
                                    springValue={this.springValue}
                                    addEmp={false}
                                    />
                    <Row style={globalstyles.gridPadding}>
                       {/*<HandBox highfives={totalHighFives}/>*/}
                       <HandBoxEmp empId={employeeId} queryConstant={GET_HIGHFIVES(employeeId)} totalHighFive={totalHighFives} hasEvent={this.state.hasEvent} pageName={pageName}/>
                       <TrophyBoxCalendar month={month} year={year} attributes={attributes} workProfile={true}/>
                    </Row>
                     <Row style={{paddingTop:deviceHeight/50}}>
                        <FeedbackTextView events={events} fbType="emp"/>
                    </Row>
                    {/*<Row style={[globalstyles.gridPadding,{alignItems:'center',justifyContent:'center'}]}>
                      <CalendarBox/>
                    </Row>*/}
                   </Grid>
                   </Content>
                   <FooterComponent/>
               </Container>)
    }
}


const styles = StyleSheet.create({
    checkboxCol:{
        width:50
    },
    checkboxUnchecked : {
      borderColor:'transparent',
      borderRadius:30,
      backgroundColor:"#E5E5E5",
      width:50,
      height:50
    },
    checkboxChecked : {
      borderColor:'transparent',
      borderRadius:30,
      backgroundColor:"#1CBECA",
      width:50,
      height:50,
      alignItems:'center',
      justifyContent:'center'
    },
    checkboxText : {
        color:'#FFF',
        fontFamily : 'Roboto',
        fontStyle: 'normal',
        fontSize: 11,
        lineHeight: 15,
        textAlign:'center'
    }
})


