import React from 'react';
import {View,Image,TouchableOpacity,ImageBackground,StyleSheet,AsyncStorage,Platform,RefreshControl,PixelRatio,AppState} from 'react-native';
import { Container,Content,Header, Item, Input, Icon, Button, Text,List,ListItem,Left,Body,Right } from 'native-base';
import {SearchBoxHome} from '../components/tabElements';
import {Loader} from '../components/loader';
import {EmpListItem} from '../components/empElements';
import {SEARCH_PLACEHOLDER,QUERY_ORGANIZATIONS,QUERY_ORG_SEARCH,QUERY_USERS,QUERY_EVENTS,QUERY_ADD_EVENT,QUERY_UPDATE_EVENT,ORG_SEARCH_DISTANCE} from '../constants';
import {OfflineImage,OfflineImageStore} from 'react-native-image-offline';
import {callMutation} from '../utils/query-handler';
import {createMutationObject,getGeoInfo,createUpdateMutationObject} from '../utils/mutation-object';
import {LocalImages} from '../components/base64-imgs';
import { withNavigation } from 'react-navigation';
import {connect} from 'react-redux';
import Analytics from 'appcenter-analytics';
import {client,callQuery,callWatchQuery,GenerateConsumerHighFive,persistor} from '../utils/query-handler';
import {getOrganizations} from '../actions';
import styles from '../assets/css/search';
import gql from "graphql-tag";
import _ from 'lodash';
import 'whatwg-fetch';
import Firebase from '../utils/nativefirebase-config';

const pageName = "Search"
const eventName = "onLoad"

class Search extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props)
    {
      super(props)
       this.state = {
         cardlist : [],
         location:'',
         searchValue:'',
         filteredList:[],
         reStoreCompleted: false,
         images : {},
         filteredOrgList:[],
         employees:[],
         events:[],
         loading:false,
         refreshing : false,
         latLong : "",
         locationError:"",
         locationPlaceHolder:"Current Location",
         appState : AppState.currentState
        }
        this.handleSearch = this.handleSearch.bind(this);
        this.cacheImages = this.cacheImages.bind(this);
    }

    callOrganizationsQuery = (location,latitude,longitude,searchText) => {
         callQuery(QUERY_ORGANIZATIONS(location,latitude,longitude,searchText),pageName,eventName,(result)=>{
           this.props.dispatch(getOrganizations(result.data.organizations));
          //order.map((num) => tempList.push(result.data.organizationSearch[num-1]));
          if(location && result.data.organizations.length === 0)
          {
              this.getOrganizationData("",{latitude:0,longitude:0},null);
          }
          else{
          this.setState({cardlist:result.data.organizations,refreshing:false,loading:false });
          this.props.isLoading(false);
        }
       })

    }


    getOrganizationData = (text,latLong,location,searchValue) => {
       let latitude = 0;
       let longitude = 0;
       let locationObj = {};
       let searchText = searchValue ? searchValue : "";
       if(location || (this.state.location && location))
       {  
        latitude = latLong ? latLong.latitude : this.state.latLong ? this.state.latLong.latitude : 0;
        longitude = latLong ? latLong.longitude : this.state.latLong ? this.state.latLong.longitude : 0;
        locationObj = {
           lat:latitude, 
           lng:longitude,
           maxDistance:ORG_SEARCH_DISTANCE}
       }
       //this.setState({loading: searchValue ? false : true});
       this.props.isLoading(searchValue ? false : true);
       let tempList = [];
       let order = [2,1,5,6,4,3];
       if(text === "refresh")
       {
          /*client.resetStore().then(()=>{
           callQuery(QUERY_ORG_SEARCH("."),(result)=>{
          //order.map((num) => tempList.push(result.data.organizationSearch[num-1]));
          this.setState({cardlist:result.data.organizationSearch,loading:false,refreshing:false });
            });
          });*/
       }
       else{
         this.callOrganizationsQuery(location || (this.state.location && location) ? locationObj : null,latitude,longitude,searchText);
       /*callQuery(QUERY_USERS(),(result)=>{
          this.setState({employees:result.data.users})
       })*/
       }
    }

     componentDidMount()
     {   
       this.setState({loading:true});
       this.setLocationPlaceholder();
       if(this.props.fromTab)
       {
         this.getOrganizationData("load");
       }
       else{
         this.props.isLoading(true);
         this.props.getProfile((isComplete)=>{ 
           if(isComplete)
           {
             this.getLocation();
           }   
         })
       }

       AppState.addEventListener('change', this._handleAppStateChange);
        
        /*callQuery(QUERY_EVENTS,(result)=>{
          this.setState({events:result.events});
       });*/

     // if(this.props.screenProps && this.props.screenProps.cardlist && this.props.screenProps.images )
     // {
       //this.getContent(3,JSON.parse(this.props.screenProps.cardlist),JSON.parse(this.props.screenProps.images));
     //  this.setState({cardlist:JSON.parse(this.props.screenProps.cardlist),images:JSON.parse(this.props.screenProps.images)});
     //  this.cacheImages(JSON.parse(this.props.screenProps.images));
     // }
       //if(!this.props.screenProps.cardlist)
      // {
        //this.setState({loading:true});
      // AsyncStorage.getItem('cardlist').then((cardlist)=>{
        //   AsyncStorage.getItem('images').then((images)=>{
      //  if(!cardlist) {
       //     this.setState({loading:true});
       //  fetch('https://high-five-data.herokuapp.com/posts','get').
       //  then((response)=>response.json()).
       //   then((response)=>{
       //   AsyncStorage.setItem('cardlist',JSON.stringify(response));
       //   if(images)
       //   {
        //   this.setState({cardlist:response,images:JSON.parse(images),loading:false});
        //   this.cacheImages(JSON.parse(images));
        //  }
        // console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        // console.log(response);
        // if(!images)
       //  {
       //  fetch('https://high-five-data.herokuapp.com/images','get').
        // then((res)=>res.json()).
        //  then((res)=>{
          //  AsyncStorage.setItem('images',JSON.stringify(res));
          // this.setState({cardlist:response ? response : JSON.parse(cardlist) ,images:res,loading:false});
          // this.cacheImages(res);           
         //  });
        // }

       // });
    //}
     //if(cardlist && !images)
    // {
      //    fetch('https://high-five-data.herokuapp.com/images','get').
        //  then((res)=>res.json()).
         // then((res)=>{
         //   AsyncStorage.setItem('images',JSON.stringify(res));
         //  this.setState({cardlist: JSON.parse(cardlist) ,images:res,loading:false});
          // this.cacheImages(res); 
          // });
     //}
        /*else{
         this.setState({cardlist:JSON.parse(cardlist),loading:false});
         this.getContent(3,JSON.parse(cardlist));
        }*/
      //  if(cardlist && images)
      //  {
      //     this.setState({cardlist:JSON.parse(cardlist),images:JSON.parse(images),loading:false});
      //     this.cacheImages(JSON.parse(images));
      //  }
      //     });
     //  });
      // }
     // setTimeout(function(){this.setState({isVisible:false})}.bind(this),3000);
     }

       cacheImages(images)
       {
         OfflineImageStore.restore({
         name: 'My_Image_gallery',
         //imageRemoveTimeout: 120, // expire image after 120 seconds, default is 3 days if you don't provide this property.
         debugMode: true,
       }, () => { // Callback function
         console.log('Restore completed and callback called !');
         // Restore completed!!
        this.setState({ reStoreCompleted: true });
         let preloadImages = [];
        /* for(var key in images)
         {
              preloadImages.push(images[key]);
         }*/
         images.map((item)=> preloadImages.push(item.imgUrl));
         // Preload images
         // Note: We recommend call this method on `restore` completion!
         OfflineImageStore.preLoad(preloadImages);
       });
       }

    handleSearch(text,emps)
    {
      let filteredList = [];
      let pattern = new RegExp(text,'i');
      this.state.cardlist.map((data,index) => {
      let orgId = data.organization.id;
      let users = this.state.employees;
      if(users && users.length > 0 && text)
      {
        let filteredEmp = [];
        filteredEmp = _.filter(users,(emp)=>{
          let employee = emp.givenName.toLowerCase() + " " + emp.familyName.toLowerCase();
          if(employee.search(pattern)!==-1)
          {
              return employee;
          }
        })
        if(filteredEmp.length > 0)
        {
          let obj = Object.assign({},data.organization,{"employee":_.uniqBy(filteredEmp,'directoryId')});
          filteredList.push(obj);
        }
      }
    })

      this.setState({filteredList:filteredList});
    }

    handleOrgSearch = (text) =>
    {
       /*let filteredList = [];
       let pattern = new RegExp(text,'i');
       filteredList = _.filter(this.state.cardlist,(org)=>{
         let orgName = org.organization.name.toLowerCase();
         if(orgName.search(pattern)!==-1)
         {
           return org;
         }
       });
       this.setState({filteredOrgList:filteredList});*/
       this.getOrganizationData("",null,null,text)
       this.setState({searchValue:text});
    }

   giveHighFive = (empInfo,org) => {
  

   let hasEvent = false;
   let eventId = "";
  

    let empId = empInfo.employeeId;
    this.sendNotification(empId);
     this.state.events.map((event) => {
        if(event.employeeInfo && event.employeeInfo.employeeId && event.employeeInfo.employeeId === empId)
         {
             hasEvent = true;
             eventId = event.id;
         }
    });

 let totalHighFive = empInfo.totalHighFive ? empInfo.totalHighFive  : 0;

  //createMutationObject(empInfo,totalHighFive,(obj)=>{
    /*if(hasEvent)
    {
      createUpdateMutationObject(eventId,[],"",(obj)=>{
         callMutation(QUERY_UPDATE_EVENT(obj),obj,(result)=>{
          console.log(result)
          let events = this.state.events.slice(); 
          events.push(result.data.addEvent);
          this.setState({events:events});
          let updatedHighfive = totalHighFive;//result.data.addEvent.totalHighFive.totalHighFives;
          this.props.navigation.navigate("AfterHighfive",{empInfo:empInfo,org:org,events:events,totalHighFives:updatedHighfive,hasEvent:true,eventId:eventId});
           });
      });
    }*/
    //else{
      //createMutationObject(empInfo,totalHighFive,(obj)=>{
      //  callMutation(QUERY_ADD_EVENT(obj),obj,(result)=>{
       // console.log(result) 
        //let events = this.state.events.slice(); 
        //events.push(result.data.addEvent);
        //this.setState({events:events});
       //let updatedHighfive = totalHighFive;//result.data.addEvent.totalHighFive.totalHighFives;
       //let eventId = result.data.addEvent.id;
       //GenerateConsumerHighFive();
       this.props.navigation.navigate("AfterHighfive",{empInfo:empInfo,org:org,events:[],eventId:"",totalHighFives:totalHighFive,hasEvent:false});
       //  });
      //});
   // }
  //});
    //})

    Analytics.trackEvent("InstantHF");

  }
  sendNotification(empId){
      console.log('sendNotification Search');
      Firebase.database().ref('devicetoken/'+empId).on("value", snapshot => {
      var tokenval=snapshot.val();
      console.log("userid"+JSON.stringify(tokenval));
      if(tokenval!=null){
        var notification={
          to: tokenval.token,
          notification: {
            title: "Notification",
            body: "Your scoring is pending",
            sound: "default",
          },
          data: {
            targetScreen: "detail"
          },
          priority: "high"
        };
        fetch("https://fcm.googleapis.com/fcm/send", {
          method: 'post',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            //Key = Server Key  from FirebaseApp==>Settings==>Cloud Messaging==>Server Key
            "Authorization": "key=AAAA-UP5J3U:APA91bGnDf3Ts-JbD9xsZSC7NodsPgJQQDRH8t5sWoD_hePXq5GdQrz4KfBZMjA-ixmod5EnQL6SunUUI3-VW6HZowtxkdSLdrr4czaEzu7Etwz4yyVOU62jBFKwiPF6kI-xW5uOrPPE"
          },
          body: JSON.stringify(notification),
        })
        .then(res=>{
          console.log('list product _response'+JSON.stringify(res));
          return res.json();
        })
        .then(json=>{
          // Alert.alert("json"+JSON.stringify(json));
          Alert.alert("Notification Sent");
        })
        .catch(error => {
          console.log ('Error : '+JSON.stringify(error));
          console.log ( 'Notification sent Failed.');
        });
      }
    });
    }

  setLocationPlaceholder = () =>
  {
          getGeoInfo((pos,locError)=>{
               if( locError && locError.message.toLowerCase() === "no location provider available."){
                this.setState({
                  locationPlaceHolder:"Please turn on location provider"
                })
            }
            else{
               this.setState({
                 locationPlaceHolder:"Current Location"
               })
            }
         })
  }


    getLocation()
    {
        getGeoInfo((pos,locError)=>{
            let crd = pos.coords;
            let loc = ""
           // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!");
           // console.log(crd.latitude);
           // console.log(crd.longitude);
           // console.log(crd.accuracy);
            let apiKey = "AIzaSyAlLJhKJcCpVrEoOcMNwqAtKIC3X83HItg";
            fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + crd.latitude + ',' + crd.longitude + '&key=' + apiKey)
           .then((response) => response.json())
           .then((responseJson) => {
               if(responseJson && Object.keys(responseJson).length > 0 && responseJson.results[0] && Object.keys(responseJson.results[0]).length > 0 && responseJson.results[0].address_components && responseJson.results[0].address_components.length > 0)
               {
               responseJson.results[0].address_components.map((address,index) => {
                   if(index < 3 || index === 3)
                   {
                       console.log(address.long_name)
                       loc = loc + address.long_name + (index !==3 ? "," : "");
                   }
               })
              }
              else{
                console.log("error getting location");
              }
              // console.log('ADDRESS GEOCODE is BACK!! => ' + JSON.stringify(responseJson));
               this.setState({location:loc ? loc : "" });
            }).catch((error)=>{
              console.log(error)
            });
            this.getSearchData(crd,loc);
            this.setState({latLong:crd});
        });
    }

    getSearchData(latLong,location){
        this.getOrganizationData("",latLong,location);
    }

   _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      this.setLocationPlaceholder();
    }
    else{
      console.log('App has come to the background!')  
    }
    
   this.setState({appState: nextAppState});
  }


    render()
    {
     let cardlist = this.state.filteredOrgList.length > 0 ? this.state.filteredOrgList : this.state.cardlist;
     let orgId =[];
     let orgName = [];
     let orgAddressLine1=[];
     let orgAddressLine2=[];
     let imageUrl = [];
     let org = [];
     let employees = [];
     let totalHighFive = [];
     let distance = [];
     let images = this.state.images ? this.state.images : {};
    //  let images = this.props.images ? this.props.images : LocalImages;
     return(
       <Container>
          {this.state.loading ? <Loader/> : null}
        <Content
            /*refreshControl={
              <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={()=>{this.getOrganizationData("refresh")}}
              />}*/
              >
            <ImageBackground style={styles.backgroundImage} source={require('../assets/images/signup_bg.png')}>
            <SearchBoxHome icon="ios-search" placeholder={SEARCH_PLACEHOLDER} iconAction={(value)=>{this.handleOrgSearch(value)}} action={(value)=>{this.handleOrgSearch(value)}} callApi={()=>{this.getSearchData()}}/>
            <SearchBoxHome icon="ios-locate-outline" placeholder={this.state.locationPlaceHolder} iconAction={()=>{this.getLocation()}} action={(location)=>{this.setState({location})}} 
              styles={{marginTop:10}} value={this.state.location} autoFocus={true} callApi={()=>{this.getSearchData()}}/>
          </ImageBackground>
       {this.state.filteredList.length > 0 ?   
          <List style={styles.listBgColor}>
             {this.state.filteredList.map((item,j)=> {
                orgName[j] = item.name;
                orgId[j] = item.id;
                imageUrl[j] = item.imageUrl;
                let viewComp = [];
                viewComp.push( <Text style={styles.yellowStrip}>{orgName[j]}</Text>)
               return <View key={"emp-view-"+ j}>
                     {item.employee.map((emp,index)=>{ 
                      return emp.employment && emp.employment.map((job,k)=>{
                       if(job.organization.id === orgId[j])
                       {
                          viewComp.push( <EmpListItem emp={emp} action1={()=>{ Analytics.trackEvent("EmployeeP"); this.props.navigation.navigate("Highfive",{org:item,empInfo:emp,events:this.state.events})}} action2={()=>{this.giveHighFive(emp,item)}}/>)
                          return viewComp;
                       }
                    })
               })}
               </View>
             })}
         </List> :
           <List style={styles.listBgColor}>
             <Text style={styles.yellowStrip}>LOCATED NEARBY-{PixelRatio.get()}</Text>
               {cardlist.length > 0 && cardlist.map((li,index) => {
                  orgId[index] = li.organization.id;
                  orgName[index] = li.organization.name;
                   imageUrl[index] = li.organization.imageUrl;
                  orgAddressLine1[index] = li.organization.address.number+" "+li.organization.address.street;
                  orgAddressLine2[index] = li.organization.address.street2+" "+li.organization.address.city;
                  org[index] = li.organization;
                 // employees[index] = li.employees;
                  totalHighFive[index] = li.highFiveScore;
                  distance[index] = li.distance ? li.distance : 0;
              //if(orgId[index] !== "A123456789")
             // {
           return <ListItem key={orgId[index]} onPress={()=>{ Analytics.trackEvent("Organization");  this.props.navigation.navigate("Organization",{org:org[index],img:imageUrl[index],employees:employees[index],orgHighFive:totalHighFive[index]})}}>
                {/*<Image style={{height:50,width:50,resizeMode:'contain'}} source={{uri:images[li.imgId]}} />*/}
               <OfflineImage resizeMode={'contain'} style={styles.imageList} source={{uri:imageUrl[index]}} />
              <Body style={styles.bodyPadding}>
                <Text style={styles.listItemTitle}>{orgName[index]}</Text>
                <Text style={styles.subtextSize} note>{orgAddressLine1[index]}</Text>
                <Text style={styles.subtextSize} note>{orgAddressLine2[index]}</Text>
              </Body>
              <Right>
                <Text note>{distance[index]} ft</Text>
              </Right>
            </ListItem>
             // }
               })}
          </List> }
        </Content> 
        </Container>
        )
       
    }
}

const mapStateToProps = (state) => {
  return {
   organizations : state.employee.organizations
  }
}

export default withNavigation (connect (mapStateToProps) (Search))


/*let styles = StyleSheet.create({
    text:{
      fontFamily:'Roboto',
      fontSize:18
    },
    subText:{
     fontFamily:'Roboto',
     fontSize:12
    },
    yellowStrip:{
     paddingRight:5,
     paddingLeft:10,
     paddingBottom:5,
     paddingTop:5,
     fontSize:20,
     fontWeight:'700',
     backgroundColor:'#FFE37A'
    },
    listBgColor:{
      backgroundColor:'#FFF'
    }
})*/