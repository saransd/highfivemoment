import React from 'react';
import { TabNavigator } from 'react-navigation';
import {Image,StyleSheet,Text,TouchableOpacity,View,AsyncStorage, Dimensions,PixelRatio} from 'react-native';
import {Container,Header,Content,Footer,FooterTab,Button,Icon,Item,Input,Grid,Row,Col} from 'native-base';
import Modal from 'react-native-modal';
import {globalstyles} from '../assets/css/styles';
import {TabIcon} from '../components/tabElements';
import {GetLogo} from '../components/get_logo';
import {Loader} from '../components/loader';
import Settings from './settings';
import Organization from './organization';
import Search from './search';
import Stars from './stars';
import Rewards from './rewards';
import Promos from './promos';
import Redeem from './redeem';
import Highfive from './highfive';
import AfterHighfive from './afterhighfive';
import AfterFeedback from './afterfeedback';
import ViewProfile from './viewprofile';
import FooterComponent from '../components/footer';
import {callQuery,callMutation,callWatchQuery,callHighFiveQuery} from '../utils/query-handler';
import {QUERY_GET_CONSUMER,QUERY_GET_CONSUMER_BY_ID,QUERY_GET_EMP_INFO,QUERY_UPDATE_DEVICES,Device,QUERY_USERS,GET_USERID_NAME} from '../constants';
import Analytics from 'appcenter-analytics';
import {connect} from 'react-redux';
import {getProfile,getUsers} from '../actions';
import _ from 'lodash';
import 'whatwg-fetch';


const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');

var logoWidth;//200
var logoHeight;//100
var modelHeight;//300
var fsiz_sub;//16
var fsize_empPosi;//14

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      logoHeight = deviceHeight/8
      logoWidth = deviceWidth/3.5
      modelHeight = deviceWidth/2
      fsiz_sub = 18
      fsize_empPosi = 16
    }
    else{
      logoHeight = deviceHeight/8
      logoWidth = deviceWidth/2.5
      modelHeight = deviceWidth/2
      fsiz_sub = 16
      fsize_empPosi = 14
    }
  }
  else{
    fsiz_sub = 16
    fsize_empPosi = 14
    modelHeight = deviceWidth/1.1
    logoHeight = deviceHeight/6
    logoWidth = deviceWidth/1.1

    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      logoHeight = deviceHeight/7
      logoWidth = deviceWidth/2
      modelHeight = deviceWidth/1.1
      fsiz_sub = 14
      fsize_empPosi = 12
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      logoHeight = deviceHeight/6.5
      logoWidth = deviceWidth/1.7
      modelHeight = deviceWidth/1.08
      fsiz_sub = 15
      fsize_empPosi = 13
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      logoHeight = deviceHeight/6.3
      logoWidth = deviceWidth/1.6
      modelHeight = deviceWidth/1.1
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      modelHeight = deviceWidth/1.2
    }
  }

const pageName = "Container";
const eventName= "onLoad";

const content = <Content contentContainerStyle={{flex:1,alignItems:'center',justifyContent:'center'}}>
                   <View style={{alignItems:'center',justifyContent:'center'}}>
                      <GetLogo styles={globalstyles.logo}/>
                   </View>
                 </Content>;


class Tab extends React.Component {
    static navigationOptions = {
        header : null
    }
    constructor(props)
    {
        super(props)
        this.state = {
             content:<View style={{flex:1}}></View>,
             active:3,
             isVisible:false,
             loading:false,
             cardlist:[],
             images : {},
             profileObj:{}
        }
        this.getContent = this.getContent.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.changeComp = this.changeComp.bind(this);
    }

     componentDidMount()
     {
        callQuery(GET_USERID_NAME(),pageName,eventName,(result)=>{
              this.props.dispatch(getUsers(result.data.users));
        })
       if(this.props.navigation.state.params && this.props.navigation.state.params.tabNo)
       {
           /*if(this.props.navigation.state.params.tabNo === 5)
           {
             this.getProfileObj((profileObj)=>{
                this.getContent(this.props.navigation.state.params.tabNo,profileObj);
             });
           }
           else{
           this.getContent(this.props.navigation.state.params.tabNo);
         }*/
          this.getContent(this.props.navigation.state.params.tabNo,this.props.profileObj,true);
          this.setState({profileObj:this.props.profileObj});

       }   
       else
       {
        this.setState({isVisible:true});
        this.getContent(3);
        setTimeout(function(){this.setState({isVisible:false})}.bind(this),3000);
        /* this.getProfileObj((profileObj)=>{ 
           // AsyncStorage.setItem('profile',JSON.stringify(profileObj));
            this.setState({profileObj:profileObj});
            this.props.dispatch(getProfile(profileObj));
        });*/
       }
     
      /*if(this.props.screenProps && this.props.screenProps.cardlist && this.props.screenProps.images )
      {
       this.getContent(3,JSON.parse(this.props.screenProps.cardlist),JSON.parse(this.props.screenProps.images));
       this.setState({cardlist:JSON.parse(this.props.screenProps.cardlist),images:JSON.parse(this.props.screenProps.images)});
      }
       AsyncStorage.getItem('cardlist').then((cardlist)=>{
           AsyncStorage.getItem('images').then((images)=>{
        if(!cardlist) {
            this.setState({loading:true});
         fetch('https://high-five-data.herokuapp.com/posts','get').
         then((response)=>response.json()).
          then((response)=>{
          AsyncStorage.setItem('cardlist',JSON.stringify(response));
          if(images)
          {
           this.setState({cardlist:response,images:JSON.parse(images),loading:false});
           this.getContent(3,response,JSON.parse(images)); 
          }
         console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
         console.log(response);
         if(!images)
         {
         fetch('https://high-five-data.herokuapp.com/images','get').
         then((res)=>res.json()).
          then((res)=>{
            AsyncStorage.setItem('images',JSON.stringify(res));
           this.setState({cardlist:response ? response : JSON.parse(cardlist) ,images:res,loading:false});
           this.getContent(3,response ? response : JSON.parse(cardlist) ,res);
           });
         }

        });
    }
     if(cardlist && !images)
     {
          fetch('https://high-five-data.herokuapp.com/images','get').
          then((res)=>res.json()).
          then((res)=>{
            AsyncStorage.setItem('images',JSON.stringify(res));
           this.setState({cardlist: JSON.parse(cardlist) ,images:res,loading:false});
           this.getContent(3, JSON.parse(cardlist) ,res);
           });
     }
        if(cardlist && images)
        {
           this.setState({cardlist:JSON.parse(cardlist),images:JSON.parse(images),loading:false});
           this.getContent(3,JSON.parse(cardlist),JSON.parse(images));  
        }
           });
       });
       setTimeout(function(){this.setState({isVisible:false})}.bind(this),3000);*/
        
     }

     getProfileObj = (callback) => {
      AsyncStorage.getItem('current-user').then((value)=>{
      let user = JSON.parse(value);
      let consumerId = user.email;
      let profileObj = {};
      let devices = [];
    
     updateDevices = (consumerData) => {
         devices.push(Device);
               let obj = {
                 id:consumerData.id,
                 devices : devices
               }
               callMutation(QUERY_UPDATE_DEVICES(obj),obj,pageName,eventName,(data)=> {
                    console.log("device updated successfully")
               })

     }

     callQuery(QUERY_GET_CONSUMER_BY_ID(consumerId),pageName,eventName,(resultOne)=>{
            let consumerData = null;
            if(resultOne.data.users)
            {
                consumerData = resultOne.data.users[0];
                if(consumerData && consumerData.devices)
                {
                    consumerData.devices.map((item)=> {
                         devices.push({id:item.id,model:item.model});          
                    })  
                }  
       if(devices.length > 0)
       {
        let isDevice = false;
        devices.map((device)=>{
            if(device.id === Device.id)
            {
                isDevice = true;
            }
        })
        if(!isDevice)
        {
          updateDevices(consumerData);
        }
       }
       else if(devices.length === 0)
       {
            updateDevices(consumerData);
       }
            /*AsyncStorage.getItem('highfives-'+consumerId).then((count)=>{
                if(count){
                   consumerData = Object.assign({},resultOne.data.consumer,{"totalHighFive":count});
                }
             })*/
            }
            profileObj = Object.assign({},{"consumer":consumerData});
            this.setState({profileObj:profileObj});
            this.props.dispatch(getProfile(profileObj));
           // callback(profileObj);
          if(consumerData && consumerData.employment)
          {
          let employeeId = consumerData.id;
          callQuery(QUERY_GET_EMP_INFO(employeeId),pageName,eventName,(resultTwo)=>{
            // let employeeData = null;
           //  if(resultTwo.data.employee)
           //  {
            //     employeeData = resultTwo.data.employee;
           //  }
            if(resultTwo.data.events){
               eventData = resultTwo.data.events; 
             }
             profileObj = Object.assign({},profileObj,{"employee":consumerData});
             profileObj = Object.assign({},profileObj,{"events":eventData});
             this.setState({profileObj:profileObj});
             this.props.dispatch(getProfile(profileObj));
             callback(true);
            // callback(profileObj);
           })
          }
      })
      })
     }

     isLoading = (flag) => {
        this.setState({loading:flag});
     }

     getContent(flag,data,fromTab)
      {
       switch(flag)
       {
           case 1:
           this.setState({active:1,content:<Settings navigation={this.props.navigation} profileObj={Object.keys(this.state.profileObj).length > 0 ? this.state.profileObj : data}/>});
           Analytics.trackEvent("Settings");
           break;
           case 2:
           this.setState({active:2,content:<Stars/>});
           Analytics.trackEvent("Stars");
           break;
           case 3:
           this.setState({active:3,content:<Search 
                                            screenProps={this.props.screenProps} 
                                            getProfile={this.getProfileObj} 
                                            profileObj={Object.keys(this.state.profileObj).length > 0 ? this.state.profileObj : data} 
                                            fromTab={fromTab}
                                            isLoading={this.isLoading}/>});
           if(data) {
              Analytics.trackEvent("GiveHFIcon");
            }
           break;
           case 4:
           AsyncStorage.getItem('isRedeemed').then((value)=>{
             this.setState({active:4,content:value === "true" ? <Redeem hideFooter={true}/> : <Promos/>});
             Analytics.trackEvent("Promo");
           });
           break;
           case 5:
           this.setState({active:5,content:<ViewProfile profileObj={Object.keys(this.state.profileObj).length > 0 ? this.state.profileObj : data}/>});
           Analytics.trackEvent("ProfileIcon");
           break;
          /* case 6:
           this.setState({active1:false,active2:false,active3:false,active4:false,active5:false,content:<Organization org={res} img={imgs} getContent={(num,props,img)=>{this.changeComp(num,props,img)}}/>});
           break;
           case 7:
           this.setState({active1:false,active2:false,active3:false,active4:false,active5:false,content: <Highfive getContent={(num,props,img)=>{this.changeComp(num,props,img)}} org={res} img={imgs}/>});
           break;
           case 8:
           this.setState({active1:false,active2:false,active3:false,active4:false,active5:false,content: <AfterHighfive getContent={(num)=>{this.changeComp(num)}}/>});
           break;
           case 9:
           this.setState({active1:false,active2:false,active3:false,active4:false,active5:false,content: <AfterFeedback getContent={(num)=>{this.changeComp(num)}}/>});
           break;*/
       }
    }

    changeComp(num,props,img)
    {
        this.getContent(num,props,img);
    }

    toggleModal()
    {
      if(this.state.isVisible)
      {
       this.setState({isVisible:false})
      }
     else
      {
       this.setState({isVisible:true})
      }
  }

    render()
    {
        return(
             <Container style={globalstyles.containerBgColor}>
                 {/*this.state.loading ? <Loader/> : null*/}
                <Modal isVisible={this.state.isVisible} style={{alignItems:'center'}} animationIn="slideInUp" animationOut='slideOutUp' animationInTiming={2000} animationOutTiming={2000}>
                 <View style={{height:modelHeight,width:modelHeight,backgroundColor:'white',alignItems:'center',justifyContent:'center',borderRadius:20}}>
                  <GetLogo styles={{resizeMode:'contain',width:logoWidth,height:logoHeight}}/>
                  <Text style={{fontWeight:'700',fontSize:fsiz_sub}}>Welcome to the High Five Movement</Text>
                  <Text style={{fontSize:fsize_empPosi,paddingTop:2}}>Reward good service. Make someone's day.</Text>
                  {/*<Text style={{fontSize:14}}>Feel good about it.</Text>*/}
                 </View>
                </Modal>
                 {this.state.content}
               {/*<Footer>
                <FooterTab style={globalstyles.footerColor} >
                  {arr.map((item,index) => {
                    return <TabIcon key={"icon"+index} action={()=>{this.getContent(index+1)}} active={item.state}  imgSrc={item.imgSrc} text={item.text}/>
                  })}
               </FooterTab>
              </Footer>*/}
              <FooterComponent getContent={(index)=>{this.getContent(index+1,null,true)}} active={this.state.active}/>
          </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        profileObj : state.employee.profileObj
    }
}

export default connect (mapStateToProps)(Tab)



