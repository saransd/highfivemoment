import React from 'react';
import {View,Image,StyleSheet,AsyncStorage,Platform,Dimensions,PixelRatio} from 'react-native';
import { Container, Header, Title, Content, Button, Left, Right, Body, Text,Item,Input,Spinner } from 'native-base';
import {globalstyles} from '../assets/css/styles';
import {TextBox,SubmitButton,SocialButtonLogin,FormFooter,ErrorText,SocialLoginAlert} from '../components/formElements';
import {Loader} from '../components/loader';
import styles from '../assets/css/login';
import firebase from '../utils/firebase-config';
//import { AccessToken, LoginManager,LoginButton } from 'react-native-fbsdk';
import { GoogleSignin } from 'react-native-google-signin';
import {GetLogo} from '../components/get_logo.js';
import {client,persistor} from '../utils/query-handler';
import {getGeoInfo} from '../utils/mutation-object';
import {createCustomer} from '../utils/create-customer';
import {callQuery} from '../utils/query-handler';
import {GET_CONSUMER_BY_EMAIL} from '../constants';
import Icon from 'react-native-vector-icons/FontAwesome';
var window = Dimensions.get('window');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
let {width,height} = window;
// import Firebase from 'react-native-firebase';
import Firebase from '../utils/nativefirebase-config';
const pageName = "Login";
const eventName = "onLoginClick"

import FBSDK from 'react-native-fbsdk'

const { LoginButton, LoginManager, AccessToken, GraphRequest, GraphRequestManager } = FBSDK


export default class Login extends React.Component
{
 static navigationOptions = {
     header:null
 }
  constructor(props)
  {
      super(props)
      this.state ={
          userid:'',
          fcmToken:'',
          email:'',
          password :'',
          error:'',
          loading:false,
          latitude:0,
          longitude:0
      }
      this.Login = this.Login.bind(this);
  }

  componentDidMount()
  {
    Firebase.messaging().getToken().then(fcmToken => {
      if (fcmToken) {
        this.setState({fcmToken:fcmToken});
        console.debug('fcm token ==', fcmToken)     
      } else {
        console.debug('User doesn\'t have a device token yet, token = ', fcmToken)
      }
    });

   GoogleSignin.hasPlayServices({ autoResolve: true }).then(() => {
    // play services are available. can now configure library
   })
   .catch((err) => {
     console.log("Play services error", err.code, err.message);
   })
   GoogleSignin.configure(Platform.OS === 'ios' ? {iosClientId : "300138031993-87bbtik8c13fspoveml58or2gb6jtgoc.apps.googleusercontent.com"} : {})
   .then(() => {
  // you can now call currentUserAsync()
    });

     getGeoInfo((pos)=>{
       let crd = pos.coords;
       this.setState({latitude:crd.latitude,longitude:crd.longitude});
    })
  }
  
  navigateToContainer(userToken)
  {
          AsyncStorage.getItem('previous-token').then((prevToken) => {
                   if(!prevToken)
                    {
                      this.setState({loading:false});
                     this.props.navigation.navigate("Tutorial");
                   }
                   else{
                     this.setState({loading:false});
                     this.props.navigation.navigate("Container");
                   }
            });
         AsyncStorage.setItem('previous-token',JSON.stringify(userToken));
  }

  Login()
  {
    if(this.state.email && this.state.password)
    {
    this.setState({error:'',loading:true});
          firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(() => {
                 var user = firebase.auth().currentUser;
                 console.log('User  :'+JSON.stringify(user));
                 AsyncStorage.setItem('current-user',JSON.stringify(user));
                 if(this.state.fcmToken!=''&&user.uid!=undefined){
                    console.log('Valid Device');
                    Firebase.database().ref('devicetoken/'+user.uid).set({token:this.state.fcmToken});
                  }
                  else{
                    console.log('Invalid Device');
                  }
                 firebase.auth().currentUser.getIdToken().then((userToken) => {
                 // client.resetStore();
                 // persistor.resume();
                 AsyncStorage.setItem('user-token',JSON.stringify(userToken)).then(()=>{
                   callQuery(GET_CONSUMER_BY_EMAIL(this.state.email),pageName,eventName,(result)=>{
                    if(result.data.users[0])
                    {
                     this.navigateToContainer(userToken);
                    }
                   else{
                     this.setState({ error: 'Login failed. Invalid User.', loading: false})
                    }
                    });  
                  });
               }) 
                  
           })
            .catch((error) => {
                //Login was not successful, let's create a new account
               this.setState({ error: 'Login failed. Please enter valid email and password.', loading: false})
            });
    }
    else{
      this.setState({error:'Please fill all the fields.'});
    }
  }

  sendNotification(){
    Firebase.database().ref('devicetoken/sej6H5jXF7bcV98yKDW0ElTwHbf2').on("value", snapshot => {
      var tokenval=snapshot.val();
      console.log("userid"+JSON.stringify(tokenval));
      if(tokenval!=null){
        var notification={
          to: tokenval.token,
          notification: {
            title: "Notification",
            body: "Your scoring is pending",
            sound: "default",
          },
          data: {
            targetScreen: "detail"
          },
          priority: "high"
        };
        fetch("https://fcm.googleapis.com/fcm/send", {
          method: 'post',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            "Authorization": "key=AAAA-UP5J3U:APA91bGnDf3Ts-JbD9xsZSC7NodsPgJQQDRH8t5sWoD_hePXq5GdQrz4KfBZMjA-ixmod5EnQL6SunUUI3-VW6HZowtxkdSLdrr4czaEzu7Etwz4yyVOU62jBFKwiPF6kI-xW5uOrPPE"
          },
          body: JSON.stringify(notification),
        })
        .then(res=>{
          console.log('list product _response'+JSON.stringify(res));
          return res.json();
        })
        .then(json=>{
          // Alert.alert("json"+JSON.stringify(json));
          Alert.alert("Notification Sent");
        })
        .catch(error => {
          console.log ('Error : '+JSON.stringify(error));
          console.log ( 'Notification sent Failed.');
        });
      }
    });
  }

  onLoginOrRegister()  
  {
  this.setState({loading:true});
  LoginManager.logInWithReadPermissions(['public_profile', 'email'])
    .then((result) => {
      if (result.isCancelled) {
        return Promise.reject(new Error('The user cancelled the request'));
      }
      // Retrieve the access token
      return AccessToken.getCurrentAccessToken();
    })
    .then((data) => {
      // Create a new Firebase credential with the token
      const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
      // Login with the credential
      return firebase.auth().signInWithCredential(credential);
    })
    .then((user) => {
        console.log('User   :'+JSON.stringify(user));
        this.props.navigation.navigate("Container");
        this.setState({loading:false});
      // If you need to do anything with the user, do it here
      // The user will be logged in automatically by the
      // `onAuthStateChanged` listener we set up in App.js earlier
    })
    .catch((error) => {
      console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%5")
      this.setState({loading:false});
      console.log(error)
      const { code, message } = error;
      // For details of error codes, see the docs
      // The message contains the default Firebase string
      // representation of the error
    });
}


googleLogin() 
{
  this.setState({loading:true});
  GoogleSignin.signIn()
    .then((data) => {
      // Create a new Firebase credential with the token
     const credential = firebase.auth.GoogleAuthProvider.credential(data.idToken, data.accessToken);
      // Login with the credential
      return firebase.auth().signInWithCredential(credential);
    })
    .then((user) => {
      var provider = new firebase.auth.GoogleAuthProvider();
      firebase.auth().currentUser.linkWithRedirect(provider);
    
      AsyncStorage.setItem('current-user',JSON.stringify(user));
      let userEmail = user.email;
      console.log('User   :'+JSON.stringify(user));
        if(this.state.fcmToken!=''&&user.uid!=undefined){
          console.log('Valid Device');
          Firebase.database().ref('devicetoken/'+user.uid).set({token:this.state.fcmToken});
        }
        else{
          console.log('Invalid Device');
        }
      user.getIdToken().then((userToken)=>{
         AsyncStorage.setItem('user-token',JSON.stringify(userToken)).then(()=>{
       /* callQuery(GET_CONSUMER_BY_EMAIL(userEmail),(result)=>{
         if(result.data.users[0])
         {
           this.navigateToContainer(userToken);
         }
         else 
         {   */
        createCustomer(user,this.state.latitude,this.state.longitude,pageName,"google login",(flag)=>{
           if(flag){
                console.log("update successful");
                //this.props.navigation.navigate("Container");s
             
                 this.navigateToContainer(userToken);
               
           }
          else{
              console.log("user already exists")
              this.setState({loading:false});
           }
          });
         });

        // }
       // });
    });
      // If you need to do anything with the user, do it here
      // The user will be logged in automatically by the
      // `onAuthStateChanged` listener we set up in App.js earlier
    })
    .catch((error) => {
      const { code, message } = error;
      console.log("***********************************")
      console.log(error)
      this.setState({loading:false});
      // For details of error codes, see the docs
      // The message contains the default Firebase string
      // representation of the error
    });
}


onFacebookLogin(){
    var accessToken;
    var lat = this.state.latitude;
    var long = this.state.longitude;
    var fcmToken = this.state.fcmToken;
    this.setState({loading:true});
    LoginManager.logInWithReadPermissions(['public_profile', 'email'])
      .then((result) => {
        console.log(result);
        if (result.isCancelled) {
          return Promise.reject(new Error('The user cancelled the request'));
        }
        // Retrieve the access token
        return AccessToken.getCurrentAccessToken();
      })
      .then((data) => {
        accessToken = data.accessToken;
        console.log(data.accessToken);
        console.log(data.accessToken.toString());
        const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
        console.log(credential);
        // return firebase.auth().signInWithCredential(credential);
        // return firebase.auth().currentUser.linkAndRetrieveDataWithCredential(credential);

        firebase.auth().signInWithCredential(credential).then(function(user) {

          console.log('User   :'+JSON.stringify(user));
        if(fcmToken!=''&&user.uid!=undefined){
          console.log('Valid Device');
          Firebase.database().ref('devicetoken/'+user.uid).set({token:fcmToken});
        }
        else{
          console.log('Invalid Device');
        }

          AsyncStorage.setItem('current-user',JSON.stringify(user));
          user.getIdToken().then((userToken)=>{
            console.log(userToken);
            AsyncStorage.setItem('user-token',JSON.stringify(userToken)).then(()=>{
            createCustomer(user,lat,long,pageName,"facebook login",(flag)=>{
              if(flag){
                console.log("update successful1"); 
                this.navigateToContainer(userToken);
              }
              else{
                console.log("creating customer failed1")
              }
            });
          });
          });
        }, function(error) {
          console.log("Error upgrading anonymous account1"+ error);
          if(error=='Error: An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.'){
            console.log(' User available   ');
            const credential = firebase.auth.FacebookAuthProvider.credential(accessToken);
            firebase.auth().currentUser.linkWithCredential(credential).then(function(user) {

              console.log('User   :'+JSON.stringify(user));
        if(fcmToken!=''&&user.uid!=undefined){
          console.log('Valid Device');
          Firebase.database().ref('devicetoken/'+user.uid).set({token:this.state.fcmToken});
        }
        else{
          console.log('Invalid Device');
        }


              AsyncStorage.setItem('current-user',JSON.stringify(user));
              user.getIdToken().then((userToken)=>{
                console.log(userToken);
                AsyncStorage.setItem('user-token',JSON.stringify(userToken));
                createCustomer(user,lat,long,pageName,(flag)=>{
                  if(flag){
                    console.log("update successful 2"); 
                    this.navigateToContainer(userToken);
                  }
                  else{
                    console.log("creating customer failed 2")
                  }
                });
              });
            }, function(error) {
              console.log("Error upgrading anonymous account 2"+ error);
            });
          }
        });
      })
      .catch((error) => {
        this.setState({loading:false});
        console.log('Error                                 :'+error)
        const { code, message } = error;
      });
  }

     render()
     {
    let tabFooterStyle = {};
    if(PixelRatio.get()<2){
      tabFooterStyle =  {footer:{height:deviceHeight/12},text:{paddingBottom:10}};
    }
     return ( <Container style={globalstyles.containerBgColor}>
      {this.state.loading ? <Loader/> : null}
<Content contentContainerStyle={globalstyles.formContent}>
<View style={styles.formWidth}>
<GetLogo styles={[styles.thfLogoForm,styles.logo]}/>
<TextBox placeholder="email" value={this.state.email} changeText={(email)=>this.setState({email})} page="login" keyboardType="email-address"/>
<TextBox placeholder="password" value={this.state.password} changeText={(password)=>this.setState({password})} secure={true} page="login"  keyboardType="default"/>
<Text style={styles.forgotPassText} onPress={()=>{this.props.navigation.navigate("ForgotPwd",{page:"login"})}}>Forgot Password</Text>
 <ErrorText error={this.state.error}/>
 <SubmitButton text="Log In" action={()=>{this.Login()}} styles={{alignSelf:'center'}} page="login"/>
</View>

<View style={styles.containerSocialLogin1}>
{/* <SocialButtonLogin text="Log In" pagename="Tutorial" style={{marginLeft:23,backgroundColor: '#36454f'}}  action={()=>{this.Login()}}/> */}
  <View style={styles.containerSocialLogin2}>
    <View style={styles.leftLine}/>
    <View><Text  style={styles.signInText}>{'  '}Or sign in with{'  '}</Text></View>
     <View style={styles.rightLine}/>
 </View>
 <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
  <SocialButtonLogin icon="facebook-f" text="Facebook" action={()=>{this.onFacebookLogin()}}/>
  <SocialButtonLogin icon="google" text="Google"  action={()=>{this.googleLogin()}}/>
  {/* <SocialButtonLogin icon="google-plus" text="Google" action={()=>{this.googleLogin()}}/> */}
 </View>
 </View>
 <FormFooter navigation={this.props.navigation} route="SignUp" text="Don't have an account? Sign Up here" styles={tabFooterStyle}/>
</Content>
</Container>);
     }
}


/*let styles = StyleSheet.create({
signInText:{
  fontFamily: "Roboto",
  fontSize: 18,
  fontWeight: "300",
  fontStyle: "normal",
  letterSpacing: 0,
  color: "#4f4f4f"
}
})*/