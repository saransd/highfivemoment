import React from 'react';
import { Dimensions,Image,StyleSheet,Text,View,TouchableOpacity,ImageBackground,Platform} from 'react-native';
import {Container,Content,Header,Title,Footer,Button} from 'native-base';
import Swiper from 'react-native-swiper';

const window = Dimensions.get('window');

export default class Tutorial extends React.Component {
  constructor(props)
  {
    super(props);
  }
  static navigationOptions = {
      header:null
  }

    render()
    {
      const arr = [{img:require('../assets/images/tutorial_1.png'),btnTxt:["Done","Next"]},
                   {img:require('../assets/images/tutorial_2.png'),btnTxt:["Done","Next"]},
                   {img:require('../assets/images/tutorial_3.png'),btnTxt:["Done","Next"]},
                   {img:require('../assets/images/tutorial_4.png'),btnTxt:["Done","Next"]},
                   {img:require('../assets/images/tutorial_5.png'),btnTxt:["Done"]}]
     return ( <Swiper style={styles.wrapper}
                     ref="swiper"
                     paginationStyle={{marginBottom:Platform.OS === 'ios' ? 235 : 180}}
                     showsButtons={true}
                     prevButton={<Text style={styles.arrow}>‹</Text>}
                     nextButton={<Text style={styles.arrow}>›</Text>}
                     dotColor="#FFF">
               {arr.map((tutorial,tutIndex)=> {
                return <View key={"tutorial-"+tutIndex} style={styles.slide1}>
                 <ImageBackground style={{flex:1,width:null,height:null,alignItems:'center',justifyContent:'center'}} source={tutorial.img} imageStyle={{resizeMode:'contain'}}>
                 <View style={{flexDirection:'row'}}>
                 {tutorial.btnTxt.map((text,index)=>{
                  return <TouchableOpacity key={text+index} style={[styles.button,((text === "Done" || text === "Next") && ( tutIndex === arr.length -1 || tutIndex === arr.length-2 || tutIndex === arr.length-3)) ? {marginTop:280} : null,index === 1 ? {marginLeft:10}:null]} onPress={text === "Next" ? () => this.refs.swiper.scrollBy(1) : ()=>{this.props.navigation.navigate("Container")}}>
                   <Text style={{color:'#FFF'}}>{text}</Text>
                   </TouchableOpacity>
                 })}
                 </View>
                 </ImageBackground>
                  {/*<Image style={{flex:1,width:null,height:null,resizeMode:'contain'}} source={{uri:'/Tutorial_1.png')}/>*/}
               </View>})}
               {/*<View style={styles.slide2}>
                 <ImageBackground style={{flex:1,width:null,height:null,alignItems:'center',justifyContent:'center'}} imageStyle={{resizeMode:'contain'}} source={{uri:'/Tutorial_2.png')}>
                    <TouchableOpacity style={{alignSelf:'center',borderWidth:1,borderColor:'#FFF',padding:10,borderRadius:5,width:100,alignItems:'center',justifyContent:'center'}} onPress={()=>{this.props.navigation.navigate("Tab")}}>
                      <Text style={{color:'#FFF'}}>Done</Text>
                     </TouchableOpacity>
                 </ImageBackground>
               </View>
                <View style={styles.slide3}>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate("Tab")}} style={{borderWidth:1,borderColor:'#36454f',height:60,width:120,alignItems:'center',justifyContent:'center',borderRadius:10,marginBottom:50}}>
                  <Text>Ready to High Five</Text>
                 </TouchableOpacity>
               </View>*/}
             </Swiper> )
    
    }
}

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
     flex: 1,
     //alignItems: 'flex-start',
     width:window.width,
     height: window.height,
     backgroundColor:'rgba(30, 53, 66, 0.9)'
    // borderWidth:2,
     //borderColor:'red',
    // alignItems: 'center',
    //justifyContent:'center'
   // backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
     height: window.height,
     width:window.width,
     backgroundColor:'rgba(30, 53, 66, 0.9)'
    //alignItems: 'center',
   // backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    //backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  arrow:{
   color:'#FFF',
   fontSize:50
  },
  button : {
    alignSelf:'center',
    borderWidth:1,
    borderColor:'#FFF',
    padding:8,
    borderRadius:5,
    width:80,
    alignItems:'center',
    justifyContent:'center'
  }
})

