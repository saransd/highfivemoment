import React from 'react';
import {View,Image} from 'react-native';
import {globalstyles} from '../assets/css/styles';
import {GetLogo} from '../components/get_logo';

export default class Splash extends React.Component 
{
  static navigationOptions = {
      header : null
  }

  componentDidMount()
  {
     setTimeout(function() {
         this.props.navigation.navigate("Home");
     }.bind(this), 1000); 
  }
    render()
    {
        return (<View style={{flex:1,backgroundColor:'#FFF',alignItems:'center',justifyContent:'center'}}>
                 <GetLogo styles={globalstyles.logo}/> 
               </View>)
    }
}