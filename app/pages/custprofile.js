import React from 'react';
import {Image,StyleSheet,ImageBackground,View,TextInput,Platform,Dimensions,AsyncStorage,TouchableOpacity,PixelRatio} from 'react-native';
import {Container,Header,Content,Grid,Row,Col,Text,CheckBox,ListItem,Body,Button,Picker,Form,Item} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import {ProfileBox,HandBox,GiftBox,CalendarBox,TrophyBoxCalendar,FeedbackTextView} from '../components/empElements';
import {GetMonthAndYear} from '../utils/date-functions';
import {callWatchQuery,callHighFiveQuery,callQuery,callQueryNetwork} from '../utils/query-handler';
import HandBoxEmp from '../components/handbox-emp';
import {QUERY_CONSUMER_HIGHFIVES,QUERY_GET_CONSUMER_EVENTS} from '../constants';
import {withNavigation} from 'react-navigation';
var window = Dimensions.get('window');
import _ from 'lodash';
import moment from 'moment';

const pageName = "Personal Profile";

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');
var imageBackgroundHeight;//400

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      imageBackgroundHeight=deviceHeight-(deviceHeight/3.9)
    }
    else{
      imageBackgroundHeight=deviceHeight-(deviceHeight/3.4) 
    }
  }
  else{
    imageBackgroundHeight=deviceHeight-(deviceHeight/3.4)
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      imageBackgroundHeight=deviceHeight-(deviceHeight/2)
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      imageBackgroundHeight=deviceHeight-(deviceHeight/2.3)
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      imageBackgroundHeight=deviceHeight-(deviceHeight/2.65)
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      imageBackgroundHeight=deviceHeight-(deviceHeight/3)
    }
  }


class CustProfile extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
        this.state = {
            totalHighFive:0,
            count:5
        }
    }

    componentDidMount()
    {   
        let consumerId = "";
        if(this.props.consumer && Object.keys(this.props.consumer).length > 0)
        {
            consumerId = this.props.consumer.id;
             callQueryNetwork(QUERY_GET_CONSUMER_EVENTS(consumerId),pageName,"getConsumerEvents",(result)=>{
               this.setState({events:result.data.events});
            })
        }
        /*if(this.props.consumer && Object.keys(this.props.consumer).length > 0)
        {
         let consumerId = this.props.consumer.consumerCoreData.consumerId;
           let storageVarName = "highfives-"+consumerId;
           AsyncStorage.getItem(storageVarName).then((value)=>{
           if(value)
           {
               this.setState({totalHighFive:JSON.parse(value)});
           }
           else
           {
           callHighFiveQuery(QUERY_CONSUMER_HIGHFIVES(consumerId),(result)=>{
             if(result.consumer && result.consumer.totalHighFive)
             {
              AsyncStorage.setItem(storageVarName,JSON.stringify(result.consumer.totalHighFive));
              this.setState({totalHighFive:result.consumer.totalHighFive});
             }
             })
           }
         })
        }*/
    }


    getConsumerEvents(count){
        this.setState({
            count : count + 5
        })
    }

  

    render()
    {
       let consumer = this.props.consumer ? this.props.consumer : {};
        let events = this.state.events ? this.state.events : [];
       let consumerData = {};
       let city = "";
       let state = "";
       let textArr = ["",""];
       let date = "";
       let year = "";
       let month = ""; 
       let consumerId = "";
       let totalHighFive = 0;
       if(Object.keys(consumer).length > 0)
       {
        consumerData = {givenName : consumer.givenName,familyName : consumer.familyName};
        city = consumer.address.city;
        state = consumer.address.countryInfo.USA_state;
        textArr = ["",""];
        date = GetMonthAndYear(consumer.joined);
        year = date.year;
        month = date.month;
        consumerId = consumer.id;
        totalHighFive = consumer.highFiveScore ? consumer.highFiveScore : this.state.totalHighFive;
       }
 
        return(<Content>
                  <Grid>
                    <ProfileBox consumerInfo={consumerData} 
                                textArr={textArr}
                                 settingStyle={{width:30,height:30,resizeMode:'contain',alignSelf:'flex-end',right:deviceWidth/40,bottom:deviceHeight/60}} 
                                 setting={true}
                                 organizations={this.props.organizations}
                                 users={this.props.users}
                                 navigation={this.props.navigation}
                                 addEmp={true}
                                 />
                    <Row>
                    <ImageBackground style={[globalstyles.formElementMargin,{width:deviceWidth,height:null,paddingLeft:10,paddingRight:deviceWidth/40,paddingBottom:10}]} source={require('../assets/images/signup_bg.png')}>
                    <Row style={{paddingTop:deviceHeight/50}}>
                        {/*<HandBox text="Given" highfives={totalHighFive}/>*/}
                          <HandBoxEmp text="Given" queryConstant={QUERY_CONSUMER_HIGHFIVES(consumerId)} pageName={pageName} isHandClick={true} handleClick={()=>{this.getConsumerEvents(this.state.count)}}/>
                        <TrophyBoxCalendar month={month} year={year}/>
                        {/*<GiftBox/>*/}
                    </Row>
                    {/*<Row style={[globalstyles.gridPadding,{alignItems:'center',justifyContent:'center',marginBottom:100}]}>
                        <CalendarBox month={month} year={year}/>
                    </Row>*/}
                     <Row style={{paddingTop:deviceHeight/50}}>
                        <FeedbackTextView events={events} count={this.state.count} fbType="cust"/>
                    </Row>
                    {/*<Row style={{paddingTop:deviceHeight/50,alignItems:'center',justifyContent:'center'}}>
                      <Button style={{color:"#FFF",backgroundColor:"#334149"}} onPress={()=>{this.props.navigation.navigate('AddEmployment',{organizations:this.props.organizations,users:this.props.users})}}><Text>Add Employment</Text></Button>
                    </Row>*/}
                    </ImageBackground>
                    </Row>
                   </Grid>
                 </Content>)
    }
}

export default withNavigation(CustProfile)


