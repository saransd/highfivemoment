import React from 'react';
import {View,Image,ImageBackground,AsyncStorage,StyleSheet,ScrollView,Platform,Dimensions,KeyboardAvoidingView,TouchableOpacity,Keyboard} from 'react-native';
import {Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right,Item,Input, Body, Text,Spinner } from 'native-base';
import {globalstyles} from '../assets/css/styles';
import stylesSignup from '../assets/css/signup';
import {TextBox,SubmitButton,SocialButtonLogin,FormFooter,SocialLoginAlert} from '../components/formElements';
import {Loader} from '../components/loader';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from '../utils/firebase-config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {GetLogo} from '../components/get_logo.js';
import {callMutation,callQuery,client,persistor} from '../utils/query-handler';
import {createCustomer} from '../utils/create-customer';
import {QUERY_ADD_CONSUMER,QUERY_ALL_EMPLOYEES,QUERY_EMP_BY_EMAIL,QUERY_UPDATE_EMP_ID,PRIVACY_POLICY,TERMS_OF_USE} from '../constants';
import {getGeoInfo} from '../utils/mutation-object';
let window = Dimensions.get('window');
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
let {width,height} = window;

const pageName = "Signup"

const eventName = "signup"

export default class SignUp extends React.Component
{
 static navigationOptions = {
     header:null
 }
  constructor(props)
  {
      super(props)
      this.state ={
          email:'',
          name:'',
          password:'',
          error:'',
          loading:false,
          viewHeight:0,
          flexValue:1,
          latitude:0,
          longitude:0

      }
      this.Register = this.Register.bind(this);
      this._keyboardDidShow = this._keyboardDidShow.bind(this);
      this._keyboardDidHide = this._keyboardDidHide.bind(this);
  }

   componentDidMount () {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    getGeoInfo((pos)=>{
       let crd = pos.coords;
       this.setState({latitude:crd.latitude,longitude:crd.longitude});
    })
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow () {
    console.log('Keyboard Shown');
    this.setState({flexValue:0});
  }

  _keyboardDidHide () {
    console.log('Keyboard Hidden');
    this.setState({flexValue:1});
  }

  
  Register()
  {
      if(this.state.email && this.state.password && this.state.name)
      {
      this.setState({error:'',loading:true});
      let latitude = this.state.latitude;
      let longitude = this.state.longitude;
       firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
                    .then(() => { 
                   var user = firebase.auth().currentUser;
                    user.getIdToken().then((userToken) => {
                        AsyncStorage.setItem('user-token',JSON.stringify(userToken));
                    });
                    user.updateProfile({
                    displayName: this.state.name,
                    photoURL: ""
                  }).then(() => {
                    createCustomer(user,latitude,longitude,pageName,"signup",(flag)=>{
                        if(flag){
                            console.log("update successful");
                        }
                        else{
                          console.log("creating customer failed")
                        }
                    });
                    /*AsyncStorage.setItem('current-user',JSON.stringify(user));
                    if (user != null) {
                    callQuery(QUERY_EMP_BY_EMAIL(user.email),(result)=>{
                       let consumerEmp = null;
                       if(result.data.employee && Object.keys(result.data.employee).length > 0){
                          let employee = result.data.employee;
                          let id = employee.id;
                          let empId = employee.employeeId;
                          let orgId = employee.organization.organizationCoreData.organizationId;
                          let orgName = employee.organization.organizationCoreData.organizationName;
                          let newEmpId = orgId+"-"+user.uid;
                          let obj = {
                                id:id,
                                employeeId:newEmpId
                          }
                        consumerEmp = {
                         organizationId:orgId,
                         organizationName:orgName,
                         employeeId: newEmpId
                        }
                        callMutation(QUERY_UPDATE_EMP_ID(obj),(obj),(result)=>{
                          console.log("%%%%%%%%%%%%%%%%%%%%%")
                          console.log(result.data);
                        })
                       }
                       name = user.displayName;
                       email = user.email;
                       photoUrl = user.photoURL;
                       emailVerified = user.emailVerified;
                       uid = user.uid;  
                       let emailArr = [];
                       emailArr.push(user.email);
                       let obj = {
                         consumerId : uid,
                         givenName : name,
                         familyName : name,
                         number:"123",
                         street:"western street",
                         street2:"",
                         city:"Chicago",
                         country:"USA",
                         USA_state:"IL",
                         USA_zipCode:"US123",
                         consumerEmployeeMapping:consumerEmp,
                         consumerEmails:emailArr,
                         latitude:latitude,
                         longitude:longitude
                       }
                       console.log("##########################")
                       console.log(obj)
                       callMutation(QUERY_ADD_CONSUMER(obj),obj,(result)=>{
                         console.log("!!!!!!!!!!!!!!!!!!!!!11")
                         console.log(result.data);
                       })
                    })
                      }*/
                  }).catch(function(error) {
                      console.log(error);
                   });
                         client.resetStore();
                         persistor.resume();
                        this.setState({ error: '', loading: false });
                        this.props.navigation.navigate("Tutorial");
                     })
                    .catch((error) => {
                       let Regerror = "Register Failed..."
                        for(var e in error)
                        {
                           if(e === "message")
                           {
                             Regerror = error[e];
                           }
                        }
                        this.setState({ error: Regerror, loading: false });
                    });
      }
      else{
          this.setState({error:'Please fill all the fields.'})
      }
  }

  layout = event =>
  {
    let height = event.nativeEvent.layout.height;
    this.setState({viewHeight:height});
  }
 
  gotoPrivacy(url)
  {
      this.props.navigation.navigate("Privacy",{url:url});
  }

     render()
     {
      let flexValue = this.state.flexValue;
      if(height === 480 && width === 320)
      {
         flexValue = 0;
      }
      let bgHeight = window.height - this.state.viewHeight;
     return (
              <Container style={[globalstyles.containerBgColor]}>
                 {this.state.loading ? <Loader/>: null}
              <Content contentContainerStyle={{flex:flexValue}}
                  enableOnAndroid
                  enableAutomaticScroll
                  keyboardOpeningTime={0}
                  extraHeight={200}>
              <View style={{alignItems:'center',justifyContent:'center'}} onLayout={this.layout}> 
                  <GetLogo styles={[stylesSignup.thfLogoForm,stylesSignup.logo]}/>
               <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                  {/*<SocialButtonSignup icon="facebook-f" text="Sign Up with Facebook" styles={globalstyles.formElementMargin}/>
                  <SocialButtonSignup danger icon="google-plus" text="Sign Up with Google" styles={{marginTop:10}}/>*/}
                    <SocialButtonLogin icon="facebook-f" text="Facebook" action={()=>{SocialLoginAlert()}}/>
                    <SocialButtonLogin icon="google" text="Google" action={()=>{SocialLoginAlert()}}/>
                  </View>
                 <Text style={[globalstyles.formElementMargin,stylesSignup.signUpText]}>By signing up, you agree to the 
                  <Text onPress={()=>{this.gotoPrivacy(TERMS_OF_USE)}} style={[stylesSignup.signUpText,stylesSignup.linkText]}> Terms of Use </Text>and 
                  <Text onPress={()=>{this.gotoPrivacy(PRIVACY_POLICY)}} style={[stylesSignup.signUpText,stylesSignup.linkText]}> Privacy Policy</Text>
                </Text>
               </View>
             <ImageBackground style={stylesSignup.backgroundImage} source={require('../assets/images/signup_bg.png')}>
              {/*<View style={[{flex:1,width:window.width,height:bgHeight,alignItems:'center'}]}>*/}
               <TextBox placeholder="email" changeText={(email)=>this.setState({email})} styles={Platform.OS === 'ios' ? stylesSignup.formWidth : stylesSignup.formWidth} keyboardType="email-address"/>
               <TextBox placeholder="full name" changeText={(name)=>this.setState({name})} styles={Platform.OS === 'ios' ? stylesSignup.formWidth : stylesSignup.formWidth} keyboardType="default"/>
               <TextBox placeholder="password" changeText={(password)=>this.setState({password})} styles={Platform.OS === 'ios' ? stylesSignup.formWidth : stylesSignup.formWidth} secure={true} keyboardType="default"/>
               <Text style={{color:'red',alignSelf:'center'}}>{this.state.error}</Text>
                 <SubmitButton text="Sign Up" pagename="Container" styles={stylesSignup.signupButton} action={()=>{this.Register()}}/>
                <Text onPress={()=>{this.props.navigation.navigate("Login")}} uppercase={false} style={stylesSignup.footerTxt}>Already have an account? Log In here.</Text>
               {/*</View>*/}
             </ImageBackground>
              {/*<FormFooter navigation={this.props.navigation} route="Login" text="Already have an account? Log In here"/>*/}
              </Content>
             </Container>
       );
     }
}

/*let styles = StyleSheet.create({
footerTxt:{
  fontFamily: "Roboto",
  fontSize: 18,
  fontWeight: "300",
  fontStyle: "normal",
  letterSpacing: 0,
  color: "#FFF",
  paddingTop:15,
  alignSelf:'center'
},
linkText:{
  color:'blue',
  textDecorationLine : 'underline'
},
signUpText:{
    fontFamily:'Roboto',
    fontSize:11
},
textBoxWidthIos:{
width:290,
height:60,
borderColor:'transparent'
},
textBoxWidth:{
width:290,
height:50,
borderColor:'#000'
}
})*/