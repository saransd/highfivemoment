import React from 'react';
import {Image,StyleSheet,ImageBackground,View,TextInput,Platform,Dimensions,PixelRatio} from 'react-native';
import {Container,Header,Content,Grid,Row,Col,Text,CheckBox,ListItem,Body,Button,Form,Item} from 'native-base';
import {ProfileBox,HandBox,TrophyBox,CalendarBox,TrophyBoxCalendar,FeedbackTextView} from '../components/empElements';
import {GET_HIGHFIVES} from '../constants';
import HandBoxEmp from '../components/handbox-emp';
import {globalstyles} from '../assets/css/styles';
var window = Dimensions.get('window');
import ProfilePicker from '../components/picker';
import {GetMonthAndYear} from '../utils/date-functions';
import {getJobTitle} from '../utils/getName';
import {withNavigation} from 'react-navigation';
import moment from 'moment';
import _ from 'lodash';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');
var fsize;//20

  if(DeviceInfo.isTablet()){
    fsize = 20
  }
  else{
    fsize = 19
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      fsize = 14
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      fsize = 16
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      fsize = 17
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      fsize = 18
    }
  }

const pageName = "Employee Profile"

class OtherEmp extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
        this.state = {
            selected : ""
        }
        this.onValueChange = this.onValueChange.bind(this);
    }
    
    onValueChange(value)
    {
        this.setState({selected:value});
    }

    render()
    {
       
       let employee = this.props.employee ? this.props.employee : {};
       //console.log("$$$$$$$$$$$$$$$$$$$$$$$$$4");
       //console.log(employee)
       let events = this.props.events ? this.props.events : [];
       let organizationName = "";
       let totalHighFives = "";
       let textArr = ["",""];
       let date = "";
       let year = "";
       let month = "";
       let position="";
       let employeeId = "";
       let attributes = [];
       let event={};
       let eventDate = "";
       let systemRole = "";
       let employment = {};
       if(Object.keys(employee).length > 0)
       {
          employeeId = employee.id;
          employment = employee.employment;
          organizationName = employee.employment[0].organization.name;
          position = getJobTitle(employee.employment,{})
         // totalHighFives = employee.totalHighFive;
          attributes = employee.attributesScore && employee.attributesScore.length > 0  ? employee.attributesScore : [];
          date = GetMonthAndYear(employee.joined);
          year = date.year;
          month = date.month;
          textArr = [position,"Joined "+organizationName,"in "+month+" "+year];
          systemRole = employee.systemRole;
       }

       /*let consumer = this.props.consumer ? this.props.consumer : {};
       let consumerData = {};
       let city = "";
       let state = ""; 
       if(Object.keys(consumer).length > 0)
       {
        consumerData = consumer.consumerCoreData;
        city = consumer.consumerCurrentAddress.city;
        state = consumer.consumerCurrentAddress.countryInfo.USA_state;
        textArr = ["",""];
        date = GetMonthAndYear(consumer.joinedDate);
        year = date.year;
        month = date.month;
       }*/

        return( <Content>
                  <Grid>
                     <ProfileBox textArr={textArr} 
                     empInfo={employee} 
                     /*consumerInfo={consumerData}*/ 
                     setting={true} 
                     settingStyle={{width:30,height:30,resizeMode:'contain',alignSelf:'flex-end',right:deviceWidth/12,bottom:5}}
                     navigation={this.props.navigation}
                     organizations={this.props.organizations}
                     users={this.props.users}
                     pageName={pageName}
                     employment={employment}
                     addEmp={true}
                     />
                     <Row>
                    <ImageBackground style={{width:window.width,height:null,paddingLeft:10,paddingRight:10,paddingBottom:10}} source={require('../assets/images/signup_bg.png')}>
                    <Row style={{paddingTop:deviceHeight/50}}>
                      {/*<HandBox highfives={totalHighFives}/>*/}
                      <HandBoxEmp empId={employeeId} queryConstant={GET_HIGHFIVES(employeeId)} workProfile={true} pageName={pageName}/>
                      <TrophyBoxCalendar month={month} year={year} attributes={attributes} workProfile={true}/>
                    </Row>
                    {/*<Row style={[globalstyles.gridPadding,{alignItems:'center',justifyContent:'center'}]}>
                       <CalendarBox/>
                    </Row>*/}
                     <Row style={{paddingTop:deviceHeight/50}}>
                        <FeedbackTextView events={events} fbType="emp"/>
                    </Row>
                      {/*systemRole === "administrator" ? <Row style={{alignItems:'center',justifyContent:'center',paddingTop:deviceHeight/50}}>
                      <Button style={{color:"#FFF",backgroundColor:"#334149"}} onPress={()=>{this.props.navigation.navigate('AddEmployment',{organizations:this.props.organizations,users:this.props.users,pageName:pageName,employment:employment})}}><Text>Add Employment</Text></Button>
                    </Row> : null*/}
                    </ImageBackground>
                    </Row>
                   </Grid>
                 </Content>)
    }
}

export default withNavigation(OtherEmp);



