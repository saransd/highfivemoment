import React from 'react';
import {Image,StyleSheet,ImageBackground,View,TextInput,Platform,Dimensions,ScrollView,KeyboardAvoidingView,TouchableOpacity,Keyboard,UIManager,AsyncStorage,findNodeHandle,AppState,PixelRatio} from 'react-native';
import {Container,Header,Content,Grid,Row,Col,Text,CheckBox,ListItem,Body,Button,Picker,Form,Item} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import {ProfileBox,HandBox,TrophyBox,CalendarBox,GiveHighFive,TrophyBoxCalendar} from '../components/empElements';
import {BackArrow} from '../components/tabElements';
import FooterComponent from '../components/footer';
import {withNavigation} from 'react-navigation';
import {QUERY_ATTRIBUTES,QUERY_EVENTS,QUERY_ADD_EVENT,QUERY_ADD_EVENT_ATTRIBS,GET_HIGHFIVES,QUERY_UPDATE_EVENT,HIGHFIVE_TIME_LIMIT} from '../constants';
import {callQuery,callMutation,client,callWatchQuery} from '../utils/query-handler';
import {createMutationObject,createUpdateMutationObject} from '../utils/mutation-object';
import {GetMonthAndYear} from '../utils/date-functions';
import {getJobTitle} from '../utils/getName';
import HandBoxEmp from '../components/handbox-emp';
import Analytics from 'appcenter-analytics';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import moment from 'moment';
import {connect} from 'react-redux';
import  momentTimeZone from 'moment-timezone';
var window = Dimensions.get('window');
let {height,width} = window;

const pageName = "Tell Us More"
const eventNames = ["onHighFive","onSubmit"];

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');

var imageSize1;//50
var imageSize2;//75
var fsize;//20
var fsize_title;//22
var fsiz_sub;//16
var heightBackWall;
var textWidth; //Window.width-127
var textWidth2;  //Window.width-119
var logoHeight;//105
var checkboxWidth;
var fontweight;//'700'
var centerBackgroundHeight; //300
var btnWidth;
var mgLeft;

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      imageSize1 = 65
      imageSize2 = 85
      fsize = 23
      fsiz_sub = 18
      textWidth = deviceWidth-(deviceWidth/4.3)
      textWidth2 = deviceWidth-(deviceWidth/4.5)
      heightBackWall = deviceHeight/14  //11.5
      logoHeight = deviceHeight/10.5 //130
      checkboxWidth = deviceWidth/6
      fsize_title = 24
      fontweight = '700'
      centerBackgroundHeight = deviceHeight/2.8
      btnWidth = deviceWidth/3.5
      mgLeft = deviceWidth/35
    }
    else{
      imageSize1 = 50
      imageSize2 = 75
      fsize = 20
      fsiz_sub = 16
      textWidth = deviceWidth-(deviceWidth/4.3)
      textWidth2 = deviceWidth-(deviceWidth/4.5)
      heightBackWall = deviceHeight/14   //  11.5
      logoHeight = deviceHeight/10.5//105
      checkboxWidth = deviceWidth/6.5
      fsize_title = 22
      fontweight = '700'
      centerBackgroundHeight = deviceHeight/2.6
      btnWidth = deviceWidth/4
      mgLeft = deviceWidth/60
    }
  }
  else{
    mgLeft = deviceWidth/60
    imageSize1 = 50
    imageSize2 = 70
    fsize = 19
    fsiz_sub = 15
    fsize_title = 21
    heightBackWall = deviceHeight/11   //9
    textWidth = deviceWidth-(deviceWidth/2.9)
    textWidth2 = deviceWidth-(deviceWidth/3.3)
    logoHeight = deviceHeight/8.2 //105
    checkboxWidth = deviceWidth/5
    fontweight = '700'
    centerBackgroundHeight = deviceHeight/1.9
    btnWidth = deviceWidth/3
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      imageSize1 = 30
      imageSize2 = 50
      fsize = 14
      fsiz_sub = 12
      heightBackWall = deviceHeight/10.5    // 8
      textWidth = deviceWidth-(deviceWidth/2.5)
      textWidth2 = deviceWidth-(deviceWidth/3.3)
      logoHeight = deviceHeight/7.5 //85
      checkboxWidth = deviceWidth/4
      fsize_title = 17
      fontweight = '600'
      centerBackgroundHeight = deviceHeight/1.9
      mgLeft = deviceWidth/25
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      imageSize1 = 35
      imageSize2 = 50
      fsize = 16
      fsiz_sub = 13
      heightBackWall = deviceHeight/9.5    //7
      textWidth = deviceWidth-(deviceWidth/2.5)
      textWidth2 = deviceWidth-(deviceWidth/3.2)
      logoHeight = deviceHeight/7.5 //95
       checkboxWidth = deviceWidth/4.4
       fsize_title = 18
       fontweight = '650'
       centerBackgroundHeight = deviceHeight/1.9
       mgLeft = deviceWidth/50
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      imageSize1 = 40
      imageSize2 = 55
      fsize = 17
      fsiz_sub = 14
      heightBackWall = deviceHeight/10.5     //8
      textWidth = deviceWidth-(deviceWidth/2.7)
      textWidth2 = deviceWidth-(deviceWidth/3.2)
      logoHeight = deviceHeight/7.5 //100
       checkboxWidth = deviceWidth/4.8
       fsize_title = 19
       centerBackgroundHeight = deviceHeight/1.9
       mgLeft = deviceWidth/55
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      centerBackgroundHeight = deviceHeight/2.1
      heightBackWall = deviceHeight/11   //8
      logoHeight = deviceHeight/8.2
      imageSize1 = 45
      imageSize2 = 60
      fsize = 18
      fsize_title = 20
    }
  }


class AfterHighfive extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
        this.state = {
            checked : [],
            empInfo : props.navigation.state.params.empInfo,
            org:props.navigation.state.params.org,
            totalHighFives : props.navigation.state.params.totalHighFives,
            appState : AppState.currentState,
            events:props.navigation.state.params.events,
            hasEvent : props.navigation.state.params.hasEvent,
            eventId : props.navigation.state.params.eventId,
            getOrganizationData : props.navigation.state.params.getOrganizationData ? props.navigation.state.params.getOrganizationData : null,
            viewTopPadding: 10,
            feedback : "",
            emp_attribs : [],
            isSubmitted : false,
            timeout : ""
        }
        this.toggleCheckBox = this.toggleCheckBox.bind(this);
        this._keyboardDidHide = this._keyboardDidHide.bind(this);
        this._keyboardDidShow = this._keyboardDidShow.bind(this);
    }


componentDidMount () {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    AppState.addEventListener('change', this._handleAppStateChange);

    this.handleSubmit("add");
      
      let org = this.state.org;
      let orgId = org.id;
      let empInfo = this.state.empInfo;
      let empId = this.state.empInfo.id;
      let empAttributes = [];
      let jobTitle = getJobTitle(empInfo.employment,org);

     // let orgDepts = this.state.org.departments && this.state.org.departments.length > 0 ? this.state.org.departments : [];
      
      if(org.attributeGroups && org.attributeGroups.length > 0)
      {   
          org.attributeGroups.map((group,index)=>{
              if(jobTitle.toLowerCase() === group.job.name.toLowerCase() )
              {
                  empAttributes = group.attributes;
                  this.setState({emp_attribs:empAttributes});
              }
          })
      }

    //callQuery(QUERY_ATTRIBUTES(orgId),(result)=>{
     //});

     /* callWatchQuery(GET_HIGHFIVES(empId),(data)=>{
            this.setState({totalHighFives:data.employee.totalHighFive});
        })*/

     /*if(!this.state.hasEvent)
     {
            this.state.events.map((event) => {
                 if(event.employeeInfo && event.employeeInfo.employeeId && event.employeeInfo.employeeId === empId)
                 {
                   this.setState({hasEvent:true,eventId:event.id});
                 }
            })
     }*/


  // this.setState({ timeout : setTimeout(function(){ this.submitAction()}.bind(this),HIGHFIVE_TIME_LIMIT) });
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _keyboardDidShow () {
      this.setState({viewTopPadding:110});
  }

  _keyboardDidHide () {
     this.setState({viewTopPadding:10});
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
    }
    else{
      console.log('App has come to the background!')  
      //this.submitAction();
    }
    
   this.setState({appState: nextAppState});
  }

    toggleCheckBox(id){
        let isChecked = this.state.checked;
        if(isChecked[id])
        {
           isChecked[id] = false
        }
        else{
            isChecked[id] = true
        }
        this.setState({checked:isChecked});
    
    }

    checkForAttribute = () =>
    {
      let isAttribute = false;
      this.state.emp_attribs.map((item,index)=>{
         if(this.state.checked["checkbox-"+index])
          {
              isAttribute = true;
              return;         
          }
      });
      return isAttribute;
    }

     submitAction = () => {
         if(!this.state.feedback && !this.state.isSubmitted)
         {
            this.handleSubmit()
         }     
     }

    handleSubmit = (eventType) => {
       let attribs = [];
       this.state.emp_attribs.map((item,index)=>{
               if(this.state.checked["checkbox-"+index])
               {
                  let obj = {
                    attributeId : item.id,
                    rating : 1
                  }
                  attribs.push(obj);
               }
       });

     /*let obj = {
           eventId : this.state.eventId,
           attributesRecognized: attribs,
           feedback : this.state.feedback
       }*/
  /*createUpdateMutationObject(this.state.eventId,attribs,this.state.feedback,(obj) => {
    callMutation(QUERY_UPDATE_EVENT(obj),obj,(result)=>{
             this.setState({isSubmitted:true});  
    });
   });*/
   //AsyncStorage.getItem('profile').then((res)=>{
    if(eventType === "add")
    {
     createMutationObject(this.state.empInfo,this.props.profileObj,this.state.org,this.state.totalHighFives,attribs,this.state.feedback,"",(obj)=>{
              callMutation(QUERY_ADD_EVENT(obj),obj,pageName,eventNames[0],(result)=>{
                  //this.setState({isSubmitted:true}); 
                  this.setState({eventId:result.data.addEvent.id,totalHighFives:result.data.addEvent.highFiveScore})
            });
         });
    }
    if(eventType === "update")
    {
        createMutationObject(this.state.empInfo,this.props.profileObj,this.state.org,this.state.totalHighFives,attribs,this.state.feedback,this.state.eventId,(obj)=>{
              callMutation(QUERY_UPDATE_EVENT(obj),obj,pageName,eventNames[1],(result)=>{
                  this.setState({isSubmitted:true}); 
            });
         });
    }
   //});
   
   Analytics.trackEvent("Optional");
     
    }


    render()
    {
      let empInfo = this.state.empInfo;
      let org = this.state.org;
      let textArr = [getJobTitle(empInfo.employment,org),org.name];
      /*let checkBoxArr = [{id:"checkbox-1",text:"Polite"},
                         {id:"checkbox-2",text:"Knowledgable"},
                         {id:"checkbox-3",text:"Kind"},
                         {id:"checkbox-4",text:"Helpful"}]*/
     let checkBoxArr = this.state.emp_attribs;
     let employeeId = this.state.empInfo.id;
     //let query = gql`${GET_HIGHFIVES(employeeId)}`;
     let date = GetMonthAndYear(this.state.empInfo.joined);
     let year = date.year;
     let month = date.month;
     let feedbackTextBoxWidth = 330;
     let feedbackColWidth = 90;
     if(height === 480 && width === 320){
         fteedbackTextBoxWidth = 300;
         bfeedbackColWidth = 80;
     }
     
     let btnDisabled = this.checkForAttribute() || this.state.feedback ? false : true;
        
        return( <Container style={[globalstyles.containerBgColor]}>
                <Content style={Platform.OS === 'ios' ? {paddingTop:10} : {paddingTop:this.state.viewTopPadding}}
                  enableOnAndroid
                  enableAutomaticScroll
                  keyboardOpeningTime={0}
                  extraHeight={Platform.OS === 'ios' ? 140 : 250}
                  removeClippedSubviews={true}
                >
                  <BackArrow action={()=>{this.props.navigation.goBack()}}/>
                  <Grid style={{paddingBottom:deviceHeight/35}}>
                      <ProfileBox 
                      textArr={textArr} 
                      empInfo={empInfo} 
                      action={()=>{this.props.navigation.navigate("Highfive",{empInfo:empInfo,org:org,events:this.state.events})}}
                      addEmp={false}
                      />
                      <ImageBackground style={[styles.imageBackHiFi,styles.rowMargin]} imageStyle={{resizeMode:'stretch'}} source={require('../assets/images/greybg.png')}>
                         
                         <View style={{flex:1,flexDirection:'row'}}>
                          <View style={{flex:0.75,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
                            <Text style={[styles.text,{fontSize:fsize,fontWeight:'700'}]}>{`You just gave`}</Text>
                            <Text style={[styles.text,{fontSize:fsize,fontWeight:'700'}]}>{`${empInfo.givenName.split(' ')[0]} a High Five!!!`}</Text>
                          </View>
                          <View style={{flex:0.25,flexDirection:'column'}}>
                            <Image style={[{width:logoHeight,height:logoHeight,bottom:15,alignSelf:'flex-end'}]} source={require('../assets/images/post_high.png')}/>
                          </View>
                      </View>
                  </ImageBackground>
                   <Row style={globalstyles.gridPadding}>
                     {this.state.isSubmitted ?
                      <ImageBackground style={{width:window.width,height:imageSize2,alignItems:'center',justifyContent:'center'}} source={require('../assets/images/rectangle.png')}>
                          <Text style={[globalstyles.text,{color:'white',fontSize:fsize}]}>Thank you for your feedback!</Text>
                      </ImageBackground> : 
                      <ImageBackground style={styles.centerBackground} source={require('../assets/images/feedback_bg.png')}>
                          <Text style={[globalstyles.text,{color:'white',fontSize:fsize_title},globalstyles.gridPadding]}>Tell Us More (Optional)</Text>
                          <Row style={{paddingTop:20,height:logoHeight}}>
                            {checkBoxArr.length > 0 && checkBoxArr.map((checkbox,index) => {
                             return <Col style={{width:checkboxWidth}} key={"checkbox-"+index}>
                                     <CheckBox id={"checkbox-"+index} checked={this.state.checked["checkbox-"+index] ? true : false} style={this.state.checked["checkbox-"+index] ? styles.checkboxChecked : styles.checkboxUnchecked} onPress={()=>{this.toggleCheckBox("checkbox-"+index)}}/>
                                     <Text style={styles.checkboxText}>{checkbox.name}</Text>
                                    </Col>
                             })} 
                          </Row>
                           <TextInput multiline={true} numberOfLines={3} placeholder="Share other feedback..."  onChangeText={(feedback)=>{this.setState({feedback:feedback})}} underlineColorAndroid='transparent' style={styles.textArea} returnKeyType="done"/>
                              {btnDisabled ?
                            <Button rounded 
                                disabled={true} 
                                style={[styles.submitBtnDisabled,Platform.OS==='ios' ? {marginRight:38}:{marginRight:deviceWidth/30}]} 
                                onPress={()=>{this.handleSubmit("update")}}>
                              <Text style={{fontSize:fsiz_sub,color:'#FFF'}}>SUBMIT</Text>
                            </Button>
                          :
                            <Button rounded 
                                disabled={false} 
                                style={[styles.submitBtnEnabled,Platform.OS==='ios' ? {marginRight:38}:{marginRight:deviceWidth/30}]} 
                                onPress={()=>{this.handleSubmit("update")}}>
                              <Text style={{fontSize:fsiz_sub,color:'#FFF'}}>SUBMIT</Text>
                            </Button>
                          }
                      </ImageBackground>}
                  </Row>
                    <Row style={[globalstyles.gridPadding,styles.rowMargin]}>
                      {/*<HandBox highfives={this.state.totalHighFives}/>*/}
                      <HandBoxEmp empId={employeeId} queryConstant={GET_HIGHFIVES(employeeId)} pageName={pageName} />
                      <TrophyBoxCalendar month={month} year={year}/>
                    </Row>
                    {/*<Row style={[globalstyles.gridPadding,{alignItems:'center',justifyContent:'center'},styles.rowMargin]}>
                      <CalendarBox/>
                    </Row>*/}
                   </Grid>
                 </Content>
                 <FooterComponent/>
               </Container>)
    }
}

const mapStateToProps = (state) => ({
    profileObj : state.employee.profileObj
})

export default connect (mapStateToProps) (AfterHighfive)


const styles = StyleSheet.create({
    checkboxCol:{
        width:73
    },
    checkboxUnchecked : {
      borderColor:'transparent',
      borderRadius:30,
      backgroundColor:"#E5E5E5",
      width:imageSize1,
      height:imageSize1,
      marginLeft:mgLeft //18
    },
    checkboxChecked : {
      borderColor:'transparent',
      borderRadius:30,
      backgroundColor:"#1CBECA",
      width:imageSize1,
      height:imageSize1,
      alignItems:'center',
      justifyContent:'center',
      paddingTop:10,
      marginLeft:mgLeft //18
    },
    checkboxText : {
        color:'#FFF',
        fontFamily : 'Roboto',
        fontStyle: 'normal',
        paddingTop:deviceHeight/100,
        height:deviceHeight/20,
        fontSize: fsiz_sub,
        lineHeight: 15,
        textAlign:'center'
    },
    rowMargin : {
       marginLeft : deviceWidth/40,
       marginRight : deviceWidth/40 
    },

    imageBackHiFi:{
      width:deviceWidth-((deviceWidth/40)*2),
      height:heightBackWall,  //80
      marginTop:deviceHeight/50,
      marginRight:15
    },
    textArea:{
      width:deviceWidth-(deviceWidth/6),
      height:logoHeight,
      backgroundColor:'#FFF',
      textAlignVertical:'top',
      fontSize: fsize,
    },
    submitBtnDisabled:{
      width: btnWidth,
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:'grey',
      alignSelf:'flex-end',
      height:imageSize1,
      marginTop:deviceHeight/40,
      opacity:0.6
    },
    submitBtnEnabled:{
      width: btnWidth,
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:"#334149",
      alignSelf:'flex-end',
      height:imageSize1,
      marginTop:deviceHeight/40,
      opacity:1
    },
    centerBackground:{
      width:window.width,
      height:centerBackgroundHeight,
      alignItems:'center',
      marginTop:20
    },
    textArea:{
      width:deviceWidth-(deviceWidth/6),
      height:logoHeight,
      backgroundColor:'#FFF',
      textAlignVertical:'top',
      fontSize: fsize,
      marginTop:deviceHeight/100,
    },
})



                        // <Row>
                        //   <Col style={Platform.OS === 'ios' ? {width:textWidth,alignItems:'center',justifyContent:'center'} : {width:textWidth2,alignItems:'center',justifyContent:'center'}}>
                        //     <Text style={[styles.text,{fontSize:fsize,fontWeight:'700'}]}>{`You just gave`}</Text>
                        //     <Text style={[styles.text,{fontSize:fsize,fontWeight:'700'}]}>{`${empInfo.givenName.split(' ')[0]} a High Five!!!`}</Text>
                        //   </Col>
                        //  <Col>
                        //    <Image style={[{width:logoHeight,height:logoHeight,bottom:15,left:25}]} source={require('../assets/images/post_high.png')}/>
                        //  </Col>
                        // </Row>