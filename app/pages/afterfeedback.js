import React from 'react';
import {Image,StyleSheet,ImageBackground,View,TextInput,Platform,Dimensions,ScrollView} from 'react-native';
import {Container,Header,Content,Grid,Row,Col,Text,CheckBox,ListItem,Body,Button,Picker,Form,Item} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import {ProfileBox,HandBox,TrophyBox,CalendarBox,GiveHighFive,TrophyBoxCalendar} from '../components/empElements';
import {BackArrow} from '../components/tabElements';
import {withNavigation} from 'react-navigation';
import FooterComponent from '../components/footer';
var window = Dimensions.get('window');

export default class AfterFeedback extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props){
        super(props)
        this.state = {
            checked : [],
            empInfo : props.navigation.state.params.empInfo
        }
        this.toggleCheckBox = this.toggleCheckBox.bind(this);
    }

    toggleCheckBox(id){
        let isChecked = this.state.checked;
        if(isChecked[id])
        {
           isChecked[id] = false
        }
        else{
            isChecked[id] = true
        }
        this.setState({checked:isChecked});
    
    }
 
    render()
    {
      let empInfo = this.state.empInfo;
      let textArr = [this.state.empInfo.job,"Lurie Children's Primary Care"];
      let checkBoxArr = [{id:"checkbox-1",text:"Polite"},
                         {id:"checkbox-2",text:"Knowledgable"},
                         {id:"checkbox-3",text:"Kind"},
                         {id:"checkbox-4",text:"Helpful"}]
        
        return(<Container style={[globalstyles.containerBgColor]}>
                <Content style={{paddingTop:20,paddingBottom:10}} removeClippedSubviews={true}>
                  <BackArrow action={()=>{this.props.navigation.goBack()}}/>
                  <Grid style={{paddingBottom:30}}>
                      <ProfileBox textArr={textArr} empInfo={this.state.empInfo}/>
                      <ImageBackground style={[{width:null,height:80,marginTop:10},styles.rowMargin]} imageStyle={{resizeMode:'stretch'}} source={require('../assets/images/greybg.png')}>
                         <Row>
                          <Col style={Platform.OS === 'ios' ? {width:window.width-127,alignItems:'center',justifyContent:'center'} : {width:window.width-119,alignItems:'center',justifyContent:'center'}}>
                            <Text style={[styles.text,{fontSize:20}]}>{`You High Fived ${empInfo.givenName.split(' ')[0]}`}</Text>
                          </Col>
                         <Col>
                           <Image style={[{width:105,height:105,bottom:15}]} source={require('../assets/images/post_high.png')}/>
                         </Col>
                        </Row>
                  </ImageBackground>
                   <Row style={globalstyles.gridPadding}>
                      <ImageBackground style={{width:window.width,height:70,alignItems:'center',justifyContent:'center'}} source={require('../assets/images/rectangle.png')}>
                          <Text style={[globalstyles.text,{color:'white',fontSize:18}]}>Thank you for your feedback!</Text>
                      </ImageBackground>
                  </Row>
                    <Row style={[globalstyles.gridPadding,styles.rowMargin]}>
                       <HandBox/>
                       <TrophyBoxCalendar/>
                    </Row>
                    {/*<Row style={[globalstyles.gridPadding,{alignItems:'center',justifyContent:'center'},styles.rowMargin]}>
                      <CalendarBox/>
                    </Row>*/}
                   </Grid>
                 </Content>
                   <FooterComponent/>
               </Container>)
    }
}


const styles = StyleSheet.create({
    checkboxCol:{
        width:73
    },
    checkboxUnchecked : {
      borderColor:'transparent',
      borderRadius:30,
      backgroundColor:"#E5E5E5",
      width:50,
      height:50,
      marginLeft:10
    },
    checkboxChecked : {
      borderColor:'transparent',
      borderRadius:30,
      backgroundColor:"#1CBECA",
      width:50,
      height:50,
      alignItems:'center',
      justifyContent:'center',
      paddingTop:10,
      marginLeft:10
    },
    checkboxText : {
        color:'#FFF',
        fontFamily : 'Roboto',
        fontStyle: 'normal',
        fontSize: 11,
        lineHeight: 15,
        textAlign:'center'
    },
    rowMargin : {
       marginLeft : 15,
       marginRight : 15 
    }
})


