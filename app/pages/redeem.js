import React from 'react';
import {Container,Content,Text,Grid,Row,Col,Button} from 'native-base';
import {Image,Dimensions,StyleSheet,AsyncStorage, Platform, PixelRatio} from 'react-native';
import FooterComponent from '../components/footer';
import {SearchBox} from '../components/tabElements';
import _ from 'lodash';
import {globalstyles} from '../assets/css/styles';

const deviceHeight = Dimensions.get('window').height;
  const deviceWidth = Dimensions.get('window').width;
  var DeviceInfo = require('react-native-device-info');
  // import styles from '../assets/css/settings';
  var fsizetitle;
  var fsize1;
  var fsize2;
  var fsize3;
  var gridColHeight;
  var testMarginTop;
  var titleTop;

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      fsize1 = 29
      fsize2 = 27
      fsize3 = 23
      gridColHeight = deviceHeight/7
      testMarginTop = deviceHeight/25
      titleTop = deviceHeight/12
    }
    else{
      fsize1 = 26
      fsize2 = 25
      fsize3 = 20
      gridColHeight = deviceHeight/7
      testMarginTop = deviceHeight/25
      titleTop = deviceHeight/20
    }
  }
  else{
    titleTop = 0
    testMarginTop = deviceHeight/25
    fsize1 = 24
    fsize2 = 25
    fsize3 = 20
    gridColHeight = deviceHeight/7

    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      fsize1 = 18
      fsize2 = 18
      fsize3 = 18
      gridColHeight = deviceHeight/9
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
    
      gridColHeight = deviceHeight/7
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      
    }
    else{
    
    }
  }

export default class Redeem extends React.Component {
    static navigationOptions ={
        header:null
    }

    constructor(props){
        super(props)
        this.state = {
            hideFooter : false
        }
    }

    componentWillMount()
    {
       AsyncStorage.setItem('isRedeemed',"true");
    }

    render()
    {  
      let hideFooter = this.props.hideFooter ? this.props.hideFooter : this.state.hideFooter;
        return(<Container style={[globalstyles.containerBgColor]}>
               <Content style={{paddingRight:10,paddingLeft:10,paddingTop:20}}>
               <Text style={styles.text1}>HIGH FIVE PROMO</Text>
               <Grid>
               <Row style={styles.gridRow1}>
                   <Image source={require('../assets/images/urbanfuel.png')} style={{height:110,width:140}}/>
              </Row>
                <Col style={styles.gridCol}>
                    <Text style={{fontSize:25}}>YOUR PROMO</Text>
                    <Text style={{fontSize:25}}>HAS BEEN REDEEMED!</Text>
                </Col>
                <Col style={styles.gridCol2}>
                    <Text style={styles.textSize}>Thank you for being part of</Text>
                    <Text style={styles.textSize}>The High Five Movement!</Text>
                </Col>
                </Grid>
           </Content>
           {hideFooter ? null : <FooterComponent/>}
           </Container>
        )
    }
}

const styles = StyleSheet.create({
    text1:{
      marginTop: titleTop,
      fontSize:fsize1,
      alignSelf:'center'
    },
    gridRow1:{
      height:deviceHeight/5,
      alignItems:'center',
      justifyContent:'center'
    },
    gridImage:{
      marginTop: testMarginTop,
      marginBottom: testMarginTop,
      height:deviceHeight/6,
      width:deviceWidth/3
    },
    gridCol:{
      marginTop: testMarginTop,
      height:gridColHeight,
      alignItems:'center',
    },
    gridCol2:{
      marginTop: testMarginTop,
      marginBottom: testMarginTop,
      height:gridColHeight,
      alignItems:'center',
    },
    textSize:{
        fontSize:fsize3
    }
})

