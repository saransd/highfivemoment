import React from 'react';
import {StyleSheet,View,Image,Platform, PixelRatio,Dimensions} from 'react-native';
import {Container,Content,Grid,Row,Col,Text} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import Swiper from 'react-native-swiper';
import FooterComponent from '../components/footer';
import Icon from 'react-native-vector-icons/FontAwesome';
import {callQuery,callWatchQuery} from '../utils/query-handler';
import {ORG_BY_ID,QUERY_ORG_SEARCH} from '../constants';
import {withNavigation} from 'react-navigation';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');
var swepierHeight;
var lineWidth;
var swiperItemWidth;
var imageHeight1; //60
var imageHeight2; //70
var fontweight;
var fsizetitle;
var fsizetitle_sub;
var fsize;

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      lineWidth = deviceWidth/3
      lineWidth2 = deviceWidth/2.7
      swepierHeight = deviceHeight/5
      swiperItemWidth = deviceWidth/5.5
      imageHeight1 = deviceWidth/7
      imageHeight2 = deviceWidth/6
      fontweight = '700'
      
      fsizetitle=27  //24
      fsizetitle_sub = 23  //18
      fsize = 20    //12
    }
    else{
      lineWidth = deviceWidth/3
      lineWidth2 = deviceWidth/2.7
      swepierHeight = deviceHeight/5
      swiperItemWidth = deviceWidth/5.5
      imageHeight1 = deviceWidth/7
      imageHeight2 = deviceWidth/6
      fontweight = '700'
      
      fsizetitle=24  //24
      fsizetitle_sub = 20  //18
      fsize = 18    //12
    }
  }
  else{
    swiperItemWidth = deviceWidth/3.4
    imageHeight1 = deviceWidth/5.5
    imageHeight2 = deviceWidth/4.9
    lineWidth = deviceWidth/4
    lineWidth2 = deviceWidth/3.3
    fontweight = '700'
    fsizetitle=24  //24
    fsizetitle_sub = 20  //18
    fsize = 18    //12
    swepierHeight = deviceHeight/5
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      imageHeight1 = deviceWidth/6.5
      fontweight = '500'
      imageHeight2 = deviceWidth/5.2
      fsizetitle=22
      fsizetitle_sub = 18
      fsize = 12
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      fsizetitle=22
      fsizetitle_sub = 18
      fsize = 13
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      fsizetitle=23
      fsizetitle_sub = 19
      fsize = 14
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      
    }
  }

class Stars extends React.Component {
    static navigationOptions = {
        header:null
    }
    constructor(props)
    {
        super(props)
        this.state = {
            organization : {},
            employees:[],
            orgHighFive : 0
        }
    }

    componentDidMount()
    {
      callWatchQuery(QUERY_ORG_SEARCH("Test Org.*"),(result)=>{
          let organizationResult = result.organizations[0];
         // let users = result.users;
          if(organizationResult)
          {
             this.setState({organization:organizationResult.organization,
                        //employees:users,
                        //orgHighFive:organizationResult.organization.highFiveScore
                    });
          }
      })
    }

    getIcon(name)
    {
       return <Icon name={name} color="#C4C4C4" size={30}/>;
    }

    render()
    {
        const row1 = [{imgsrc:require('../assets/images/user2.png'),name:"Jennifer F",job:"Nurse"},
                      {imgsrc:require('../assets/images/user1.png'),name:"Adam L",job:"Server"},
                      {imgsrc:require('../assets/images/user1.png'),name:"Ed S",job:"Bus Driver"}
        ];
        const row2 = [
          {imgsrc:null,name:"Kristen Bell",job:"Cashier"},
          {imgsrc:{uri:"https://thfmimages.blob.core.windows.net/organisation/UrbanFuel/h5/business5%402x.png"},name:"Adam Levine",job:"Manager",route:true},
          {imgsrc:null,name:"Ed sheeran",job:"Server"}
        ];
        const block = [[{imgsrc:require('../assets/images/restaurant.png'),name:"Restaurants"},
                        {imgsrc:require('../assets/images/healthcare.png'),name:"Healthcare"},
                        {imgsrc:require('../assets/images/airlines.png'),name:"Airlines"},
        ],[{imgsrc:require('../assets/images/coffee.png'),name:"Coffee"},
                        {imgsrc:require('../assets/images/sports.png'),name:"Sports"},
                        {imgsrc:require('../assets/images/music.png'),name:"Music"},
        ]]
        return( <Content  style={{paddingTop:deviceHeight/55,backgroundColor:'white'}} contentContainerStyle={{alignItems:'center',justifyContent:'center'}}>
                     <Text style={{fontSize:fsizetitle}}>STARS IN YOUR AREA</Text>
                     <View style={[{flexDirection:'row',alignItems:'center',justifyContent:'center'}]}>
                       <View style={styles.leftLine}/>
                       <View><Text  style={{fontFamily:'Roboto',fontSize:fsizetitle_sub}}>{' '}By INDIVIDUAL{' '}</Text></View>
                       <View style={styles.rightLine}/>
                     </View>
                     <Swiper style={styles.swiper1} 
                             showsPagination={false} 
                             showsButtons={true} 
                             prevButton={this.getIcon("caret-left")} 
                             nextButton={this.getIcon("caret-right")}>
                        {row1.map((ele,index) => {
                       return <Grid key={"grid-"+index}>
                            <Row style={{alignItems:'center',justifyContent:'center'}}> 
                             {row1.map((item)=>{
                                return<Col style={{width:swiperItemWidth,alignItems:'center',justifyContent:'center'}} key={item.name}>
                                          <Image style={{height:imageHeight1,width:imageHeight1}} source={item.imgsrc}/>
                                          <Text style={{fontSize:fsize,fontWeight:fontweight}}>{item.name}</Text>
                                          <Text style={{fontSize:fsize}}>{item.job}</Text>
                                      </Col>
                             })}
                             </Row>
                         </Grid>
                         })
                         }
                         </Swiper>
                     <View style={[{flexDirection:'row',alignItems:'center',justifyContent:'center'}]}>
                       <View style={styles.leftLine}/>
                       <View><Text style={{fontFamily:'Roboto',fontSize:fsizetitle_sub}}>{' '}BY BUSINESS{' '}</Text></View>
                       <View style={styles.rightLine2}/>
                     </View>
                     <Swiper style={styles.swiper1} 
                             showsPagination={false} 
                             showsButtons={true} 
                             prevButton={this.getIcon("caret-left")} 
                             nextButton={this.getIcon("caret-right")}>
                         {row2.map((ele,index) => {
                         return <Grid key={"grid-row2-"+index}>
                            <Row style={{alignItems:'center',justifyContent:'center'}}> 
                             {row2.map((item,index)=>{
                                return<Col style={{width:swiperItemWidth,height:swiperItemWidth, alignItems:'center',justifyContent:'center'}} key={"row2-"+index} onPress={()=>{item.route ? this.props.navigation.navigate("Organization",{org:this.state.organization,employees:this.state.employees,orgHighFive:this.state.orgHighFive,img:item.imgsrc.uri}):{}}}>
                                          {item.imgsrc ? <Image style={{height:imageHeight2,width:imageHeight2,resizeMode:'contain'}} source={item.imgsrc}/> : <View style={{height:70,width:70,backgroundColor:"#A2D9EB"}}/>}
                                      </Col>
                             })}
                             </Row>
                         </Grid>
                         })}
                       </Swiper>

                      <View style={[{flexDirection:'row',alignItems:'center',justifyContent:'center'}]}>
                       <View style={styles.leftLine2}/>
                       <View><Text style={{fontFamily:'Roboto',fontSize:fsizetitle_sub}}>{' '}BY TYPE{' '}</Text></View>
                       <View style={styles.rightLine2}/>
                     </View>
                     <Grid style={{marginBottom:deviceHeight/60}}>
                         {block.map((arr,index) => {
                         return <Row key={"block-"+index} style={{paddingTop:5}}>
                                 {arr.map((item,i)=>{
                                   return<Col style={{width:deviceWidth/3,alignItems:'center',justifyContent:'center',marginBottom:deviceHeight/70}} key={item.name}>
                                          <Image style={{height:imageHeight1,width:imageHeight1}} source={item.imgsrc}/> 
                                          <Text style={{fontSize:fsize}}>{item.name}</Text>
                                      </Col>
                                 })} 
                             <Col>
                             </Col>
                         </Row>})}
                     </Grid>
                 </Content>)
    }
}

export default withNavigation(Stars);


let styles = StyleSheet.create({
  swiper1:{
    width:deviceWidth,
    height:swepierHeight,
  },
  colWithBg : {
    width:90,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:"#A2D9EB",
    marginRight:10
  },
  colWithoutBg : {
    width:90,
    alignItems:'center',
    justifyContent:'center',
    marginRight:10
  },
  rightLine:{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width:lineWidth
  },
  leftLine:{
    borderBottomColor: '#4f4f4f',
    borderBottomWidth: 1,
    width:lineWidth
  },
  rightLine2:{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width:lineWidth2
  },
  leftLine2:{
    borderBottomColor: '#4f4f4f',
    borderBottomWidth: 1,
    width:lineWidth2
  },

})