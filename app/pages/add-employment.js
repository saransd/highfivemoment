import React from 'react';
import {StyleSheet,Alert,TouchableOpacity} from 'react-native';
import {Container,Content,Grid,Column,Item,Input,Text,Button,Form,Col} from 'native-base';
import PickerComponent from '../components/picker';
import Autocomplete from 'react-native-autocomplete-input'
import AutoCompleteComponent from '../components/auto-complete'
import {QUERY_ADD_EMPLOYMENT} from '../constants';
import {callMutation} from '../utils/query-handler';
import {GetDateTime} from '../utils/date-functions';
import {Loader} from '../components/loader';
import _ from 'lodash';

const pageName = "Profile";

const eventName = "Add Employment"

const EmploymentStatus = ["Active","Dormant","Inactive"];

const EmpRole = ["administrator","supervisor","employee"];


export default class AddEmployment extends React.Component{

     static navigationOptions = {
        header:null
    }

  constructor(props){
      super(props)
      this.state = {
            organizations:props.navigation.state.params.organizations,
            users:props.navigation.state.params.users,
            pageName : props.navigation.state.params.pageName,
            employment: props.navigation.state.params.employment ? props.navigation.state.params.employment : [],
            orgName : "",
            userName : "",
            departments:[],
            jobs:[],
            locations:[],
            selectedOrg:"",
            selectedUser:"",
            selectedJob : "",
            selectedDept : "",
            selectedLocation:"",
            empStatus:"Active",
            selectedRole:"administrator",
            empCode:"",
            childThis:"",
            childThis2:"",
            btnEnabled:false,
            clearData : false,
            loading:false
      }
      this.initialState = this.state;
  }

  componentDidMount()
  {
       if(this.state.employment.length > 0){

           const {employment} = this.state;
           let orgId = "";
           let orgName = "";
           let filteredOrg =[];
           let org = {};
           employment.map((item,index) => {
           orgId = item.organization.id;
           orgName = item.organization.name;
           filteredOrg = _.filter(this.state.organizations,(obj)=>{
                return (orgId === obj.organization.id && item.role === "administrator")
           });
           if(filteredOrg[0]){
                return;
           }
        })
           org = filteredOrg[0] ? filteredOrg[0].organization : {}
           this.setState({orgName})
           
           if(Object.keys(org).length > 0 )
           {
            this.setOrganization(org)
           }
       }
  }

  setOrganization = (org) => 
    {
      if(org)
      {
        this.setState({selectedOrg:org.id,
                       departments:org.departments,
                       jobs:org.jobs,
                       locations:org.locations,
                       selectedDept:org.departments[0].id,
                       selectedJob:org.jobs[0].id,
                       selectedLocation:org.locations[0].id
                    });
      }
    }

    setUser = (user) => {
        this.setState({selectedUser:user.id});
    }

     onDeptChange(value)
    {
        this.setState({selectedDept:value});
    }

    onJobChange(value)
    {
        this.setState({selectedJob:value});
    }

    onLocationChange(value)
    {
         this.setState({selectedLocation:value})
    }

    onEmpStatusChange(value){
        this.setState({empStatus:value})
    }

    onRoleChange(value){
         this.setState({selectedRole:value})
    }

    checkForm = (childThis) => {
     if(this.state.employment.length > 0 ? this.state.userName : this.state.selectedUser && this.state.selectedOrg && this.state.selectedDept && this.state.selectedJob &&
        this.state.empCode && this.state.selectedRole && this.state.empStatus && this.state.selectedLocation)
        {
              this.setState({btnEnabled:true});
        }
        else{
            this.setState({btnEnabled : false });
        }
        if(!this.state.childThis){
            this.setState({childThis});
        }
    }

     checkForm2 = (childThis2) => {
     if(this.state.employment.length > 0 ? this.state.userName : this.state.selectedUser && this.state.selectedOrg && this.state.selectedDept && this.state.selectedJob &&
        this.state.empCode && this.state.selectedRole && this.state.empStatus && this.state.selectedLocation)
        {
              this.setState({btnEnabled:true});
        }
        else{
            this.setState({btnEnabled : false });
        }
        if(!this.state.childThis2){
            this.setState({childThis2});
        }
    }

    addEmployment = () => {
        let selectedUser = "";
        let userId = "";
      if(this.state.employment.length > 0){
         _.filter(this.state.users,(obj)=>{
          obj.emails.map((value)=>{
                if(value === this.state.userName)
                {
                    userId = obj.id
                    return;
                }
            })
        })
      }
      

     let employmentObj =   {
    "employment": {
      "userId": userId ? userId: this.state.selectedUser,
      "organizationId": this.state.selectedOrg,
      "departmentId": this.state.selectedDept,
      "jobId": this.state.selectedJob,
      "code": this.state.empCode,
      "role": this.state.selectedRole,
      "status": this.state.empStatus,
      "locationId": this.state.selectedLocation, 
      "start":GetDateTime(),
      "end":GetDateTime("add")

        }
     }

    if(this.state.btnEnabled)
    {
    this.setState({loading:true});
    callMutation(QUERY_ADD_EMPLOYMENT(employmentObj),employmentObj,pageName,eventName,(result)=>{
       
        this.setState({loading:false});
        
        let msg1 = "";
        let msg2 = "";

        if(typeof result === "object" && result.data && result.data.addEmployment)
        {
           msg1 = "Add Employment";
           msg2 = "Employment Added Successfully";
        }

        if(typeof result === "string"){
            if(result.contains("User already has an employment record for organization"))
            {
             msg1 = "User already has an employment record for the organization"
             msg2= "Please try another user or organization"
            }
            if(result.contains("Unable to find a user with the id"))
            {
              msg1= "Unable to find a user with the id"
              msg2= "Please enter a valid user"
            }
            if(!msg1 && !msg2)
            {
                msg1 = "Error"
                msg2 = "Unable to Add Employment"
            }
        }

         Alert.alert(
           msg1,
           msg2,
           [
         {text: 'OK', onPress: () => console.log('OK Pressed')},
         ],
         { cancelable: false }
         )
        if(this.state.employment.length === 0 && typeof result==="object")
        {
         this.state.childThis.setState(this.state.childThis.initialState);
         this.state.childThis2.setState(this.state.childThis2.initialState)
         this.setState(this.initialState);
        }
        else if(typeof result==="object"){
            this.setState(this.initialState);
        }
     })
    }
   
    }




    render(){
        return(<Container style={{backgroundColor:"darkgrey"}}>
                {this.state.loading ? <Loader/> : null}
                  <Content>
                       <Col style={{marginTop:20,padding:10}}>
                     <Form>
                      <Item regular style={styles.itemBorder}>
                          <Text> Add Employment </Text>
                      </Item>
                      <Item regular style={[styles.autocompleteContainer,styles.itemBorder]}>
                          {this.state.orgName ? <Input style={{backgroundColor:'#FFF',height:40,fontSize:14}} editable={false} value={this.state.orgName} onBlur={()=>{this.checkForm()}} /> : 
                          <AutoCompleteComponent 
                                dataToFilter={this.state.organizations} 
                                dataType="org" 
                                placeholder="Enter an Organization"  
                                setOrganization={(org)=>{this.setOrganization(org)}}
                                checkForm={(childThis)=>{this.checkForm(childThis)}}/>}
                      </Item>
                       <Item regular style={[styles.autocompleteContainerTwo,styles.itemBorder]}>
                           {this.state.orgName ? <Input value={this.state.userName} style={{backgroundColor:'#FFF',height:40,fontSize:14}} onBlur={()=>{this.checkForm()}} onChangeText={(userName)=>{this.setState({userName})}} /> : 
                         <AutoCompleteComponent 
                         dataToFilter={this.state.users} 
                         dataType="user" 
                         placeholder="Enter a User" 
                         setUser={(user)=>{this.setUser(user)}}
                         checkForm={(childThis2)=>{this.checkForm2(childThis2)}}
                         />}
                      </Item>
                      <Item regular style={[{marginTop:95},styles.itemBorder]}>
                        <PickerComponent value={this.state.selectedDept} options={this.state.departments} type="dept" action={(value)=>{this.onDeptChange(value)}}/>
                      </Item>
                      <Item regular style={styles.itemBorder}>
                         <PickerComponent value={this.state.selectedJob} options={this.state.jobs} type="job" action={(value)=>{this.onJobChange(value)}}/>
                      </Item> 
                         <Item regular style={styles.itemBorder}>
                         <PickerComponent value={this.state.selectedLocation} options={this.state.locations} type="location" action={(value)=>{this.onLocationChange(value)}}/>
                      </Item>
                       <Item regular style={styles.itemBorder}>
                         <Input placeholder="Enter Employee Code" 
                          style={{backgroundColor:'#FFF',height:40,fontSize:14}} 
                          value={this.state.empCode}
                          onChangeText={(empCode)=>this.setState({empCode})}
                          onBlur={()=>{this.checkForm()}}
                          />
                      </Item>
                      <Item regular style={styles.itemBorder}>
                         <PickerComponent value={this.state.selectedRole} options={EmpRole} type="empRole" action={(value)=>{this.onRoleChange(value)}}/>
                     </Item>
                     <Item regular style={styles.itemBorder}>
                         <PickerComponent value={this.state.empStatus} options={EmploymentStatus} type="empStatus" action={(value)=>{this.onEmpStatusChange(value)}}/>
                     </Item>
                     <Item regular style={[{alignItems:'center',justifyContent:'center',paddingTop:10},styles.itemBorder]} >
                         <Button style={[styles.btnStyle,this.state.btnEnabled ? styles.btnEnabledColor : styles.btnDisabledColor]} 
                         onPress={()=>{this.addEmployment()}} 
                         disabled={!this.state.btnEnabled}>
                         <Text>Add</Text></Button>
                         <Button style={[styles.btnStyle,styles.btnEnabledColor,styles.cancelBtnMargin]} 
                         onPress={()=>{this.props.navigation.goBack()}}>
                         <Text>Cancel</Text> 
                         </Button>
                     </Item>
                     </Form>
                     </Col>
                  </Content>
              </Container>)
    }
}




const styles = StyleSheet.create({
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 30,
    zIndex: 2
  },
   autocompleteContainerTwo: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 75,
    zIndex: 1
  },
  itemBorder:{
    borderColor:"transparent"
  },
  btnStyle:{
   width:125,
   justifyContent:'center',
   alignItems:'center'
  },
  btnEnabledColor:{
  backgroundColor:"#334149"
  },
  btnDisabledColor:{
  backgroundColor:"grey"
},
cancelBtnMargin : {
     marginLeft:5
}
});
