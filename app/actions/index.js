
export const getProfile = (obj) => {
    return dispatch => {
        dispatch({
            type:"GET_PROFILE",
            payload:{profileObj:obj}
        })
    }
}

export const getOrganizations = (orgs) => {
    return dispatch => {
         dispatch({
              type:"ORGANIZATIONS",
              payload : {organizations:orgs}
         })
    }
}

export const getUsers = (users) => {
    return dispatch => {
         dispatch({
             type:"USERS",
             payload:{users:users}
         })
    }
}