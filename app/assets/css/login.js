const React = require('react-native');

const { StyleSheet, Dimensions ,PixelRatio } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');
import {Platform} from 'react-native';

var fsizetitle;
var fsize;
var i_size;
var mgleft;
var fsizeFP;
var fsizetitle_sub;
var FPpaddingRight;
var logoMarginTop;
var lineWidth;
var containerWidth1;
var logoHeight;

if(DeviceInfo.isTablet()){
  fsizeFP=18
  fsizetitle=24
  fsize = 20
  i_size = 18
  fsizetitle_sub = 22
  FPpaddingRight = deviceWidth/60
  if(Platform.OS === 'ios'){
    logoMarginTop = 0
  }
  else{
    logoMarginTop= deviceHeight/10.5
  }
  lineWidth = deviceWidth/4
  containerWidth1 = deviceWidth/1.5
  logoHeight = deviceHeight/5
}
else{
  lineWidth = deviceWidth/4.5
  if(PixelRatio.get()>=0&&PixelRatio.get()<1){
    fsizetitle = 16
    fsize = 13
    fsizeFP =12
    i_size = 16
    fsizetitle_sub = 16
    FPpaddingRight = deviceWidth/60
    if(Platform.OS === 'ios'){
      logoMarginTop = 0
    }
    else{
      logoMarginTop= deviceHeight/15
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/4.8
  }
  else if(PixelRatio.get()>=1&&PixelRatio.get()<1.5){
    fsizetitle = 16
    fsize = 13
    fsizeFP =13
    i_size = 16
    fsizetitle_sub = 16
    FPpaddingRight = deviceWidth/60
    if(Platform.OS === 'ios'){
      logoMarginTop = 0
    }
    else{
      logoMarginTop= deviceHeight/30
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/5.5
  }
  else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
    fsizetitle = 17
    fsize = 14
    fsizeFP =13
    i_size = 16
    fsizetitle_sub = 16
    FPpaddingRight = deviceWidth/60
    if(Platform.OS === 'ios'){
      logoMarginTop = deviceHeight/14 //52
    }
    else{
      logoMarginTop= deviceHeight/20
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/5.2
  }
  else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
    fsizetitle = 18
    fsize = 14
    fsizeFP =14
    i_size = 16
    fsizetitle_sub = 16
    FPpaddingRight = deviceWidth/60
    if(Platform.OS === 'ios'){
      logoMarginTop = deviceHeight/13 //102
    }
    else{
      logoMarginTop=deviceHeight/15        //72
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/5
  }
  else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
    fsizetitle = 18
    fsize = 15
    fsizeFP =14
    i_size = 16
    fsizetitle_sub = 16
    FPpaddingRight = deviceWidth/60
    if(Platform.OS === 'ios'){
      logoMarginTop = deviceHeight/13 //102
    }
    else{
      logoMarginTop=deviceHeight/10.5          //72
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/4.8
  }
  else{
    fsizetitle = 20
    fsizeFP =15
    fsize = 16
    i_size = 16
    fsizetitle_sub = 18
    FPpaddingRight = deviceWidth/60
    if(Platform.OS === 'ios'){
      logoMarginTop = deviceHeight/14 //102
    }
    else{
      logoMarginTop=deviceHeight/10         //72
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/4.8
  }
}

export default {
  forgotPassText:{
    fontFamily:'Roboto',
    color:'#6B6161',
    fontSize:fsizeFP,
    alignSelf:'flex-end',
    paddingRight:FPpaddingRight,
    paddingTop: FPpaddingRight,
    paddingBottom: FPpaddingRight,
  },
  signInText:{
    fontFamily: "Roboto",
    fontSize: fsizetitle,
    fontWeight: "300",
    fontStyle: "normal",
    letterSpacing: 0,
    color: "#4f4f4f"
  },
  leftLine:{
    borderBottomColor: '#4f4f4f',
    borderBottomWidth: 1,
    width:lineWidth
  },
  rightLine:{
    borderBottomColor: '#4f4f4f',
    borderBottomWidth: 1,
    width:lineWidth
  },
  logo:{
    marginTop: logoMarginTop
  },
  address:{
    width:deviceWidth/2
  },
  loginButton:{
    alignSelf:'center',
    width:containerWidth1,
  },
  containerSocialLogin2:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    paddingBottom:deviceHeight/40
  },
  containerSocialLogin1:{
    flex:1,
    justifyContent:'flex-end',
    paddingBottom:deviceHeight/35
  },


  thfLogoForm :{
    width:containerWidth1,
    height: logoHeight,  //150,
    resizeMode:'contain',
    alignSelf:'center',
  },
  formWidth:{
    width: containerWidth1,  //290,
    alignItems:'center',
    justifyContent:'center',
  },

};
