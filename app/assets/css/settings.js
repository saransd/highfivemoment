const React = require('react-native');

const { StyleSheet, Dimensions ,PixelRatio } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');
import {Platform} from 'react-native';

  var fontSubsize;
  var fontSubsize1;
  var fontMainSize;
  var fontSizeHead;

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      fontSubsize = 16
      fontSubsize1 = 18
      fontMainSize = 20
      fontSizeHead = 21
    }
    else{
      fontSubsize = 14
      fontSubsize1 = 16
      fontMainSize = 18
      fontSizeHead = 19
    }
  }
  else{
    if(PixelRatio.get()>=0&&PixelRatio.get()<1){
      fontSubsize = 12
      fontSubsize1 = 13
      fontMainSize = 14
      fontSizeHead = 15
    }
    else if(PixelRatio.get()>=1&&PixelRatio.get()<1.5){
      fontSubsize = 12
      fontSubsize1 = 13
      fontMainSize = 14
      fontSizeHead = 15
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      fontSubsize = 13
      fontSubsize1 = 15
      fontMainSize = 17
      fontSizeHead = 18
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      fontSubsize = 13
      fontSubsize1 = 15
      fontMainSize = 17
      fontSizeHead = 18
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      fontSubsize = 14
      fontSubsize1 = 16
      fontMainSize = 18
    }
    else{
      fontSubsize = 14
      fontSubsize1 = 16
      fontMainSize = 18
      fontSizeHead = 19
    }
  }

  export default {
    header:{
      backgroundColor:"#E5E5E5",
      alignItems:'center',
      justifyContent:'center',
    },
    footer:{
      backgroundColor:"#E5E5E5",
      alignItems:'center',
      justifyContent:'center'
    },
    subText:{
      fontSize:fontSubsize
    },
    subTextRow:{
      paddingLeft:deviceWidth/30,
      width:deviceWidth/1.3
    },
    subTextTitle:{
      flex:0.8,
      paddingLeft:deviceWidth/30,
      fontSize:fontSubsize1
    },
    switch:{
      flex:0.2
    },
    titleText:{
      width:deviceWidth/1.1,
      fontSize:fontMainSize
    },
    fontHead:{
      fontSize: fontSizeHead,
    }

  };