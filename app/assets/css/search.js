const React = require('react-native');

const { StyleSheet, Dimensions ,PixelRatio } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');
import {Platform} from 'react-native';

var fsizetitle;
var fsize;
var i_size;
var mgleft;
var fsizeFP;
var fsizetitle_sub;
var titleFontWeight;
var searchContainerHeight;
var searchbarWidth;
var searchbarHeight;
var bodyPadd;
var imageSize1;//50

if(DeviceInfo.isTablet()){
  if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
    fsizeFP=20
    fsizetitle=24
    fsize = 22
    i_size = 20
    fsizetitle_sub = 24
    titleFontWeight = '700'
    searchContainerHeight = deviceHeight/7.5
    searchbarHeight = 45
    searchbarWidth = deviceWidth-(deviceWidth/3)
    bodyPadd = 10
    imageSize1 = deviceWidth/9
  }
  else{  //PixelRatio = 2
    fsizeFP=18
    fsizetitle=22
    fsize = 20
    i_size = 18
    fsizetitle_sub = 22
    titleFontWeight = '700'
    searchContainerHeight = deviceHeight/7.5
    searchbarHeight = 35
    searchbarWidth = deviceWidth-(deviceWidth/2.5)
    bodyPadd = 10
    imageSize1 = deviceWidth/9
  }
}
else{
  imageSize1 = deviceWidth/7
  if(PixelRatio.get()>=0&&PixelRatio.get()<1){
    fsizetitle = 15
    fsize = 13
    fsizeFP =12
    i_size = 16
    fsizetitle_sub = 14
    titleFontWeight = '400'
    searchContainerHeight = deviceHeight/5
    searchbarHeight = 30
    searchbarWidth = deviceWidth-(deviceWidth/7)
    bodyPadd = 5
    imageSize1 = deviceWidth/7
  }
  else if(PixelRatio.get()>=1&&PixelRatio.get()<1.5){
    fsizetitle = 15
    fsize = 13
    fsizeFP =13
    i_size = 16
    fsizetitle_sub = 15
    titleFontWeight = '400'
    searchContainerHeight = deviceHeight/5
    searchbarHeight = 30
    searchbarWidth = deviceWidth-(deviceWidth/7)
    bodyPadd = 5
    imageSize1 = deviceWidth/7
  }
  else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
    fsizetitle = 17
    fsize = 14
    fsizeFP =13
    i_size = 16
    fsizetitle_sub = 16
    titleFontWeight = '700'
    searchContainerHeight = deviceHeight/5
    searchbarHeight = 35
    searchbarWidth = deviceWidth-(deviceWidth/8)
    bodyPadd = 7
    imageSize1 = deviceWidth/5
  }
  else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
    fsizetitle = 18
    fsize = 15
    fsizeFP =14
    i_size = 16
    fsizetitle_sub = 16
    titleFontWeight = '700'
    searchContainerHeight = deviceHeight/5.5
    searchbarHeight = 35
    searchbarWidth = deviceWidth-(deviceWidth/8)
    bodyPadd = 9
    imageSize1 = deviceWidth/6
  }
  else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
    fsizetitle = 18
    fsize = 16
    fsizeFP =14
    i_size = 16
    fsizetitle_sub = 16
    titleFontWeight = '700'
    searchContainerHeight = deviceHeight/6.5
    searchbarHeight = 35
    searchbarWidth = deviceWidth-(deviceWidth/9)
    bodyPadd = 10
    imageSize1 = deviceWidth/6.5
  }
  else{
    fsizetitle = 20
    fsizeFP =15
    fsize = 16
    i_size = 16
    fsizetitle_sub = 18
    titleFontWeight = '700'
    searchContainerHeight = deviceHeight/7
    searchbarHeight = 35
    bodyPadd = 10
    searchbarWidth = deviceWidth-(deviceWidth/9.5)
  }
}

export default {
  text:{
      fontFamily:'Roboto',
      fontSize:18
    },
    subText:{
     fontFamily:'Roboto',
     fontSize:12
    },
    yellowStrip:{
     paddingRight:5,
     paddingLeft:deviceWidth/30,
     paddingBottom:5,
     paddingTop:5,
     fontSize:fsizetitle,
     fontWeight: titleFontWeight,
     backgroundColor:'#FFE37A'
    },
    listBgColor:{
      backgroundColor:'#FFF'
    },
    backgroundImage:{
      flex: 1, 
      width:undefined, 
      height: searchContainerHeight, //125,
      alignItems:'center',
      justifyContent:'center'
    },
    searchBox: {
      borderWidth:1,
      borderColor:'white',
      backgroundColor:'white',
      width: searchbarWidth,
      height: searchbarHeight,
    },
    imageList:{
      height:imageSize1,
      width:imageSize1
    },
    listItemTitle:{
      fontSize:fsizetitle,
      fontWeight: titleFontWeight
    },
    bodyPadding:{
      paddingLeft:bodyPadd
    },
    subtextSize:{
      fontSize: fsizeFP
    }

};
