import {StyleSheet,Dimensions,Platform,PixelRatio} from 'react-native';
var window = Dimensions.get('window');
let {height,width} = window;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');

var serachBarWidth;
var serachBarHeight;
var logoWidth;//327
var logoHeight;//167
var containerWidth1;

  if(DeviceInfo.isTablet()){
    containerWidth1 = deviceWidth/1.5
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      serachBarWidth = deviceWidth/1.6
      serachBarHeight = 45
      logoHeight = deviceHeight/6
      logoWidth = deviceWidth/1.5
    }
    else{
      serachBarWidth = deviceWidth/1.6
      serachBarHeight = 35
      logoHeight = deviceHeight/6
      logoWidth = deviceWidth/1.5
    }
  }
  else{
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/6
    logoWidth = deviceWidth/1.5
    serachBarHeight = 35
    serachBarWidth = deviceWidth/1.1
    
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      serachBarHeight = 30
      logoHeight = deviceHeight/7
      logoWidth = deviceWidth/2
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      logoHeight = deviceHeight/6.5
      logoWidth = deviceWidth/1.7
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      logoHeight = deviceHeight/6.3
      logoWidth = deviceWidth/1.6
    }
  }

export const globalstyles = StyleSheet.create({
 logo : {
    width: logoWidth,
    height: logoHeight 
 },
textBox:{
  //width: 309,
 // height: 30,
  fontFamily: "Roboto",
  fontSize: 18,
  fontWeight: "300",
  fontStyle: "normal",
  letterSpacing: 0,
  color: "#4f4f4f",
  textAlign:'center',
  borderBottomWidth:1,
  borderBottomColor:'#000000'
 },
 formElementMargin:{
   marginTop:20
},
button : {
  //width: 309,
  height: 50,
  borderRadius: 40,
  backgroundColor: '#00446A',
  alignItems:'center',
  justifyContent:'center'
  //marginLeft:20
},
buttonText:{
  width: containerWidth1,
  height: 30,
  fontFamily: "Roboto",
  fontSize: 20,
  fontWeight: "normal",
  fontStyle: "normal",
  letterSpacing: 0,
  color: "#ffffff",
  textAlign:'center',
  paddingTop:5,
  textAlignVertical:'center'
},
buttonTextIos : {
  width: containerWidth1,
  height: 45,  
  fontFamily: "Roboto",
  fontSize: 20,
  fontWeight: "normal",
  fontStyle: "normal",
  letterSpacing: 0,
  color: "#ffffff",
  textAlign:'center',
  textAlignVertical:'bottom',
  marginTop:25
},
socialBtnLogin :{
 width: 155,
 height: 45,
 borderRadius: 40
},
socialBtnIcon:{
  color:'#fff',
  paddingLeft:15
},
socialBtnSignup : {
  color:"#fff"
},
tabImage : {
 width: 25,
 height: 25,
 resizeMode:'contain'
},
 tabIconTxt : {
  fontFamily: 'Roboto',
  fontSize: height = 480 && width === 320 ? 9 : 10,
  lineHeight: 14,
  color:'#ffffff',
  paddingTop:2,
 },
 searchBox: {
 borderWidth:1,
 borderColor:'white',
 backgroundColor:'white',
 width:serachBarWidth,
 height:serachBarHeight
},
footerColor:{
  backgroundColor: '#00446A'
},
thfLogoForm :{
 width:300,
 height:150,
 resizeMode:'contain',
 alignSelf:'center'
 },
formContent :{
  flex:1,
  flexDirection:'column',
  width:window.width,
  alignItems:'center'
},
containerBgColor:{
  backgroundColor:'#FFF'
},
formWidth:{
  width:290,
  alignItems:'center',
  justifyContent:'center'
},
text:{
   fontFamily: 'Roboto',
   fontWeight:'300'
},
subtext:{
    fontSize:14
},
footerTab:{  
  width: 414,
  alignItems:'center',
  justifyContent:'center'
  //height: 50,
  //backgroundColor: '#36454f'
},
footerTxt:{
 // width: 343,
  //height: 30,
  fontFamily: 'Roboto',
  fontSize: 16,
  fontWeight: "300",
  fontStyle: 'normal',
  letterSpacing: 0,
  color: "#ffffff"
},
gridPadding:{
  paddingTop:10
}
});