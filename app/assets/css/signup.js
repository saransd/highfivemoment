const React = require('react-native');

const { StyleSheet, Dimensions ,PixelRatio } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');
import {Platform} from 'react-native';

var fsizetitle;
var fsize;
var lineWidth;
var containerWidth1;
var logoHeight;
var fsize; //18
var inputHeight;//50
var logoMarginTop;//marginTop:Platform.OS === 'ios' ? 92 : 42
var backgroundHeight;

if(DeviceInfo.isTablet()){
  fsize = 18
  inputHeight=60
  if(Platform.OS === 'ios'){
    logoMarginTop = 0
  }
  else{
    logoMarginTop= deviceHeight/10.5
  }
  backgroundHeight = deviceHeight-(deviceHeight/3.5)
  containerWidth1 = deviceWidth/1.5
  logoHeight = deviceHeight/5
}
else{
  backgroundHeight = deviceHeight-(deviceHeight/3.1)
  fsize = 16
  inputHeight = 50
  if(PixelRatio.get()>=0&&PixelRatio.get()<1){
    if(Platform.OS === 'ios'){
      logoMarginTop = 0
    }
    else{
      logoMarginTop= deviceHeight/15
    }
    fsize = 14
    inputHeight = 40
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/4.8
  }
  else if(PixelRatio.get()>=1&&PixelRatio.get()<1.5){
    if(Platform.OS === 'ios'){
      logoMarginTop = 0
    }
    else{
      logoMarginTop= deviceHeight/30
    }
    fsize = 14
    inputHeight = 40
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/5.5
  }
  else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
    if(Platform.OS === 'ios'){
      logoMarginTop = deviceHeight/14 //52
    }
    else{
      logoMarginTop= deviceHeight/20
    }
    fsize = 15
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/5.2
  }
  else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
    if(Platform.OS === 'ios'){
      logoMarginTop = deviceHeight/13 //102
    }
    else{
      logoMarginTop=deviceHeight/15        //72
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/5
  }
  else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
    if(Platform.OS === 'ios'){
      logoMarginTop = deviceHeight/13 //102
    }
    else{
      logoMarginTop=deviceHeight/10.5          //72
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/4.8
  }
  else{
    if(Platform.OS === 'ios'){
      logoMarginTop = deviceHeight/14 //102
    }
    else{
      logoMarginTop=deviceHeight/10         //72
    }
    containerWidth1 = deviceWidth/1.4
    logoHeight = deviceHeight/4.8
  }
}

export default {
  thfLogoForm :{
    width:containerWidth1,
    height: logoHeight,  //150,
    resizeMode:'contain',
    alignSelf:'center',
  },
  textBox:{
    width: containerWidth1,
    fontFamily: "Roboto",
    fontSize: 18,
    fontWeight: "300",
    fontStyle: "normal",
    letterSpacing: 0,
    color: "#4f4f4f",
    textAlign:'center',
    borderBottomWidth:1,
    borderBottomColor:'#000000'
 },
 formWidth:{
    width: containerWidth1,
    borderColor:'#000',
    height:inputHeight,
  },
  signupButton:{
    alignSelf:'center',
    width:containerWidth1,
  },
  footerTxt:{
    fontFamily: "Roboto",
    fontSize: fsize,
    fontWeight: "300",
    fontStyle: "normal",
    letterSpacing: 0,
    color: "#FFF",
    paddingTop:deviceHeight/65,
    alignSelf:'center'
  },
  backgroundImage:{
    flex: 1, 
    width: undefined, 
    height: backgroundHeight,
    marginTop:10,
    alignItems:'center'
  },
  linkText:{
    color:'blue',
    textDecorationLine : 'underline'
  },
  signUpText:{
    fontFamily:'Roboto',
    fontSize:11
  },
  logo:{
    marginTop: logoMarginTop
  },
};
