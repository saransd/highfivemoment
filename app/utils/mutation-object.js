import {AsyncStorage} from 'react-native';
import {GetDateTime} from './date-functions'
import moment from 'moment';
import  momentTimeZone from 'moment-timezone';
import {Device} from '../constants';

export const createMutationObject = (empInfo,consumerInfo,orgInfo,highfives,attribs,feedback,eventId,callback) => {
   AsyncStorage.getItem('current-user').then((value) => {
   let user = JSON.parse(value);
   //let date = moment().format("YYYY-MM-DDTHH:mm:ss");
   //let datetime = momentTimeZone.tz(date, "America/Chicago").utc().format();
   
   let locationId = orgInfo.locations && orgInfo.locations.length > 0 ? orgInfo.locations[0].id : ""

      getGeoInfo((pos)=>{
        let crd = pos.coords;
        let obj = {};
        if(eventId){
        obj = Object.assign({},obj,{id:eventId});
        }
       obj =  Object.assign({},obj,{
        employeeId:empInfo.id,
        consumerId: consumerInfo.consumer ?  consumerInfo.consumer.id : "",
        organizationId: orgInfo.id,
        //givenName:user ? user.displayName ? user.displayName : user.email.split('@')[0] : "",
        //familyName:user ? user.displayName ? user.displayName : user.email.split('@')[0] : "",
        occuredOn : GetDateTime(),
        locationId : locationId,
        attributesAwarded:attribs,
        feedback:feedback.trim() ? feedback.trim() : null,
        geometry:{
          type:"Point",
          coordinates : [crd.latitude,crd.longitude]
        },
        deviceId:Device.id,
        active:true
        //latitude : crd.latitude,
        //longitude : crd.longitude,
        //createdAt : GetDateTime(),
        //totalHighFives: highfives,
       }); 
       console.log(obj)
       callback(obj);
      });

   });
}


export const createUpdateMutationObject = (eventId,attributeBreakdown = [],feedback = "",callback) => {
     let obj = {
        id : eventId,
        attributeBreakdown : attributeBreakdown,
        feedback:feedback.trim() ? feedback.trim() : null,
        lastModifiedAt: GetDateTime()
     }
     console.log(obj);
       callback(obj);
}

export const getGeoInfo = (callback) => {

     let options = {
      enableHighAccuracy : false,
      //timeout : 5000,
      //maximumAge : 0
     }
     error = (e) => {
       let obj = {
          coords : {
            latitude:0,
            longitude:0
          }
       }
       callback(obj,e);
        console.log(e.message);
     }
     success = (pos) => {
       callback(pos);
    }
    
    navigator.geolocation.getCurrentPosition(success,error,options);
}