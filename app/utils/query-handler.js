import { AsyncStorage,Platform,Alert } from 'react-native';
import { withNavigation } from 'react-navigation';
import ApolloClient from 'apollo-client';
import {InMemoryCache,HttpLink} from 'apollo-boost';
import {ApolloProvider,Query} from 'react-apollo';
import { persistCache,CachePersistor } from 'apollo-cache-persist';
import { setContext } from 'apollo-link-context';
import {QUERY_API} from '../constants';
import gql from "graphql-tag";
import firebase from './firebase-config';
import {CreateLog} from '../constants';
import 'whatwg-fetch';

const AuthApiKey = "AIzaSyAwhvwCTg-fKtcD794IOrlcn8GEGvKAcLQ";

const cache = new InMemoryCache();
const link = new HttpLink({ uri: QUERY_API })

let counter = 0;

/*persistCache({
  cache,
  storage: AsyncStorage
});*/

export const persistor = new CachePersistor({ cache,
  storage: AsyncStorage
});

const getBearerToken = async () => { 
    const token =  await AsyncStorage.getItem('user-token')
    return token ? `Bearer ${JSON.parse(token)}` : ""
}

export const callSetContext = () => {

return setContext(async (req, { headers }) => {
  const token = await getBearerToken()
  console.log(token)
  return {
    headers: {
      ...headers,
      Authorization: token
    }
  }
})
}

const authLink =  callSetContext();


export const client = new ApolloClient({
   link : authLink.concat(link),
   cache : cache
});

export const callQuery = (queryString,pageName,eventName,callback) =>
{
 const query = gql`${queryString}`;
  
   client
  .query({
    query: query
  })
  .then(result => { 
     callback(result)
  }).catch((e) => { 
      console.log(eventName);
      console.log(JSON.stringify(e))
      createErrorLog(pageName,eventName,"EventLog",e);
      updateUserToken(e,"query",queryString,null,pageName,eventName,callback)
  });
}

export const callQueryNetwork = (queryString,pageName,eventName,callback) =>
{
 const query = gql`${queryString}`;
  
 client
  .query({
    query: query,
     fetchPolicy: 'network-only'
  })
  .then(result => { 
    callback(result)
  }).catch((e) => { 
    console.log(e)
    updateUserToken(e,"queryNetwork",queryString,null,pageName,eventName,callback)
  });
}

export const callMutation = (queryString,variables,pageName,eventName,callback) => 
{
  const query = gql`${queryString}`; 
  client.mutate({
    variables:variables,
    mutation: query
  })
  .then(result => {
     callback(result)
  }).catch((e)=>{
    console.log(e);
      createErrorLog(pageName,eventName,"EventLog",e);
     let Graphqlerror = "";
      for(var m in e)
      {
          if(m === "message") {
            Graphqlerror = e[m];
          }
      }
      Graphqlerror = Graphqlerror ? Graphqlerror.split("GraphQL error:")[1] ? Graphqlerror.split("GraphQL error:")[1].trim() : "" : ""; 
      if(Graphqlerror === "A user already exsits with the userId supplied")
      {
         callback(Graphqlerror);
      }
      if(eventName === "Add Employment")
      {
         callback(Graphqlerror);
      }
      if(e && e.errors && e.errors[0])
      {
        if(errors[0].message === "A user already exsits with the userId supplied")
         {
            callback(e)
         }
      }
     updateUserToken(e,"mutation",queryString,variables,pageName,eventName,callback)
  });
}

export const callWatchQuery = (queryString,callback) =>
{
 const query = gql`${queryString}`;
 
 const observableQuery = client
  .watchQuery({
    query,
    pollInterval: 3600000
  })
  
observableQuery.subscribe({
  next: ({ data }) => {
     callback(data);
  }
})

}

export const callHighFiveQuery = (queryString,callback) =>
{
 const query = gql`${queryString}`;
 
 const observableQuery = client
  .watchQuery({
    query,
    pollInterval: 5000
  })
  
observableQuery.subscribe({
  next: ({ data }) => {
     callback(data);
  }
})

}

export const updateUserToken = (error,type,queryString,variables,pageName,eventName,callback) => {
    if(error && error.graphQLErrors && error.graphQLErrors.length > 0 && error.graphQLErrors[0].message.statusCode === 403)
    {
       counter++;
       if(counter <= 3)
       {
          checkForAuth(type,queryString,variables,pageName,eventName,callback);
       }
       if(counter > 3)
       {
         if(eventName.toLowerCase() === "onloginclick")
         {
          callback({data:{users:[]}});
         }
         else {
            /*Alert.alert(
        'There is problem while processing the request.Your session may have expried',
        'Please Login again',
        [{text: 'Ok', onPress: () => console.log("ok")}],
        { cancelable: false }
      )*/
            alert("There is problem while processing the request. Please contact System Administrator");
         }
       }
    }
}

  export const getRefreshToken = () => {
            let user = firebase.auth().currentUser;
            let token = null;
            if(user)
            {
              token = user.refreshToken
            }
           return token;
    }

export const checkForAuth = (type,queryString,variables,pageName,eventName,callback) => {
       
    let token = getRefreshToken();
   //let reqBody = `grant_type=refresh_token&refresh_token=${token}`
    if(token)
    {
      refreshToken(token,type,queryString,variables,pageName,eventName,callback);
    }
    else {
       token = getRefreshToken();
       if(token)
       {
         refreshToken(token,type,queryString,variables,pageName,eventName,callback);
       }
    }
    
      /*  if(user){          user.getIdToken().then((userToken)=>{
             let reqBody =  {"token":userToken,"returnSecureToken":true};
             refreshToken(reqBody);
          })
        }*/
}

export const refreshToken =  (token,type,queryString,variables,pageName,eventName,callback) => {
    fetch(`https://securetoken.googleapis.com/v1/token?key=${AuthApiKey}&grant_type=refresh_token&refresh_token=${token}`,{
      method:"post",
      headers:{
        "Content-Type" : "application/x-www-form-urlencoded"
      },
     // body : JSON.stringify(reqBody)
    }).then((res)=> {
             return res.json()
    }).then((response)=>{
        if(response && Object.keys(response).length > 0 && response.access_token)
        {
          AsyncStorage.setItem('user-token',JSON.stringify(response.access_token));
          callSetContext();
          recallQuery(type,queryString,variables,pageName,eventName,callback)
        }
        if(response && response.error && Object.keys(response.error).length > 0 && response.error.message)
        {
               console.log("Invalid request")     
        }
    }).catch((error)=>{
       console.log(error);
    })
}

export const recallQuery = (type,queryString,variables,pageName,eventName,callback) => {
   switch(type) {
     case "query" : 
        callQuery(queryString,pageName,eventName,callback);
     break;
     case "queryNetwork" : 
         callQueryNetwork(queryString,callback);
     break;
     case "mutation" : 
         callMutation(queryString,variables,pageName,eventName,callback);
     break;
   }
}


export const GenerateConsumerHighFive = () => {
    AsyncStorage.getItem('current-user').then((value)=>{
      let user = JSON.parse(value);
      let consumerId = user.uid;
      let storageVarName = "highfives-"+consumerId;
    AsyncStorage.getItem(storageVarName).then((value)=>{
      let totalHighFive = 0;
        if(value){
           totalHighFive = JSON.parse(value) + 1;
        }
        else{
           totalHighFive = totalHighFive + 1;
        }
      AsyncStorage.setItem(storageVarName,JSON.stringify(totalHighFive));
    })
    })
}


const createErrorLog = (pageName,eventName,LogType,e) => {
     let error = "";
     if(e.graphQLErrors.length === 0)
     {
     for(var m in e)
      {
          if(m === "message") {
            error = e[m];
          }
      }
     }
      if(e && e.graphQLErrors && e.graphQLErrors.length > 0 && e.graphQLErrors[0].message.statusCode === 403)
      {
        CreateLog(pageName,eventName,"Exception",e.graphQLErrors[0].message.message );
      }
     if(error)
      {
         CreateLog(pageName,eventName,"Exception",error);
      }
      if(e && e.errors && e.errors[0])
      {
        if(errors[0].message)
         {
             CreateLog(pageName,eventName,"Exception",errors[0].message);
         }
      }
}