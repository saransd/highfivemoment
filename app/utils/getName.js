export const getName = (empName) => {
  let givenName = empName.givenName.split(" ")[0] ? empName.givenName.split(" ")[0] : empName.givenName;
  let middleName = empName.middleName ? empName.middleName : "";
  let familyName =  empName.familyName.split(" ")[1] ? empName.familyName.split(" ")[1] : empName.familyName;
    return givenName.trim() + " " + middleName + " " + (familyName ? familyName.trim().charAt(0) +  "." : "");
}

export const getJobTitle = (employment,org) => {
    let empPosition = "";
     if(employment && employment.length > 0)
      {
        employment.map((item,index)=>{
         if(item && item.organization && org && (org.id === item.organization.id))
         {
            empPosition = item.job ? item.job.name ? item.job.name : "" : "";
         }
        })
       }
        if(!empPosition && employment[0]){
           empPosition = employment[0].job ? employment[0].job.name ? employment[0].job.name : "" : "";
        } 
    return empPosition;
}