import {AsyncStorage} from 'react-native';
import {callMutation,callQuery} from '../utils/query-handler';
import {QUERY_ADD_CONSUMER,QUERY_EMP_BY_EMAIL,QUERY_UPDATE_EMP_ID,Device} from '../constants';


export const createCustomer = (user,latitude,longitude,pageName,eventName,callback) => {
     AsyncStorage.setItem('current-user',JSON.stringify(user));
     console.log("inside create customer function")
    if (user !== null && pageName.toLowerCase() !== "signup") {
                   callQuery(QUERY_EMP_BY_EMAIL(user.email),pageName,eventName,(result)=>{
                     console.log("***************************")
                     console.log("inside the emp by email query")
                   //   let consumerEmp = null;
                       if(!result.data.users[0]){
                          /*let employee = result.data.employee;
                          let id = employee.id;
                          let empId = employee.employeeId;
                          let orgId = employee.organization.organizationCoreData.organizationId;
                          let orgName = employee.organization.organizationCoreData.organizationName;
                          let newEmpId = orgId+"-"+user.uid;
                          let obj = {
                                id:id,
                                employeeId:newEmpId
                          }
                        consumerEmp = {
                         organizationId:orgId,
                         organizationName:orgName,
                         employeeId: newEmpId
                        }
                        callMutation(QUERY_UPDATE_EMP_ID(obj),(obj),(result)=>{
                          console.log("%%%%%%%%%%%%%%%%%%%%%")
                          console.log(result.data);
                        })
                       }*/
                          create(user,latitude,longitude,pageName,eventName,callback);
                      
                      }
                      else if(result.data.users[0]){
                        callback(true);
                      }
                   })
                }
                else {
                    create(user,latitude,longitude,pageName,eventName,callback)
                }
}


export const create = (user,latitude,longitude,pageName,eventName,callback) => {
       let name = user.displayName;
                       let email = user.email;
                       let photoUrl = user.photoURL;
                       let emailVerified = user.emailVerified;
                       let directoryId = user.uid;  
                       //directoryIds.push(user.uid);
                       let splittedName = name.split(' ');
                       let firstName = "";
                       let middleName = "";
                       let lastName = "";
                       if(splittedName.length <= 2)
                       {
                           firstName = splittedName[0];
                           lastName = splittedName[1] ? splittedName[1] : "";
                       }
                       if(splittedName.length === 3)
                       {
                            firstName = splittedName[0];
                            middleName = splittedName[1] ? splittedName[1] : "";
                            lastName = splittedName[2] ? splittedName[2] : "";
                       }
                       let emailArr = [];
                       let devices = [];
                       devices.push(Device);
                       emailArr.push(user.email);
                       let obj = {
                         directoryId : directoryId,
                         givenName : firstName,
                         familyName : lastName,
                         nickname : "",
                         address:{
                         number:"123",
                         street:"street1",
                         street2:"street2",
                         city:"Chicago",
                         countryInfo:{
                         country:"USA",
                         USA_state:"IL",
                         USA_zipCode:"US123"
                         },
                         geometry:{
                           type:"Point", 
                           coordinates: [latitude,longitude] 
                          }
                         },
                         emails:emailArr,
                         systemRole:"user",
                         devices: devices,
                         active:true
                       }
                       callMutation(QUERY_ADD_CONSUMER(obj),obj,pageName,"onCreateUser",(result)=>{
                         console.log("!!!!!!!!!!!!!!!!!!!!!11")
                         console.log(result.data);
                         callback(true);
                       })
}