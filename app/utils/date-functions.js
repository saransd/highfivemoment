import moment from 'moment';
import  momentTimeZone from 'moment-timezone';

export const GetMonthAndYear = (joinedDate) => {
    if(joinedDate){
     let date = joinedDate.split('T')[0];
    return {
        "year": date.split('-')[0],
        "month" : moment(date.split('-')[1], 'MM').format('MMMM')
    }
}
 else{
     return {
         "month":"May",
         "year":"2018"
     }
 }
}


export const GetDateTime = (hint) => {
   let date = moment().format("YYYY-MM-DDTHH:mm:ss");
   if(hint === "add"){
    date = moment().add(2,"years").format("YYYY-MM-DDTHH:mm:ss");
   }
   let datetime = momentTimeZone.tz(date, "America/Chicago").utc().format();
   return datetime;
}