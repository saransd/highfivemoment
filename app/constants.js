import DeviceInfo from 'react-native-device-info';
import Analytics from 'appcenter-analytics';

export const SEARCH_PLACEHOLDER = "Search Employee Name or Employer";
export const PRIVACY_POLICY = "https://www.thehighfivemovement.com/privacy-policy";
export const TERMS_OF_USE = "https://www.thehighfivemovement.com/privacy-policy";
export const FEEDBACK_LINK = "https://www.thehighfivemovement.com/app-feedback";
export const QUERY_API = "https://hfm-api-development.azurewebsites.net/graphql";
//export const QUERY_API = "https://hfm-api-staging.azurewebsites.net/graphql";
//export const QUERY_API = "https://hfm-api-dev.azurewebsites.net/graphql";
export const HIGHFIVE_TIME_LIMIT = 15000;
export const ORG_SEARCH_DISTANCE = 5;
export const PROFILE_PAGE_CLOSE_TIME = 15000;

export const CreateLog = (PageName, EventName, LogType, Exception, Comments) => {
   var exp = Exception.length < 125 ? Exception : Exception.substring(0,125);
   let obj = {
    PageName: PageName,
    EventName: EventName,
    LogType: LogType,
    Exception: exp,
    Comments: Comments ? Comments : "",
    apiInfo: QUERY_API
  }
  // https://docs.microsoft.com/en-us/appcenter/sdk/analytics/react-native
  Analytics.trackEvent('Logs',obj);
}

export const Device = {
  id : DeviceInfo.getDeviceId(),
  model : DeviceInfo.getModel()
}

export const QUERY_ORGANIZATIONS =  (location,latitude,longitude,searchValue) => {
  return `{
  organizations(location:${location ? `{lat:${latitude},lng:${longitude},maxDistance:${ORG_SEARCH_DISTANCE}}` : location},searchFor:"${searchValue+'.*'}"){
    organization{
     id
      name
      url
      imageUrl
    address{
      number
      street
      street2
      city
      countryInfo{
        __typename
      }
    }
      departments{
        id
       name
    }
    jobs{
      id
      name
    }
    locations{
      id
      name
    }
     attributeGroups
      {
        job{
          id
          name
        }
        attributes{
          id
          name
        }
      }
    highFiveScore
}
 distance
}
} `
}


export const QUERY_ORG_SEARCH = (regex) => {
 return  `{
  organizations(searchFor:"${regex}"){
    organization{
      id
      name
      url
      imageUrl
    address{
      number
      street
      street2
      city
      countryInfo{
        __typename
      }
    }
    departments{
       name
    }
    locations{
      id
      name
    }
     attributeGroups
      {
        job{
          id
          name
        }
        attributes{
          name
        }
      }
    highFiveScore
  } 
}
}`
}


export const QUERY_USERS = () => {
 return `{
    users{
       id
      givenName
      middleName
      familyName
      nickname
     employment{
       organization{
         id
         name
       }
       job{
         id
         name
       }
     }
     emails
    address{
         number
         street
         street2
         city
        countryInfo{
          __typename
        }
      }
  }
  }`
}


export const GET_USERID_NAME = () => {
 return `{
    users{
       id
      givenName
      middleName
      familyName
      nickname
      emails
  }
  }`
}

export const QUERY_USERS_ORGID = (orgId) => {
   return  `{
      users(employment:{value:"${orgId}",property:organizationId}){
     id
    directoryId
      givenName
      middleName
      familyName
      nickname
      employment{
        job{
          id
          name
        }
        organization
        {
          id
          name
        }
       }
     address{
         number
         street
         street2
         city
        countryInfo{
          __typename
        }
      }
   attributesScore{
       attribute{
         id
         name
       }
       score
    }
    highFiveScore
    joined
  }
    }`
}

export const QUERY_ORG_AND_EMPLOYEES = (regex,orgId) => {
 return  `{
  organizations(name:"${regex}"){
        organization{
          id
           highFiveScore
        }
   }
  }`
}

export const QUERY_ALL_EMPLOYEES = `{
  employees{
    employeeId
    employeeName{
      givenName
      familyName
    }
    url
    totalHighFive
    joinedDate
  }
  }`

export const QUERY_EMPLOYEES = (orgId) =>  {
  return  `{
  employees(organizationId:"${orgId}"){
    employeeId
    employeeName{
      givenName
      familyName
      additionalName
    }
    position
    url
     address{
         number
         street
         street2
         city
        countryInfo{
          __typename
        }
      }
    totalHighFive
    joinedDate
  }
} `
}

export const QUERY_EMP_BY_EMAIL = (email) => {
  return `{
  users(email:"${email}")
  {
    id
    directoryId
    givenName
    familyName
    nickname
    highFiveScore
    joined
  }
}`
}

export const QUERY_UPDATE_EMP_ID = (obj) => {
 return  `mutation updateEmployee($id:ID!,$employeeId:ID){
  updateEmployee(employee:{
    id:$id
    employeeId:$employeeId
  })
    {
      employeeId
    }
}`
}

export const QUERY_ATTRIBUTES = (orgId) => {
  return ` {organization(organizationId:"${orgId}"){ 
         attributeGroups{
      employeeAttributes{
         attributeId
        attributeName
      }
    }
  }
}`
}

export const QUERY_EVENTS = `{
  events
  {
    id,
    employeeInfo{
      employeeId
      employeeName{
        givenName
        }
      }
   }
  }`

  export const QUERY_ADD_EVENT = (obj) => {
   return `mutation addEvent($employeeId:ID!,$consumerId:ID!,$organizationId:ID!,$occuredOn:Date,$locationId:String,
  $attributesAwarded:[AttributeAwardInput],$feedback:String,$geometry:PointInput,$deviceId:String,$active:Boolean!)
    {
   addEvent(event:{
    employeeId:$employeeId
    consumerId:$consumerId
    organizationId:$organizationId
    occuredOn:$occuredOn
    locationId:$locationId
    attributesAwarded:$attributesAwarded
    feedback:$feedback
    geometry:$geometry
    deviceId: $deviceId
    active:$active
      })
   {
    id
    consumer{
        id
        givenName
        familyName
    }
    employee{
       id
       givenName
       familyName
      }
    occuredOn
    feedback
    }
    }`
  }

  export const QUERY_UPDATE_EVENT = (obj) => {
   return `mutation updateEvent($id:ID!,$employeeId:ID!,$consumerId:ID!,$organizationId:ID!,$occuredOn:Date,$locationId:String,
  $attributesAwarded:[AttributeAwardInput],$feedback:String,$geometry:PointInput,$deviceId:String,$active:Boolean!)
    {
   updateEvent(event:{
    id:$id
    employeeId:$employeeId
    consumerId:$consumerId
    organizationId:$organizationId
    occuredOn:$occuredOn
    locationId:$locationId
    attributesAwarded:$attributesAwarded
    feedback:$feedback
    geometry:$geometry
    deviceId: $deviceId
    active:$active
      })
   {
    id
    consumer{
        id
        givenName
        familyName
    }
    employee{
       id
       givenName
       familyName
      }
    occuredOn
    feedback
    }
    }`
  }

   export const QUERY_ADD_EVENT_ATTRIBS = (attributes) => {
   return `mutation addEventAttributes($eventId:ID!,$attributesRecognized:[EmployeeAttributeInput]!,$feedback:String)
    {
   addEventAttributes(eventId:$eventId,attributesRecognized:$attributesRecognized,feedback:$feedback)
   {
     id
     eventInfo
    {
     attributesRecognized{
       attributeId
       attributeName
     }
     }
     feedback
    }
    }`
  }


   export const QUERY_UPDATE_EVENT_ATTRIBS = (obj) => {
   return `mutation updateEvent($employeeId:ID!,$patronId:ID!,$givenName:String!,$familyName:String!,$eventDate:Date!,
   $latitude:Float!,$longitude:Float!,$totalHighFives:Int,$totalForAttribute:Int,$attributeId:ID!,$attributeName:String!)
    {
   addEvent(event:{
     employeeId:$employeeId
     patronInfo:{
       patronId:$patronId,
       patronName:{
       givenName:$givenName,
       familyName:$familyName
      }
    }
    eventInfo:{
       eventDate:$eventDate,
       eventGeo:{
       latitude:$latitude,
       longitude:$longitude
      }
    }
    totalHighFive:{
     totalHighFives:$totalHighFives,
     attributeBreakdown:[]
    }
       
       })
   {
    patronInfo{
      patronId
      patronName{
        givenName
        familyName
      }
    }
    eventInfo{
      eventDate
      eventGeo{
        latitude
        longitude
      }
    }
    totalHighFive{
      totalHighFives
      attributeBreakdown{
        totalForAttribute
        attribute{
          attributeId
          attributeName
        }
      }
    }

    }
    }`
  }

export const ORG_BY_ID = `{
  organization(organizationId:"UFCH123"){
    id
    organizationCoreData{
      organizationId
      organizationName
      organizationUrl
      organizationImageUrl
    }
    organizationMainAddress{
      number
      street
      street2
      city
      countryInfo{
        __typename
      }
    }
    attributeGroups{
      groupId
      employeeAttributes{
        attributeId
        attributeName
      }
    }
  }
}`

export const GET_HIGHFIVES = (empId) => {
   return `{users(id:"${empId}"){
     id
     highFiveScore
  }}`
}

export const QUERY_EMP_ATTRIBS = (empId) => {
  return `{
  users(id: "${empId}"){
    attributesScore{
       attribute{
         id
         name
       }
       score
    }
  }
  }`
}

export const QUERY_GET_EMP_INFO = (empId) => {
  return `{
  events(employeeId: "${empId}", onlyWithFeedback: true){
   id
   consumer{
     givenName
     middleName
     familyName
   }
   occuredOn
   feedback
   created
   modified
   } 
  }`
}

export const QUERY_GET_CONSUMER_EVENTS = (consumerId) => {
  return `{
  events(consumerId: "${consumerId}", onlyWithFeedback: true){
   id
   employee{
     givenName
     middleName
     familyName
     employment{
       organization{
         name
       }
     }
   }
   occuredOn
   feedback
   created
   modified
   } 
  }`
}

export const QUERY_GET_CONSUMER = `{
  consumers{
    id
    consumerCoreData{
      consumerId
      consumerName {
        familyName
        givenName
        additionalName
      }
    }
     consumerCurrentAddress{
      number
      street
      street2
      city
      countryInfo{
        country
        USA_state
        USA_zipCode
      }
    }
  joinedDate
  totalHighFive
  }
}`

export const QUERY_GET_CONSUMER_BY_ID = (consumerId) => {
  return `{
  users(email:"${consumerId}"){
       id
      directoryId
        familyName
        givenName
        nickname
     address{
      number
      street
      street2
      city
      countryInfo{
        country
        USA_state
        USA_zipCode
      }
    }
   employment{
      organization{
        id
        name
      }
      department{
        id
        name
      }
      location{
        id
        name
      }
      job{
        id
        name
      }
      role
    }
    joined
  devices{
    id
    model
  }
  attributesScore{
     attribute{
       id
       name
     }
     score
  }
  systemRole
  }
}`
}

export const  GET_CONSUMER_BY_EMAIL = (email) => {
return `{
       users(email : "${email}"){
        id
        givenName
        familyName 
        emails
        }
  }`
}

export const QUERY_CONSUMER_HIGHFIVES = (consumerId) => {
return `{
    users(id:"${consumerId}")
    {
      id
      highFivesAwarded
    }
  }`
}

export const QUERY_ADD_CONSUMER = (obj) => {
return `mutation addUser($directoryId:String!,$givenName:String!,$familyName:String!,$nickname:String!,$address:AddressInput,$emails:[Email!],$systemRole:SystemRole!,$devices:[DeviceInput]!,$active:Boolean!){
  addUser(user:{
    directoryId : $directoryId
    givenName: $givenName
    familyName: $familyName
    nickname:$nickname
    address:$address
    emails:$emails,
    systemRole :$systemRole,
    devices : $devices,
    active : $active
  })
  {
      directoryId
        givenName
        familyName
     address{
      number
      street
      street2
      city
      countryInfo{
        country
        USA_state
        USA_zipCode
      }
      geometry{
        type
        coordinates
      }
    }
    emails
  }
}`
}


export const QUERY_UPDATE_DEVICES = (obj) => {
  return `mutation updateUser($id:ID!,$devices:[DeviceInput]!){
        updateUser(user:{
           id:$id,
           devices:$devices
        }){
          id
          devices{
            id
            model
          }
          }
        }`
}


export const QUERY_ADD_EMPLOYMENT = (obj) => {
return `mutation addEmployment($employment:EmploymentInput!)
{
   addEmployment(employment:$employment)
  {
      familyName
      givenName
  }
}`

} 


