import React, { Component } from "react";
import { connect } from "react-redux";
import {View,AsyncStorage} from 'react-native';
import { addNavigationHelpers } from "react-navigation";
import {getRoutes} from '../../Routes';

class AppNavigation extends Component {
  
  constructor(props)
  {
    super(props)
    this.state ={
      userToken : "",
      root:<View></View>
    }
  }

   componentDidMount()
  {
      AsyncStorage.getItem('user-token').then((userToken)=>{
         AsyncStorage.getItem('new-cardlist').then((cardlist)=>{
          AsyncStorage.getItem('images').then((images)=>{
         //this.setState({userToken:userToken});
         this.setState({root:getRoutes(userToken,cardlist,images,false,this.props)});
        });
        });
      });
  }


  render() {
    const { navigationState, dispatch } = this.props;
    return this.state.root;
  }
}

const mapStateToProps = state => {
  return {
    navigationState: state.navigationReducer
  };
};

export default connect(mapStateToProps)(AppNavigation);