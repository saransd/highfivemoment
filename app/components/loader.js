import React from 'react';
import {View,StyleSheet} from 'react-native';
import {Spinner} from 'native-base';

export const Loader = (props) => {
    return (<View style={[styles.container, styles.horizontal]}>
             <Spinner color="#00446A"/>
             </View>)
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center',
    justifyContent: 'center',
    position: 'absolute',
    backgroundColor:'rgba(196, 196, 196, 0.75)',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex:1
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})