import React from 'react';
import {Image} from 'react-native';

export const GetLogo = (props) => {
    return <Image style={props.styles} source={require('../assets/images/thf_logo.png')}/>
}