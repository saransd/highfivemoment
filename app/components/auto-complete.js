import React from 'react';
import {TouchableOpacity,Text,StyleSheet,View} from 'react-native';
import Autocomplete from 'react-native-autocomplete-input'
import {getName} from '../utils/getName';

 const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();

export default class AutoCompleteComponent extends React.Component{
    constructor(props){
        super(props)
         this.state = {
             query:"",
             dataToSend:"",
             dataToFilter:[],
             dataType:"",
             placeholder:"",
         }
         this.initialState = this.state;
    }

    componentDidMount()
    {
        this.setState({dataToFilter:this.props.dataToFilter,
                       dataType:this.props.dataType,
                       placeholder:this.props.placeholder
                    })
    }

    handleQuery = (value) => {
          this.setState({query:value})
    }

    setQueryAndData = (item) => {
    
    let stateObj = this.state.dataType === "org" ? {query:item.organization.name,dataToSend:item.organization} :
    {query: getName({givenName:item.givenName,middleName:item.middleName,familyName:item.familyName}),dataToSend:item}
    this.setState(stateObj);

    this.state.dataType === "org" ?  this.props.setOrganization(item.organization) : this.props.setUser(item);
          
    }

    _filterData = (query) => {
            if (query === '') {
                return [];
            }
    const { dataToFilter,dataType } = this.state;
    const regex = new RegExp(`${query.trim()}`, 'i');
    return dataToFilter.filter(value => 
    {
        let nameToSearch = dataType === "org" ?  value.organization.name.toLowerCase() : 
        getName({givenName:value.givenName,middleName:value.middleName,familyName:value.familyName});
        return nameToSearch.search(regex) >= 0
    });
   }

    render()
    {

       const { query,dataType,placeholder } = this.state;
       const data = this._filterData(query)
       const dataWithCondition = dataType === "org" ?
        data.length === 1 && comp(query, data[0].organization.name) ? [] : data :
        data.length === 1 && comp(query,getName({givenName:data[0].givenName,middleName:data[0].middleName,familyName:data[0].familyName})) ? [] : data ;

       return (
            <Autocomplete
                            id={"auto-"+dataType}
                            data={dataWithCondition}
                            placeholder={this.state.placeholder}
                            defaultValue={query}
                            onChangeText={(text) => {this.handleQuery(text)}}
                            onBlur={()=>{this.props.checkForm(this)}}
                            renderItem={item => {
                            let displayValue = dataType === "org" ? item.organization.name : 
                            getName({givenName:item.givenName,middleName:item.middleName,familyName:item.familyName});
                            let returnValue = this.state.query !== displayValue ? 
                             (<TouchableOpacity onPress={() => this.setQueryAndData(item)}>
                                <Text>{displayValue}</Text>
                             </TouchableOpacity> ) : null;
                             return returnValue;  
                         }}
                         /> )

    }
}

