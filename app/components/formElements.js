import React, {Component} from "react";
import {Item,Input,Button,Text,Footer,FooterTab} from 'native-base';
import {View,Alert,Platform, Dimensions, PixelRatio} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {globalstyles} from '../assets/css/styles';
import stylesSignup from '../assets/css/signup';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');
var fsizetitle_sub;
var reduce=0;
var fsizePass;
var subtext2;

if(DeviceInfo.isTablet()){
  if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
    fsizetitle_sub = 22
    fsizePass  =26
    subtext2 = 16
    reduce=2
  }
  else{
    fsizetitle_sub = 18
    fsizePass  =22
    subtext2 = 14
  }
}
else{
    subtext2  = 13
    fsizePass = 20
    fsizetitle_sub = 16
    if(PixelRatio.get()>=0&&PixelRatio.get()<1){
        fsizetitle_sub = 13
        fsizePass = 14
        subtext2  = 11
    }
    else if(PixelRatio.get()>=1&&PixelRatio.get()<1.5){
        fsizetitle_sub = 13
        reduce=1
        fsizePass = 15
        subtext2  = 11
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
        fsizetitle_sub = 14
        fsizePass = 17
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
        fsizetitle_sub = 15
        fsizePass = 18
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
        fsizetitle_sub = 16
        fsizePass = 19
    }
}

export const TextBox = (props) => {
 return (<Item style={props.styles}><Input placeholder={props.placeholder} style={[globalstyles.textBox,props.page === "login" ? null : globalstyles.formElementMargin]} value={props.value} onChangeText={props.changeText} secureTextEntry={props.secure} keyboardType={props.keyboardType} autoCapitalize='none'/></Item>);
}

export const TextBoxForgotPass = (props) => {
  return (
    <Item style={props.styles}>
      <Input 
          placeholder={props.placeholder} 
          style={[stylesSignup.textBox,{marginTop:deviceHeight/40}]} 
          value={props.value} 
          onChangeText={props.changeText} 
          secureTextEntry={props.secure} 
          keyboardType={props.keyboardType} 
          autoCapitalize='none'/>
    </Item>
  );
}

export const SubmitButton = (props) => {
  return (<Button style={[globalstyles.button,props.page === "login" ? null : globalstyles.formElementMargin,props.styles]} onPress={props.action}>
        <Text uppercase={false} style={[Platform.OS === 'ios' ? globalstyles.buttonTextIos : globalstyles.buttonText]}>{props.text}</Text>
    </Button>);
} 

export const SocialButtonLogin = (props) => {
    return (
         <Button rounded danger={props.icon === "google" ? true : false} style={[globalstyles.socialBtnLogin,props.icon == "facebook-f" ? {backgroundColor: "#3a559f"}:{marginLeft:5},{width:deviceWidth/2.5,justifyContent:'center'},reduce==1 ? {height:35}:null,reduce==2 ? {height:70}:null]} onPress={props.action}>
             <Icon name={props.icon} style={[globalstyles.socialBtnIcon,props.icon === "facebook-f" ? {marginTop:15} : null]} size={40}/>
             <Text uppercase={false} style={[{fontSize:fsizetitle_sub},reduce==2 ? {paddingTop:2}:null]}>{props.text}</Text>
          </Button>
    )
}

export const SocialButtonSignup = (props) => {
    return (
            <Button rounded block danger={props.icon === "google" ? true : false} style={[props.styles,props.icon == "facebook-f" ? {backgroundColor: "#3a559f"}:null]}>
            <Icon name={props.icon} size={30} style={globalstyles.socialBtnSignup}/> 
            <Text uppercase={false}>{props.text}</Text>
            </Button>
    )
}

export const PwdTextBlock = (props) => {
    return (
        <View style={[{alignItems:'center',justifyContent:'center'},globalstyles.formElementMargin]}>
         <Text style={[globalstyles.text,{fontSize:fsizePass}]}>{props.text}</Text>
         <Text style={[globalstyles.text,{fontSize:subtext2}]}>Please enter a new password.</Text>
         {/* <Text style={[globalstyles.text,globalstyles.subtext]}>password reset.</Text> */}
        </View>
    )
}
export const PwdTextBlockForgot = (props) => {
    return (
        <View style={[{alignItems:'center',justifyContent:'center'},globalstyles.formElementMargin]}>
         <Text style={[globalstyles.text,{fontSize:fsizePass}]}>{props.text}</Text>
         <Text style={[globalstyles.text,{fontSize:subtext2}]}>Please enter your email.</Text>
         {/* <Text style={[globalstyles.text,globalstyles.subtext]}>password reset.</Text> */}
        </View>
    )
}

export const FormFooter = (props) => {
    return (
         <Footer style={props.styles ? props.styles.footer : null}>
          <FooterTab style={[globalstyles.footerTab,globalstyles.footerColor]}>
            <Button full onPress={()=>{props.navigation.navigate(props.route)}}>
              <Text uppercase={false} style={[globalstyles.footerTxt,props.textStyle,props.styles ? props.styles.text : null]}>{props.text}</Text>
            </Button>
          </FooterTab>
        </Footer> 
    )
}

export const ErrorText = (props) =>{
    return (<Text style={{color:'red',alignSelf:'center',textAlign:'center'}}>{props.error}</Text>)
}

export const SocialLoginAlert = () => {
    return (Alert.alert(
        '',
        'Coming Soon...',
        [{text: 'Ok', onPress: () => console.log('Ask me later pressed')}],
        { cancelable: false }
      ))
}







