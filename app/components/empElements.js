import React from 'react';
import {Grid,Row,Col,Text,ListItem,Body,Left,Right,Button} from 'native-base';
import {View,Image,Platform,ImageBackground,PixelRatio,Dimensions,StyleSheet,Animated,TouchableWithoutFeedback,TouchableOpacity} from 'react-native';
import {globalstyles} from '../assets/css/styles';
import {getName,getJobTitle} from '../utils/getName';
import RNFetchBlob from 'react-native-fetch-blob';
import ImagePicker from 'react-native-image-crop-picker';
var Window = Dimensions.get('window');
let {height,width} = Window;
import moment from 'moment';
import _ from 'lodash';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');

const eventName = "onGetCount";
var widthIOS = 165;
var heightIOS = 155;
var profilepicHeight;//100
var widthAndroid = (deviceWidth/2)-(deviceWidth/30);//161
var heightAndroid;//150
var imageSize1;//50
var imageSize2;//75
var fsize;//20
var fsiz_sub;//16
var fsize_empPosi;//13
var custImage;
var heightBackWall;
var textWidth;
var logoHeight;//105
var margRight;

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      heightAndroid = deviceHeight/6
      imageSize1 = deviceWidth/10
      imageSize2 = 85
      fsize = 22
      fsiz_sub = 18
      custImage = 70
      fsize_empPosi = 16
      textWidth = deviceWidth-(deviceWidth/4.3)
      profilepicHeight = deviceHeight/8
      heightBackWall = deviceHeight/14   //11.5
      logoHeight = deviceHeight/10.5  //130
    }
    else{
      heightAndroid = deviceHeight/6
      imageSize1 = deviceWidth/10
      imageSize2 = 75
      fsize = 20
      fsiz_sub = 16
      custImage = 60
      fsize_empPosi = 13
      textWidth = deviceWidth-(deviceWidth/4.3)
      profilepicHeight = deviceHeight/8
      heightBackWall = deviceHeight/14  //11.5
      logoHeight = deviceHeight/10.5    //105
    }
  }
  else{
    imageSize1 = 50
    imageSize2 = 70
    fsize = 19
    fsiz_sub = 15
    custImage = 55
    fsize_empPosi = 13
    profilepicHeight = deviceHeight/8
    heightAndroid = deviceHeight/4
    heightBackWall = deviceHeight/8.5
    textWidth = deviceWidth-(deviceWidth/2.9)
    logoHeight = deviceHeight/7 //105
    margRight = deviceWidth/45
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      heightAndroid = deviceHeight/4
      imageSize1 = 30
      imageSize2 = 50
      fsize = 14
      fsiz_sub = 12
      fsize_empPosi = 12
      custImage = 45
      heightBackWall = deviceHeight/11  //8
      textWidth = deviceWidth-(deviceWidth/2.5)
      logoHeight = deviceHeight/8 //85
      margRight = deviceWidth/32
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      heightAndroid = deviceHeight/4
      imageSize1 = 35
      imageSize2 = 50
      fsize = 16
      fsiz_sub = 13
      custImage = 50
      heightBackWall = deviceHeight/10  //7
      textWidth = deviceWidth-(deviceWidth/2.5)
      logoHeight = deviceHeight/7.5 //95
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      imageSize1 = 40
      imageSize2 = 55
      fsize = 17
      fsiz_sub = 14
      custImage = 55
      heightBackWall = deviceHeight/9.5   //8
      textWidth = deviceWidth-(deviceWidth/2.7)
      logoHeight = deviceHeight/7.5 //100

    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      heightBackWall = deviceHeight/8.5   //8
      logoHeight = deviceHeight/7
      imageSize1 = 45
      imageSize2 = 60
      fsize = 18
      custImage = 55
    }
    else if(PixelRatio.get()>=3&&PixelRatio.get()<3.5){
    }
  }

  var logoHeight2;//105
  var yellowHeight;

  if(DeviceInfo.isTablet()){
    logoHeight2 = 110
    yellowHeight=90
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      logoHeight2 = 120
      yellowHeight=90
    }
    else{
      logoHeight2 = 115
      yellowHeight=90
    }
  }
  else{
    logoHeight2 = 105
    yellowHeight=80
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      logoHeight2 = 70
      yellowHeight=45
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      logoHeight2 = 80
      yellowHeight=55
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      logoHeight2 = 85
      yellowHeight=60
    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      logoHeight2 = 90
      yellowHeight=65
    }
    else if(PixelRatio.get()>=3&&PixelRatio.get()<4){
      logoHeight2 = 95
      yellowHeight=70
    }
  }

//const polyfill = RNFetchBlob.polyfill

//window.XMLHttpRequest = polyfill.XMLHttpRequest;
//window.Blob = polyfill.Blob;

const fs = RNFetchBlob.fs;

export const HandBox = (props) => {
  return <Col style={[Platform.OS === 'ios' ? {width:165,height:155} : {width:161,height:150},{alignItems:'center',justifyContent:'center'},props.bgColor ? props.bgColor : styles.handBgColor]}>
           <Image style={{width:75,height:75,resizeMode:'contain'}} source={require('../assets/images/hand_nobox.png')}/>
           <Text style={[globalstyles.text,{fontSize:20}]}>{props.highfives ? props.highfives : "0"} {props.text ? props.text : "Received"}</Text>
         </Col>
}

export const TrophyBox = (props) => {
    return <Col style={[Platform.OS === 'ios' ? {width:165,height:155} : {width:161,height:150},{alignItems:'center',justifyContent:'center',marginLeft:10},props.bgColor ? props.bgColor : styles.trophyBgColor]}>
              <Image style={{width:75,height:75,marginTop:20}} source={require('../assets/images/trophy.png')}/>
              <Text style={[globalstyles.text,{fontSize:20}]}>Ranked 2</Text>
              <Text style={[globalstyles.text,globalstyles.subtext]}>Nurse</Text>
            </Col>
}  

export const TrophyBoxCalendar = (props) => {
  let attributes = props.attributes ? props.attributes : []; 
    let column = props.workProfile && attributes.length > 0 ? <Col style={[Platform.OS === 'ios' ? styles.boxIOS : styles.boxAndroid,{alignItems:'center',justifyContent:'center',width:widthAndroid,marginLeft:deviceWidth/50,paddingTop:5},props.bgColor ? props.bgColor : styles.calendarBgColor]}>
                {attributes.map((attrib,index)=>{
                 return <Row key={"count-attrib"+index}>
                          <Col style={{marginLeft:deviceWidth/50}}>
                            <Text style={[globalstyles.text,{fontSize:fsize}]}>{attrib.score} {attrib.attribute.name}</Text>
                          </Col>
                        </Row>
                })}
            </Col> : 
            <Col style={[Platform.OS === 'ios' ? styles.boxIOS : styles.boxAndroid,{alignItems:'center',justifyContent:'center',width:widthAndroid,marginLeft:deviceWidth/50,paddingTop:5},props.bgColor ? props.bgColor : styles.calendarBgColor]}>
                  <Image style={{width:imageSize2,height:imageSize2,marginTop:5}} source={require('../assets/images/calendar.png')}/>
                <Text style={[globalstyles.text,globalstyles.subtext,{paddingTop:5}]}>Member since</Text>
                <Text style={[globalstyles.text,{fontSize:fsize}]}>{props.month ? props.month : "May"} {props.year ? props.year : "2018"}</Text>
            </Col>
    return column;
}         
       


export const GiftBox = (props) => {
    return  <Col style={[Platform.OS === 'ios' ? {width:165,height:155} : {width:161,height:150},{alignItems:'center',justifyContent:'center',backgroundColor:'#FFF5D1',marginLeft:10}]}>
                <Image style={{width:75,height:75}} source={require('../assets/images/present.png')}/>
                <Text style={[globalstyles.text,{fontSize:20}]}>0 Redeemed</Text>
            </Col>
}

export const CalendarBox = (props) => {
  let bgColor = props.bgColor ? props.bgColor : styles.calendarBgColor;
    return <Row style={props.rowStyle}>
                      <Col style={[styles.calendarColumn,bgColor]}>
                        <View style={{marginLeft:30,alignItems:'center'}}>
                         <Text style={[globalstyles.text,globalstyles.subtext]}>Member since</Text>
                         <Text style={[globalstyles.text,{fontSize:28}]}>{props.month ? props.month : "May"} {props.year ? props.year : "2018"}</Text>
                         </View>
                      </Col>
                      <Col style={[styles.calendarColumn,bgColor]}>
                        <Image style={{width:75,height:75}} source={require('../assets/images/calendar.png')}/>
                      </Col>
                 </Row>
}

export class ProfileBox extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      img : ""
    }
  }

  render()
  {
  let props = this.props;
  let empInfo = props.empInfo ? props.empInfo : null;
  let consumerInfo = props.consumerInfo ? props.consumerInfo : null;
  let personName = "";
  let imageUrl = require('../assets/images/user2.png');
  let position = "";
  let systemRole="";
  if(empInfo && Object.keys(empInfo).length > 0)
  {
    personName = getName({givenName:empInfo.givenName,familyName : empInfo.familyName});
     if(empInfo.url)
     {
     imageUrl = {uri:empInfo.url};
    }
    systemRole = empInfo.systemRole;
  }
  else if(consumerInfo && Object.keys(consumerInfo).length > 0){
    let consumerName = {givenName :consumerInfo.givenName,
                        familyName : consumerInfo.familyName}
    personName = getName(consumerName);

  }

  openPicker = () => {
    ImagePicker.openPicker({
     width: 300,
     height: 400,
    cropping: true
   }).then(image => {
     let path = image.path;
 
         /*Blob.build(RNFetchBlob.wrap(path), { type : 'image/jpg' })
        .then((blob) => {
              console.log(blob);
        });*/

     fs.readFile(path, 'base64')
      .then((data) => {
           image = "data:image/png;base64,"+data;
           this.setState({img:{uri:image}});
           console.log(image)
       })
    });
  }

    return <Row>
      <Col style={{marginLeft:deviceWidth/30,width:(deviceHeight/8)+(deviceHeight/40)}}>
      <TouchableOpacity onPress={()=>{openPicker()}}>
        <Image style={{width:profilepicHeight,height:profilepicHeight,borderRadius:50}} source={this.state.img ? this.state.img : imageUrl}  />
      </TouchableOpacity>
       </Col>
       <Col style={{paddingTop:deviceHeight/50}}>
          <TouchableOpacity onPress={props.action}>
             <Text style={[globalstyles.text,{fontSize:fsize}]}>{personName ? personName : "Beta Test User"}</Text>
          </TouchableOpacity>
         {props.textArr.map((text) => {
          return <Text key={text} style={[globalstyles.text,globalstyles.subtext]}>{text}</Text>
         })}
         {/*props.setting ? <Image style={props.settingStyle} source={require('../assets/images/settings2.png')} /> : null*/}
        
        </Col>
         {this.props.addEmp ? systemRole === "administrator" ? 
            <Col style={{paddingTop:deviceHeight/50}}>
                <Button style={styles.addEmpBtn} onPress={()=>{this.props.navigation.navigate('AddEmployment',{organizations:this.props.organizations,users:this.props.users,pageName:this.props.pageName,employment:this.props.employment})}}>
                  <Text uppercase={false} style={[globalstyles.text,styles.addEmpBtnFont]}>Add Employment</Text>
                </Button>
            </Col> :
              <Col style={{paddingTop:deviceHeight/50}}>
              <Button style={styles.addEmpBtn} onPress={()=>{this.props.navigation.navigate('AddEmployment',{organizations:this.props.organizations,users:this.props.users})}}>
                <Text uppercase={false} style={[globalstyles.text,styles.addEmpBtnFont]}>Add Employment</Text>
                </Button>
              </Col> : null}
     </Row>
}
}

export const GiveHighFive = (props) => {
  let empInfo = props.empInfo;

  if(DeviceInfo.isTablet()){
    return <ImageBackground style={styles.imageBackHiFi} imageStyle={{resizeMode:'stretch'}} source={require('../assets/images/yellowbg.png')}>

            <View style={{flex:1,flexDirection:'row'}}>
              <View style={{flex:0.75,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
                <Text style={[globalstyles.text,styles.giveHighFiveText,{textAlign:'center'}]}>{`Give ${empInfo.givenName.split(' ')[0]} a High Five`}</Text>
                <Text style={[globalstyles.text,styles.orgText,{textAlign:'center'}]}>{props.text}</Text>
              </View>
              <View style={{flex:0.25,flexDirection:'column',alignItems:'flex-end',marginRight:-((deviceWidth/40)*3)}}>
                <TouchableWithoutFeedback onPress={props.action}>
                  <Animated.Image 
                    style={{width:logoHeight2,height:logoHeight2,bottom:15,alignSelf:'flex-end',transform: [{scale: props.springValue}]}}
                    source={require('../assets/images/pre_high.png')}/>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </ImageBackground>
        }
        else{

          return <ImageBackground style={styles.imageBackHiFi} imageStyle={{resizeMode:'stretch'}} source={require('../assets/images/yellowbg.png')}>

                  <View style={{flex:1,flexDirection:'row'}}>
                    <View style={{flex:0.75,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
                      <Text style={[globalstyles.text,styles.giveHighFiveText,{textAlign:'center'}]}>{`Give ${empInfo.givenName.split(' ')[0]} a High Five`}</Text>
                      <Text style={[globalstyles.text,styles.orgText,{textAlign:'center'}]}>{props.text}</Text>
                    </View>
                    <View style={{flex:0.25,flexDirection:'column',alignItems:'flex-end',marginRight:-((deviceWidth/40)*3)}}>
                      <TouchableWithoutFeedback onPress={props.action}>
                        <Animated.Image 
                          style={{width:logoHeight2,height:logoHeight2,bottom:15,alignSelf:'flex-end',transform: [{scale: props.springValue}]}}
                          source={require('../assets/images/pre_high.png')}/>
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </ImageBackground>
        }          
      }

export const EmpListItem = (props) => {
  let empImage = props.emp.url ? {uri:props.emp.url} : require('../assets/images/user2.png');
  let empPosition = getJobTitle(props.emp.employment,props.org)
  let empName = {givenName : props.emp.givenName, familyName: props.emp.familyName};
  return <ListItem key={"emp-"+props.emp.name} style={Platform.OS === 'ios' ? {height:90} : null}>
  <TouchableOpacity style={{flex:1,flexDirection:'row',borderColor:"lightgrey",borderWidth:1,paddingTop:5,paddingBottom:5,paddingLeft:3,marginRight:deviceWidth/20}} onPress={props.action1}>
    <Image style={{height:custImage,width:custImage,resizeMode:'contain',borderRadius:Platform.OS === 'ios'? 30 : 35}} source={empImage} />
    <Body style={{paddingLeft:5}}>
    <Text style={[styles.text,{fontSize:fsize}]}>{getName(empName)}</Text>
   <Text style={{fontSize:fsize_empPosi}}>{empPosition}</Text>
 </Body>
 </TouchableOpacity>
 <Right>
 <TouchableOpacity onPress={props.action2}>
    <Image style={[styles.imageWork,{marginRight:deviceWidth/40}]} source={require('../assets/images/pre_high.png')}  />
 </TouchableOpacity>
 </Right>
</ListItem>
}


export const FeedbackTextView = (props) => {
  let events = props.fbType === "emp" ? _.orderBy(props.events, ['modified' || 'created' || 'occuredOn'], ['desc']) :
  events = props.events.slice(Math.max(props.events.length - props.count ));
  events = events.length > 0 ?  _.orderBy(events, ['modified' || 'created' || 'occuredOn'], ['desc']) : []
  let eventDate = "";
  let name = "";
  let organization = "";

   let feedback = [];
   if(events.length > 0)
       {
         events.map((event,index)=>{
           eventDate = event.modified ? event.modified : event.created ? event.created : event.occuredOn;
           eventDate = moment(eventDate).format("MM/DD/YY");
           if(props.fbType === "emp")
           {
              name = event.consumer && event.consumer.givenName ?  getName({givenName:event.consumer.givenName,familyName:event.consumer.familyName}) : "Anonymous";
           }
           else if(props.fbType === "cust")
           {
             name = event.employee && event.employee.givenName ?  getName({givenName:event.employee.givenName,familyName:event.employee.familyName}) : "Anonymous";
             organization = event.employee && event.employee.employment && event.employee.employment.organization ? 
             event.employee.employment.organization.name : "";
           }
           if(event.feedback)
           {
            feedback.push(<Text key={"feedback-"+index} style={[globalstyles.text,{fontSize:fsize,paddingTop:5,paddingLeft:5}]}>{`"${event.feedback.trim()}" - ${name}${props.fbType === "cust" ? " ("+organization+")" : ""} ${eventDate}`}</Text>);
          if(events.length-1 !== index && props.fbType === "emp" || props.fbType === "cust")
            {
             feedback.push( Platform.OS === 'ios' ? 
             <View key={"hr-"+index} style={{borderBottomColor: '#4f4f4f',borderBottomWidth: 1,marginTop:5}}/> :
             <Text key={"hr-"+index} style={{borderBottomColor: '#4f4f4f',borderBottomWidth: 1}}/>
            );
           }
           }
         });
       }

       return  <Col style={[{minHeight:150,backgroundColor:'#FFF5D1',padding:10},feedback.length === 0 ? {alignItems:'center',justifyContent:"center"} : null]}>
                    {feedback.length > 0 ? feedback : <Text style={[globalstyles.text,{fontSize:20,textAlign:"center"}]}>Additional Details</Text>}
                        {/*<Text style={[globalstyles.text,{fontSize:22}]}>Details</Text>*/}
                </Col>
}


const styles = StyleSheet.create({
   handBgColor:{
     backgroundColor:'#DBFDFF'
   },
   text:{
    fontFamily: 'Roboto',
     fontWeight:'300'
  },
   trophyBgColor:{
     backgroundColor:'#FFF5D1'
   },
   calendarBgColor : {
     backgroundColor:'#FFE5E8'
   },
   calendarColumn:{
      height:100,
      alignItems:'center',
      justifyContent:'center'
   },
   giveHighFiveText : {
     fontSize: fsize, //height === 480 && width === 320 ? 18 : 20
   },
   orgText:{
     fontSize: fsiz_sub, //height === 480 && width === 320 ? 14 : 16
   },
   boxAndroid: {
    height:heightAndroid
  },
  boxIOS: {
    width:widthIOS,
    height:heightIOS
  },
  imageWork: {
    width:imageSize1,
    height:imageSize1,
    resizeMode:'contain'
  },
  imagePersonal:{
    width:imageSize2,
    height:imageSize2,
    resizeMode:'contain'
  },
  // imageBackHiFi:{
  //   flex:1,
  //   // width:deviceWidth-((deviceWidth/40)*2),
  //   height:heightBackWall,  //80
  //   marginTop:deviceHeight/50,
  //   marginBottom: deviceHeight/100,
  //   // marginRight:15
  // },

  imageBackHiFi:{
    flex:1,
    width:deviceWidth-((deviceWidth/40)*5),
    height:yellowHeight, //heightBackWall,  //80
    marginTop:deviceHeight/50,
    marginBottom: deviceHeight/100,
  },
  addEmpBtn:{
   color:"#FFF",
   backgroundColor:"#334149",
   marginLeft:2
  },
  addEmpBtnFont:{
   fontSize:11
  }
})








                      //  <Grid>
                      //    <Row>
                      //     <Col style={{width:textWidth,alignItems:'center',justifyContent:'center'}}>
                      //       <Text style={[globalstyles.text,styles.giveHighFiveText]}>{`Give ${empInfo.givenName.split(' ')[0]} a High Five`}</Text>
                      //       <Text style={[globalstyles.text,styles.orgText]}>{props.text}</Text>
                      //     </Col>
                      //    <Col>
                      //      <TouchableWithoutFeedback onPress={props.action}>
                      //      <Animated.Image 
                      //        style={{width:logoHeight,height:logoHeight,bottom:15,left:45,transform: [{scale: props.springValue}]}}
                      //        source={require('../assets/images/pre_high.png')}/>
                      //        </TouchableWithoutFeedback>
                      //    </Col>
                      //   </Row>
                      // </Grid>



              //  <View style={{flexDirection:'row'}}>
              //   <View style={{flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
              //     <Text style={[globalstyles.text,styles.giveHighFiveText]}>{`Give ${empInfo.givenName.split(' ')[0]} a High Five`}</Text>
              //     <Text style={[globalstyles.text,styles.orgText]}>{props.text}</Text>
              //   </View>
              //   <View style={{flexDirection:'column'}}>
              //     <TouchableWithoutFeedback onPress={props.action}>
              //       <Animated.Image 
              //         style={[{width:logoHeight,height:logoHeight,bottom:15,left:(deviceWidth/40)*4,transform: [{scale: props.springValue}]},margRight>0?{marginRight:margRight}:'']}
              //         source={require('../assets/images/pre_high.png')}/>
              //     </TouchableWithoutFeedback>
              //   </View>
              // </View>




  //               if(DeviceInfo.isTablet()){
  //   return  <ImageBackground style={styles.imageBackHiFi} imageStyle={{resizeMode:'stretch'}} source={require('../assets/images/yellowbg.png')}>

  //             <View style={{flex:1,flexDirection:'row'}}>
  //               <View style={{flex:0.75,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
  //                 <Text style={[globalstyles.text,styles.giveHighFiveText]}>{`Give ${empInfo.givenName.split(' ')[0]} a High Five`}</Text>
  //                 <Text style={[globalstyles.text,styles.orgText]}>{props.text}</Text>
  //               </View>
  //               <View style={{flex:0.25,flexDirection:'column'}}>
  //                 <TouchableWithoutFeedback onPress={props.action}>
  //                   <Animated.Image 
  //                     style={{width:logoHeight,height:logoHeight,bottom:15,alignSelf:'flex-end',transform: [{scale: props.springValue}]}}
  //                     source={require('../assets/images/pre_high.png')}/>
  //                 </TouchableWithoutFeedback>
  //               </View>
  //             </View>
  //           </ImageBackground>
  // }
  // else{
  //   return  <ImageBackground style={styles.imageBackHiFi} imageStyle={{resizeMode:'stretch'}} source={require('../assets/images/yellowbg.png')}>

  //             <View style={{flex:1,flexDirection:'row'}}>
  //               <View style={{flex:0.7,flexDirection:'column',alignItems:'center',justifyContent:'center'}}>
  //                 <Text style={[globalstyles.text,styles.giveHighFiveText]}>{`Give ${empInfo.givenName.split(' ')[0]} a High Five`}</Text>
  //                 <Text style={[globalstyles.text,styles.orgText]}>{props.text}</Text>
  //               </View>
  //               <View style={{flex:0.3,flexDirection:'column',alignItems:'flex-end'}}>
  //                 <TouchableWithoutFeedback onPress={props.action}>
  //                   <Animated.Image 
  //                     style={{width:logoHeight,height:logoHeight,bottom:15,alignSelf:'flex-end',transform: [{scale: props.springValue}]}}
  //                     source={require('../assets/images/pre_high.png')}/>
  //                 </TouchableWithoutFeedback>
  //               </View>
  //             </View>
  //           </ImageBackground>
  // }