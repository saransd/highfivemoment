import React from 'react';
import {Text, Image, TouchableOpacity, PixelRatio} from 'react-native';
import {Button,Icon,Item,Input} from 'native-base';
import Fonticon from 'react-native-vector-icons/FontAwesome';
import {globalstyles} from '../assets/css/styles';
import styles from '../assets/css/search';

var DeviceInfo = require('react-native-device-info');

if(DeviceInfo.isTablet()){
  if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
    iconSize = 40
  }
  else{
    iconSize = 30
  }
  }
  else{
    iconSize = 30
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      iconSize = 20
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      iconSize = 25
    }
  }

export const TabIcon = (props) => {
    return (<Button vertical onPress={props.action}>
              <Image style={[globalstyles.tabImage, {tintColor: props.active ? '#55D7E0' : '#FFF'}]} source={props.imgSrc}/>
              <Text style={globalstyles.tabIconTxt}>{props.text}</Text>
            </Button>)
}

export const SearchBox = (props) => {
  return (
      <Item style={[globalstyles.searchBox,props.styles]}>
            <Icon name={props.icon} style={{paddingLeft:5}} onPress={props.iconAction ? props.iconAction : null}/>
            <Input placeholder={props.placeholder} value={props.value} onChangeText={(value)=>{props.action ? props.action(value): null}} onBlur={()=>{props.callApi ? props.callApi() : {}}} clearButtonMode="while-editing"/>
       </Item>
  )
}

export const SearchBoxHome = (props) => {
  return (
      <Item style={[styles.searchBox,props.styles]}>
            <Icon name={props.icon} style={{paddingLeft:5}} onPress={props.iconAction ? props.iconAction : null}/>
            <Input placeholder={props.placeholder} value={props.value} onChangeText={(value)=>{props.action ? props.action(value): null}} onBlur={()=>{props.callApi ? props.callApi() : {}}} clearButtonMode="while-editing"/>
       </Item>
  )
}

export const BackArrow = (props) => {
  return <TouchableOpacity onPress={props.action}>
             <Fonticon name="arrow-left" style={{paddingLeft:10}} size={iconSize} color="#00446A"/>
         </TouchableOpacity>
}