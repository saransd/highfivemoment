  import React from 'react';
  import {AsyncStorage,StyleSheet,View, Dimensions, Platform, PixelRatio} from 'react-native';
  import {List,ListItem,Text,Switch,Content} from 'native-base';
  import { Col, Row, Grid } from 'react-native-easy-grid';
  import ProfilePicker from './picker';
  //import { LoginManager } from 'react-native-fbsdk';
  import { GoogleSignin } from 'react-native-google-signin';
  import {FEEDBACK_LINK} from '../constants';
  import Analytics from 'appcenter-analytics';
  import {client,persistor} from '../utils/query-handler';
  let flag = [],color=[];
  const deviceHeight = Dimensions.get('window').height;
  const deviceWidth = Dimensions.get('window').width;
  var DeviceInfo = require('react-native-device-info');
  import styles from '../assets/css/settings';

  const pickerOptions = ["Work Profile","Personal Profile"];

const getSettingSection = () => {
  return  [{name:"Provide Feedback for High Five App",route:"Privacy",params:{url:FEEDBACK_LINK}},
                      {name:"Privacy Policy",route:"Privacy",params:{title:"Privacy Policy"}},
                      {name:"Terms of Use",route:"Privacy",params:{}},
                      {name:"How to Use",route:"Tutorial",params:{}}]
}

const personalProfileList = [getSettingSection(),
                     [{name:"Password Settings"},
                     {name:"Push Notifications",options:["Daily Activity Summary"]}],
                     [{name:"Privacy Settings",options:["Private Account"],text:"When your account is private, nobody can view your high five activity. Your high fives will be anonymous."},
                     {name:"Connectivity",options:["Share on Facebook","Share on Twitter"]}],
                     [{name:"Email Settings",options:["Email me my Daily Activity","Change Email"]},
                     {name:"Reset Password",route:"ResetPwd",params:{page:"settings"}},
                     {name:"Log Out",route:"Login",params:{}}]
                     ];
 const workProfileList = [getSettingSection(),
                     [{name:"Password Settings"},
                     {name:"Push Notifications",options:["Every time I get a High Five","Every time I get a Compliment","Daily Reminder"]}],
                     [{name:"Privacy Settings",options:["Private Account"],text:"When your account is private, nobody aside from you and your employer can see your High Five Statistics."},
                     {name:"Connectivity",options:["Share on Facebook","Share on Twitter"]}],
                     [{name:"Email Settings",options:["Email me my Daily Activity","Change Email"]},
                     {name:"Reset Password",route:"ResetPwd",params:{page:"settings"}},
                     {name:"Log Out",route:"Login",params:{}}]]

export class SettingsList extends React.Component{
   constructor(props){
       super(props)
          this.state = {
            switch : [],
            tint : [],
            selected : "Work Profile"
        }
      this.onValueChange = this.onValueChange.bind(this);
   }

     toggleSwitch(id)
    {
        if(this.state.switch[id]) {
         flag[id] = false;
         color[id] = "#FAB1B1";
        }
        else{
          flag[id]=true;
          color[id] = "#B6EDAD";

        }
        this.setState({switch:flag,tint:color});
    }

    gotoRoute = (route,params) =>
    {
      switch(route){
        case "Login":
        let clearValues = ['user-token','current-user'];
        clearValues.map((keyValue)=>{
            AsyncStorage.removeItem(keyValue).then((val)=>{
              console.log(keyValue+" "+"cleared");
            });
        });
        //AsyncStorage.clear();
         persistor.pause();
         persistor.purge(); 
         setTimeout(() => { client.resetStore(); }, 0);
        /*firebase.auth().signOut().then(function() {
           console.log('Signed Out');
        }, function(error) {
        console.error('Sign Out Error', error);
        });*/
        //LoginManager.logOut();
        if(GoogleSignin._user)
        {   
          GoogleSignin.signOut()
          .then(() => {
             console.log('out');
           })
           .catch((err) => {
            });
        }
        break;
        case "Tutorial":
         Analytics.trackEvent("HowToUse");
        break;
      }
      this.props.navigation.navigate(route,params);
    }

     onValueChange(value)
    {
        this.setState({selected:value});
    }

   render()
   {
      let selectedProfile = this.state.selected ? this.state.selected : "Work Profile";
      let selectedList = this.state.selected === "Work Profile" ? workProfileList : personalProfileList;
       return (<Content>
            <ProfilePicker value={this.state.selected} options={pickerOptions} type="profile" employee={this.props.employee} action={(value)=>{this.onValueChange(value)}}/>
              {/*<Text style={{padding:10,backgroundColor:"#f2f2f2",paddingLeft:15}}>{selectedProfile}</Text>*/}
               {selectedList.map((subArr,index) => { 
           return <List style={[{backgroundColor:'#FFF'},index === 0 ? null : {marginTop:deviceHeight/25}]} key={"list-"+index}>
                {
           subArr.map((category,j) => {
                let temp = [];
                temp.push (<ListItem onPress={()=>{category.route ? this.gotoRoute(category.route,category.params) : {}}} key={category.name}>
              <Text  style={styles.titleText}>{category.name}</Text>
            </ListItem>)
                 category.options ? category.options.map((opt,i) => {
              temp.push(<ListItem>
              <Grid>
              <Row>
                <Text style={styles.subTextTitle}>{opt}</Text>
                <Switch style={styles.switch} value={this.state.switch[String(index+"-"+i)]} disabled={true} id={String(index+"-"+i)} thumbTintColor="grey"  tintColor="lightgrey"  onValueChange={()=>{this.toggleSwitch(String(index+"-"+i))}}/>
              </Row>
              {category.text ? <Row style={styles.subTextRow}>
                  <Text style={styles.subText}>{category.text}</Text>
              </Row> : null}
              </Grid>
            </ListItem>);
                }) : null
            return temp;
            })
                }
            </List>
           })}
           </Content>)
   }
}

/*const styles = StyleSheet.create({
    subText:{
              fontSize:13
            }
 })*/
