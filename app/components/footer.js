import React from 'react';
import {Footer,FooterTab} from 'native-base';
import {TabIcon} from './tabElements';
import {withNavigation} from 'react-navigation';
import {globalstyles} from '../assets/css/styles';

const arr = [{imgSrc:require('../assets/images/settings.png'), text:"Settings"},
             {imgSrc:require('../assets/images/stars.png'),text:"Stars"},
             {imgSrc:require('../assets/images/high_five.png'),text:"Give High Five"},
             {imgSrc:require('../assets/images/promos.png'),text:"Promos"},
             {imgSrc:require('../assets/images/profile.png'),text:"My Profile"}]

 class FooterComponent extends React.Component
 {
   render()
   {
     return <Footer>
          <FooterTab style={globalstyles.footerColor} >
                  {arr.map((item,index) => {
                    return <TabIcon key={"icon"+index}  
                                    imgSrc={item.imgSrc} 
                                    text={item.text} 
                                    active={this.props.active === index+1 ? true : false}
                                    action={()=>{this.props.getContent ? this.props.getContent(index) : this.props.navigation.navigate('Container',{tabNo:index+1,routeName:item.routeName})}}/>
                  })}
               </FooterTab>
      </Footer>
   }
}

export default withNavigation(FooterComponent)
