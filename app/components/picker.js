import React from 'react';
import {Platform,StyleSheet} from 'react-native';
import {Form,Picker,Item} from 'native-base';
import Analytics from 'appcenter-analytics';

export default class extends React.Component {
constructor(props){
    super(props)
}


render()
{
let props = this.props;

let options = [];

let defaultValue = "";

if(props.type === "profile")
{
  props.options.map((opt,index)=> {

   if(index === 0)
   {
     options.push(<Item label={opt} value={opt} />); 
   }

  if(props.employee && Object.keys(props.employee).length > 0 && index === 1)
   {
    options.push(<Item label={opt} value={opt} />);
   }
  })
}
else if(props.type === "empStatus" || props.type === "empRole"){
  props.options.map((opt,index)=>{
    options.push(<Item label={opt} value={opt} />);
   })
}
else{
    props.options.map((opt,index)=>{
        options.push(<Item label={opt.name} value={opt.id} />);
    })
}

const FormStyle = props.type === "profile" ? styles.formStyle : styles.formStyleEmp;
const ItemTextStyle = props.type === "profile" ? styles.itemTextStyle : styles.itemTextStyleEmp;
const TextStyle = props.type === "profile" ? styles.textStyle :  styles.textStyleEmp;
const StyleAttribColor = props.type === "profile" ? styles.styleAttribColor : styles.styleAttribColorEmp;
    

    return   <Form style={FormStyle}>
             <Picker
             mode="dropdown"
             placeholder="Select Profile"
             selectedValue={props.value}
             onValueChange={props.action}
             style={[styles.styleAttrib,Platform.OS!=='ios'? StyleAttribColor:null]}
             itemStyle={[styles.itemStyle,Platform.OS === 'ios' ? styles.itemStyleIos : null]}
             textStyle={TextStyle}
             itemTextStyle={[ItemTextStyle,Platform.OS === 'ios' ? styles.itemTextStyleIos : null]}>
             {options}
             </Picker>
             </Form>
}
}

const styles = StyleSheet.create({
 formStyleEmp:{
   flex:1,
   backgroundColor:"#FFF",
   margin:2
 },
 formStyle : {
  width:170,
  backgroundColor: '#334149',
  margin:10
 },
  styleAttrib:{
   height:40,
   alignItems:'center',
   justifyContent:'center'
},
styleAttribColor:{
  color:  '#FFF' 
},
styleAttribColorEmp:{
    color:"#000"
},
itemStyle: {
      backgroundColor:'#334149',
},
itemStyleIos:{
    marginRight:10,
    alignItems:'center',
    justifyContent:'center'
},
textStyle:{
    color:'#FFF'
},
itemTextStyle:{
     color:'#FFF'
},
textStyleEmp:{
    color:"#000"
},
itemTextStyleEmp:{
    color:"#000"
},
itemTextStyleIos:{
 paddingLeft:50
}

})