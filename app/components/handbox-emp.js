import React from 'react';
import {Image,Text,Platform,StyleSheet,TouchableOpacity, Dimensions, PixelRatio} from 'react-native';
import {Col} from 'native-base';
import {globalstyles} from '../assets/css/styles';
import {callHighFiveQuery,callQueryNetwork} from '../utils/query-handler';
import {GET_HIGHFIVES} from '../constants';
import {connect} from 'react-redux';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
var DeviceInfo = require('react-native-device-info');

const eventName = "onGetCount";

var widthIOS = 165;
var heightIOS = 155;
var widthAndroid = (deviceWidth/2)-(deviceWidth/30);//161
var heightAndroid;//150
var imageSize1;//50
var imageSize2;//75
var fsize;//20

  if(DeviceInfo.isTablet()){
    if(PixelRatio.get()>=0&&PixelRatio.get()<=1){
      heightAndroid = deviceHeight/6
      imageSize1 = 60
      imageSize2 = 85
      fsize = 24
    }
    else{
      heightAndroid = deviceHeight/6
      imageSize1 = 50
      imageSize2 = 75
      fsize = 20
    }
  }
  else{
    imageSize1 = 50
    imageSize2 = 70
    fsize = 19
    heightAndroid = deviceHeight/4
    if(PixelRatio.get()>=0&&PixelRatio.get()<1.5){
      heightAndroid = deviceHeight/4
      imageSize1 = 30
      imageSize2 = 50
      fsize = 13
    }
    else if(PixelRatio.get()>=1.5&&PixelRatio.get()<2){
      heightAndroid = deviceHeight/4
      imageSize1 = 35
      imageSize2 = 50
      fsize = 16
    }
    else if(PixelRatio.get()>=2&&PixelRatio.get()<2.5){
      imageSize1 = 40
      imageSize2 = 55
      fsize = 17

    }
    else if(PixelRatio.get()>=2.5&&PixelRatio.get()<3){
      imageSize1 = 45
      imageSize2 = 60
      fsize = 18
    }
  }

class HandBoxEmp extends React.Component {
   
   constructor(props){
       super(props)
       this.state = {
           totalHighFive : props.totalHighFive ? props.totalHighFive : 0,
           empId : props.empId,
       }
       this.count = 0;
   }

   getHighFiveScore = () => {
     callQueryNetwork(this.props.queryConstant,this.props.pageName,eventName,(result)=>{
           let data = result.data;
           //console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
           //console.log(data);
           let totalHighFive =  this.props.text ? data.users[0] ? data.users[0].highFivesAwarded : this.state.totalHighFive : data.users[0] ? data.users[0].highFiveScore : this.state.totalHighFive;
            this.setState({totalHighFive: totalHighFive });
        });
   }

   componentDidMount()
   {
     this.getHighFiveScore();
   }

   componentDidUpdate()
   {  
     this.getHighFiveScore();
   }

  shouldComponentUpdate(nextProps, nextState){
         if(nextState.totalHighFive !== this.state.totalHighFive)
         {
            return true;
         }
         else{
           return false;
         }
   }



  render()
  {
    let tempArr = [this.state.totalHighFive ? this.state.totalHighFive : "0",
    this.props.text ? null : "High Fives",
    this.props.text ? this.props.text : "Received"
    ]
    let column = this.props.workProfile ? <Col style={[Platform.OS === 'ios' ? styles.boxIOS : styles.boxAndroid, {width:widthAndroid,alignItems:'center',justifyContent:'center',paddingTop:5}, this.props.bgColor ? this.props.bgColor : styles.handBgColor]}>
           <Image style={styles.imageWork} source={require('../assets/images/hand_nobox.png')}/>
           {tempArr.map((text,index)=>{
             return <Text id={"profile-txt-"+index} style={[globalstyles.text,{fontSize:fsize,color:"#000"}]}>{text}</Text>
           })}
         </Col> :
         <Col style={[Platform.OS === 'ios' ?  styles.boxIOS : styles.boxAndroid, {width:widthAndroid,alignItems:'center',justifyContent:'center',paddingTop:5}, this.props.bgColor ? this.props.bgColor : styles.handBgColor]}>
         {this.props.isHandClick ? <TouchableOpacity onPress={()=>{this.props.handleClick()}}>
            <Image style={styles.imagePersonal} source={require('../assets/images/hand_nobox.png')}/>
         </TouchableOpacity> :
         <Image style={{width:75,height:75,resizeMode:'contain'}} source={require('../assets/images/hand_nobox.png')}/>}

           <Text style={[globalstyles.text,{fontSize:fsize,color:"#000"}]}>{this.state.totalHighFive ? this.state.totalHighFive : "0"} {this.props.text ? this.props.text : "Received"}</Text>
         </Col> ;
        return column;

  }
}

const mapStateToProps = (state) => {
   return {
      totalHighFive : state.employee.totalHighFive
   }
}

export default connect (mapStateToProps)(HandBoxEmp)

const styles = StyleSheet.create({
  handBgColor:{
    backgroundColor:'#DBFDFF'
  },
  boxAndroid: {
    height:heightAndroid
  },
  boxIOS: {
    width:widthIOS,
    height:heightIOS
  },
  imageWork: {
    width:imageSize1,
    height:imageSize1,
    resizeMode:'contain'
  },
  imagePersonal:{
    width:imageSize2,
    height:imageSize2,
    resizeMode:'contain'
  },
})