import React from 'react';
import { StyleSheet, Text, View,SafeAreaView,AsyncStorage } from 'react-native';
//import {Avatar,Button,SearchBar,TabBar,Icon} from 'react-native-ios-kit';
import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './app/reducers';
import codePush from "react-native-code-push";
import AppNavigation from './app/components/app-navigation';   
import nativefirebase from 'react-native-firebase';

//import ApolloClient,{InMemoryCache} from 'apollo-boost';
//import {ApolloProvider,Query} from 'react-apollo';
//import { persistCache } from 'apollo-cache-persist';

//import { Sentry } from 'react-native-sentry';

//Sentry.config('https://952a58b7c77645759b0ea33c431c7cfb:767aa17b98ae469d873b0f396efb285b@sentry.io/416606').install();

const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
);

/*const cache = new InMemoryCache();

persistCache({
  cache,
  storage: AsyncStorage,
});

export const client = new ApolloClient({
   uri: "https://hfm-api.azurewebsites.net/graphql",
   cache
});*/


 class App extends React.Component {
  constructor() {
    super();
    this.state = { };
  }

  async componentDidMount() {
    // Build a channel
    nativefirebase.notifications().setBadge(0)
    const channel = new nativefirebase.notifications.Android.Channel('test-channel', 'Test Channel', nativefirebase.notifications.Android.Importance.Max)
      .setDescription('My apps test channel');

    // Create the channel
    nativefirebase.notifications().android.createChannel(channel);

    this.notificationDisplayedListener = nativefirebase.notifications().onNotificationDisplayed((notification: Notification) => {
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        
      console.log("ifnotification display");

    });

    this.notificationListener = nativefirebase.notifications().onNotification((notification: Notification) => {
        // Process your notification as required

        // nativefirebase.notifications().displayNotification(notification)
        //                         .catch(err => console.error(err));


        const localNotification = new nativefirebase.notifications.Notification({
    show_in_foreground: true
  })
    .setNotificationId(notification.notificationId)
    .setTitle(notification.title)
    .setSubtitle(notification.subtitle)
    .setBody(notification.body)
    .setData(notification.data)
    .setSound('default')
    .android.setChannelId("channelId") // e.g. the id you chose above
    .android.setColor("#000000") // you can set a color here
    .android.setPriority(nativefirebase.notifications.Android.Priority.High);

  nativefirebase.notifications().displayNotification(localNotification);


        console.log("if1");
    });
    this.notificationOpenedListener = nativefirebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification;
        console.log("if2");
        nativefirebase.notifications().removeAllDeliveredNotifications();
    });
    const notificationOpen: NotificationOpen = await nativefirebase.notifications().getInitialNotification();
    if (notificationOpen) {
        // App was opened by a notification
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification;
               console.log("if3");
        nativefirebase.notifications().removeAllDeliveredNotifications();
    }
}

componentWillUnmount() {
    this.notificationDisplayedListener();
    this.notificationListener();
        this.notificationOpenedListener();

}

  render() {
    return (
   <Provider store={store}>
    <SafeAreaView style={{flex:1}}>
       <AppNavigation/>
    </SafeAreaView>
     </Provider>
    );
  }
}

export default codePush(App)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
