import React from 'react';
import {Platform} from 'react-native'
import  {StackNavigator,NavigationActions} from 'react-navigation';
import Login from './app/pages/login';
import SignUp from './app/pages/signup';
import Tab from './app/pages/tab';
import Tutorial from './app/pages/tutorial';
import Splash from './app/pages/splash';
import Settings from './app/pages/settings';
import ForgotPassword from './app/pages/forgot-pwd';
import ResetPassword from './app/pages/reset-pwd';
import Highfive from './app/pages/highfive';
import Privacy from './app/pages/privacy';
import Organization from './app/pages/organization';
import AfterHighfive from './app/pages/afterhighfive';
import AfterFeedback from './app/pages/afterfeedback';
import Redeem from './app/pages/redeem';
import Analytics from 'appcenter-analytics';

export const getRoutes = (userToken,cardlist,images) => {

let HomeObj = userToken ? {screen : Tab} : {screen : Login}
let FirstObj = Platform.OS === 'ios' ?  HomeObj : {screen:Splash}

const ModalStack = StackNavigator({
   First :FirstObj,
   Home : HomeObj,
   SignUp :{
     screen : SignUp
   },
   Login : {
     screen : Login
   },
   Container : {
     screen : Tab
   },
   Tutorial : {
     screen : Tutorial
   },
   ForgotPwd :{
     screen : ForgotPassword
   },
   ResetPwd : {
     screen : ResetPassword
   },
   Highfive : {
     screen : Highfive
   },
   Privacy:{
     screen : Privacy
   },
   Organization:{
     screen : Organization
   },
   AfterHighfive:{
     screen : AfterHighfive
   },
   AfterFeedback:{
     screen : AfterFeedback
   },
   Redeem : {
     screen : Redeem
   }
});
const navigateOnce = (getStateForAction) => (action, state) => {
  const {type, routeName} = action;
  /*if(state && type === NavigationActions.NAVIGATE && routeName !== state.routes[state.routes.length - 1].routeName)
  {
      Analytics.trackEvent(routeName);
  }*/
  
  return (
    state &&
    type === NavigationActions.NAVIGATE &&
    routeName === state.routes[state.routes.length - 1].routeName
  ) ? null : getStateForAction(action, state);
  // you might want to replace 'null' with 'state' if you're using redux (see comments below)
};

ModalStack.router.getStateForAction = navigateOnce(ModalStack.router.getStateForAction);

return <ModalStack screenProps={{cardlist:cardlist,images:images}}/>
}